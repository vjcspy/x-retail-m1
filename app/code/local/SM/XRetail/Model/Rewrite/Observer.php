<?php

/**
 * Created by PhpStorm.
 * User: tung
 * Date: 22/07/2016
 * Time: 16:31
 */
class SM_XRetail_Model_Rewrite_Observer
{

    public function salesOrderPlaceAfter($observer)
    {
        $order = $observer->getOrder();

        if (!Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()) {
            return;
        }

        $warehouseId = $order->getWarehouseId();

        if (!Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse() || !$warehouseId) {
            $originObserver = new Magestore_Inventorywarehouse_Model_Observer();
            $originObserver->salesOrderPlaceAfter($observer);
            return;
        }

        $items = $order->getAllItems();

        foreach ($items as $item) {
            $manageStock = Mage::helper('inventoryplus')->getManageStockOfProduct($item->getProductId());
            if (!$manageStock) {
                continue;
            }

            if ($item->getProduct()->isComposite()) {
                continue;
            }


            $qtyOrdered = Mage::helper('inventoryplus')->getQtyOrderedFromOrderItem($item);

            $warehouseProduct = Mage::getModel('inventoryplus/warehouse_product')->getCollection()
                ->addFieldToFilter('warehouse_id', $warehouseId)
                ->addFieldToFilter('product_id', $item->getProductId())
                ->getFirstItem();
            if (!$warehouseProduct->getId()) {
                $warehouseProduct = Mage::getModel('inventoryplus/warehouse_product')
                    ->getCollection()
                    ->addFieldToFilter('product_id', $item->getProductId())
                    ->getFirstItem();
                $warehouseId = $warehouseProduct->getWarehouseId();
            }

            if (!$warehouseProduct->getId()) {
                return;
            }

            $currentQty = $warehouseProduct->getAvailableQty() - $qtyOrdered;

            try {
                $warehouseProduct->setAvailableQty($currentQty)
                    ->save();
                $warehouse = Mage::getModel('inventoryplus/warehouse')->load($warehouseId);
                $warehouse->setWarehouseOrderItem($item, $item->getProductId(), $qtyOrdered);
            } catch (Exception $e) {

            }
        }
    }
}

