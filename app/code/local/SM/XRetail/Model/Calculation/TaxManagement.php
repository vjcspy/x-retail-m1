<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/25/16
 * Time: 5:41 PM
 */
class SM_XRetail_Model_Calculation_TaxManagement extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var \SM_XRetail_Model_Resource_Tax_Calculation
     */
    protected $_taxCalculationResourceModel;
    /**
     * @var \Mage_Tax_Model_Calculation_Rate
     */
    private $_taxRateModel;
    /**
     * @var \Mage_Tax_Model_Calculation_Rule
     */
    private $_taxRuleModel;
    /**
     * @var \Mage_Tax_Model_Calculation
     */
    private $_taxCalculationModel;
    private $_taxClassModel;

    /**
     * SM_XRetail_Model_Calculation_TaxManagement constructor.
     */
    public function __construct() {
        $this->_taxCalculationResourceModel = Mage::getModel('xretail/resource_tax_calculation');
        $this->_taxCalculationModel         = Mage::getModel('tax/calculation');
        $this->_taxRateModel                = Mage::getModel('tax/calculation_rate');
        $this->_taxRuleModel                = Mage::getModel('tax/calculation_rule');
        $this->_taxClassModel               = Mage::getModel('tax/class');
        parent::__construct();
    }

    public function getXTax() {
        $addInfo = new Varien_Object();
        $taxRate = new Varien_Object($this->getRequest()->getParam('taxRate'));
        $addInfo->setCountryId($taxRate->getTaxCountryId())
                ->setRegionId($taxRate->getTaxRegionId())
                ->setPostcode($taxRate->getTaxPostcode());
        $this->_taxCalculationResourceModel->getRateByAddress($addInfo);

        $data  = $this->_taxCalculationResourceModel->getRateByAddress($addInfo);
        $rates = array();
        foreach ($data as $rate) {
            $rates[] = new SM_XRetail_Model_Api_Data_XTax($rate);
        }

        $searchResult = $this->getSearchResult();
        $searchResult->setItems($rates);
        $searchResult->setTotalCount(count($rates));

        return $searchResult->getOutput();
    }

    public function getXTaxCalculation() {
        $searchCriteria = $this->getSearchCriteria();
        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        $collection = $this->_taxCalculationModel->getCollection();

        return $this->processDataCollection($collection)->getSearchResult()->getOutput();
    }

    public function getXTaxRule() {
        $searchCriteria = $this->getSearchCriteria();

        $collection = $this->_taxRuleModel->getCollection();

        $collection->setCurPage($searchCriteria->getData('currentPage'));
        $collection->setPageSize(100);

        $collection->load();
        $items = array();
        foreach ($collection as $item) {
            $items[]                  = $item->getData();
            $item['tax_calculations'] = $this->getTaxCalculation($item->getData('tax_calculation_rule_id'));
        }
        $searchResult = $this->getSearchResult();
        $searchResult->setItems($items);
        $searchResult->setTotalCount($collection->getSize());

        return $this->processDataCollection($collection)->getSearchResult()->getOutput();
    }

    protected function getTaxCalculation($rateId) {
        $collection = Mage::getModel('tax/calculation')->getCollection();
        $collection->addFieldToFilter('tax_calculation_rule_id', array('eq' => $rateId));
        $collection->load();
        $items = array();
        foreach ($collection as $item) {
            $items[] = $item->getData();
        }

        return $items;
    }

    public function getXTaxRate() {
        $searchCriteria = $this->getSearchCriteria();
        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        $collection = $this->_taxRateModel->getCollection();

        return $this->processDataCollection($collection)->getSearchResult()->getOutput();
    }

    public function getXTaxClass() {
        $searchCriteria = $this->getSearchCriteria();
        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        $collection = $this->_taxClassModel->getCollection();

        if ($searchCriteria->getData('class_type')) {
            $collection->addFieldToFilter('class_type', $searchCriteria->getData('class_type'));
        }

        return $this->processDataCollection($collection)->getSearchResult()->getOutput();
    }

    /**
     * @param $collection
     *
     * @return $this
     * @throws \Exception
     */
    private function processDataCollection($collection) {
        $searchCriteria = $this->getSearchCriteria();
        $collection->setCurPage($searchCriteria->getData('currentPage'));
        $collection->setPageSize(100);

        $collection->load();
        $items = array();
        foreach ($collection as $item) {
            $items[] = $item->getData();
        }
        $searchResult = $this->getSearchResult();
        $searchResult->setItems($items);
        $searchResult->setTotalCount($collection->getSize());

        return $this;
    }
}
