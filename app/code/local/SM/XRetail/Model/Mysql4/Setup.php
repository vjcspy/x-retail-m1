<?php

/**
 * User: vjcspy
 * Date: 3/21/16
 * Time: 10:19 AM
 */
class SM_XRetail_Model_Mysql4_Setup extends Mage_Core_Model_Resource_Setup {

    protected $_helper;

    public function __construct(
        $resourceName
    ) {
        $this->_helper = Mage::helper('xretail');
        parent::__construct($resourceName);
    }

    public function install() {
        $this->_helper->addLog('Installed!');
        return $this;
    }
}