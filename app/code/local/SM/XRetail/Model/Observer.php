<?php

/**
 * Created by khoild@smartosc.com/mr.vjcspy@gmail.com
 * User: vjcspy
 * Date: 20/04/2016
 * Time: 14:49
 */
class SM_XRetail_Model_Observer extends Varien_Object {

    protected $_arrayOptions = array();
    protected $_currentOption = 0;
    private $realTimeManagement;

    public function __construct() {
        $this->realTimeManagement = Mage::getModel('xretail/resourceModel_realTimeManagement');

        return parent::__construct();
    }

    function saveNewCustomer(Varien_Event_Observer $observer) {

        if (SM_XRetail_Adminhtml_CustomerController::$isSaveNewCustomer === true) {
            $customer = $observer->getData('customer');
            $cId      = $customer->getId();
            $this->realTimeManagement->needReloadRealTime($cId, SM_XRetail_Model_ResourceModel_RealTimeManagement::TYPE_CUSTOMER_NEW);
        }

        return $this;
    }
}
