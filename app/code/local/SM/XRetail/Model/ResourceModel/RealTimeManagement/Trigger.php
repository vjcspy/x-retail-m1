<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 06/04/2016
 * Time: 10:10
 */
class SM_XRetail_Model_ResourceModel_RealTimeManagement_Trigger {

    /**
     * @var SM_XRetail_Model_Api_Configuration
     */
    private $_apiConfig;
    /**
     * @var SM_XRetail_Helper_Data
     */
    private $_helper;

    /**
     * SM_XRetail_Model_ResourceModel_RealTimeManagement_Trigger constructor.
     */
    public function __construct() {

        $this->_apiConfig = Mage::getModel('xretail/api_configuration');
        $this->_helper    = Mage::helper('xretail');
    }

    /**
     * Trigger X-Retail client
     *
     * @param null  $url
     * @param array $data
     *
     * @return bool
     */
    public function triggerApp($url = null, $data = array()) {

        if (is_null($url))
            $url = $this->_apiConfig->getStoreConfig('xretail/config/triggerurl');
        if (is_null($url))
            return false;
        else {
            // $url = "http://mage1connector.xd.smartosc.com/index.php/rest/v1/test";

            return $this->sendPostViaSocket($url, $data);
        }
    }

    /**
     * @param $url
     * @param $params
     *
     * @return string
     */
    private function sendRequest($url, $params) {

        $opts = array(
            'http' =>
                array(
                    'method'  => 'GET',
                    'header'  => "Content-Type: text/xml\r\n",
                    // "Authorization: Basic " . base64_encode("$https_user:$https_password") . "\r\n",
                    'content' => $params,
                    'timeout' => 1
                )
        );

        $context = stream_context_create($opts);

        return (file_get_contents($url, false, $context, -1, 40000));
    }

    /**
     * @param      $url
     * @param null $param
     *
     * @return bool
     */
    private function sendGetViaSocket($url, $param = null) {

        $parts  = parse_url($url);
        $packet = "GET " . $parts['path'] . " HTTP/1.0\r\n";
        $packet .= "Host: {$parts['host'] }\r\n";
        $packet .= "Connection: close\r\n\r\n";


        try {
            $sock = fsockopen(
                $parts['host'],
                isset($parts['port']) ? $parts['port'] : 80,
                $errno,
                $errstr,
                1);

            if (!$sock)
                die("Can't connect: " . $errstr);

            fwrite($sock, $packet, strlen($packet));
            socket_close($sock);
            var_dump($sock);

            return true;
        } catch (Exception $e) {
            $this->_helper->addLog("Can't update real time product. Because: " . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $url
     * @param $params
     *
     * @return bool
     */
    private function sendPostViaSocket($url, $params) {
        try {
            $post_params = array();
            foreach ($params as $key => &$val) {
                if (is_array($val)) $val = implode(',', $val);
                $post_params[] = $key . '=' . urlencode($val);
            }
            $post_string = implode('&', $post_params);

            $parts = parse_url($url);
            $fp    = fsockopen(
                $parts['host'],
                isset($parts['port']) ? $parts['port'] : 80,
                $errno,
                $errstr,
                30);

            $out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
            $out .= "Host: " . $parts['host'] . "\r\n";
            $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $out .= "Content-Length: " . strlen($post_string) . "\r\n";
            $out .= "Connection: Close\r\n\r\n";
            if (isset($post_string)) $out .= $post_string;
            fwrite($fp, $out, strlen($out));
            socket_close($fp);
        } catch (Exception $e) {
            Mage::helper('xretail')->addLog($e->getMessage(), null, 'xretail-reailtime.log');
        }

        return true;
    }

    private function testSocket($url, $params) {
        error_reporting(E_ALL);

        echo "<h2>TCP/IP Connection</h2>\n";

        /* Get the port for the WWW service. */
        $service_port = getservbyname('www', 'tcp');

        /* Get the IP address for the target host. */
        $address = gethostbyname('192.168.30.106');

        /* Create a TCP/IP socket. */
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($socket === false) {
            echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
        }
        else {
            echo "OK.\n";
        }

        echo "Attempting to connect to '$address' on port '$service_port'...";
        $result = socket_connect($socket, $address, $service_port);
        if ($result === false) {
            echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
        }
        else {
            echo "OK.\n";
        }

        $in = "HEAD / HTTP/1.1\r\n";
        $in .= "Host: www.example.com\r\n";
        $in .= "Connection: Close\r\n\r\n";
        $out = '';

        echo "Sending HTTP HEAD request...";
        socket_write($socket, $in, strlen($in));
        echo "OK.\n";

        echo "Reading response:\n\n";
        $buf = 'This is my buffer.';
        if (false !== ($bytes = socket_recv($socket, $buf, 2048, MSG_WAITALL))) {
            echo "Read $bytes bytes from socket_recv(). Closing socket...";
        }
        else {
            echo "socket_recv() failed; reason: " . socket_strerror(socket_last_error($socket)) . "\n";
        }
        socket_close($socket);

        echo $buf . "\n";
        echo "OK.\n\n";
        die();

        return true;
    }
}