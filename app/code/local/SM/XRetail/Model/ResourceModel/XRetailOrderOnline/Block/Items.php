<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 11:16 AM
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Items extends SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutput implements SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutputInterface {

    /**
     * @var Mage_Adminhtml_Block_Sales_Order_Create_Items_Grid
     */
    protected $block;

    /**
     * SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Items constructor.
     * @param array $args
     */
    public function __construct(array $args) {
        $this->block = Mage::getBlockSingleton('adminhtml/sales_order_create_items_grid');
        parent::__construct($args);
    }

    public function getOutput() {
        $dataItems = array();
        $items = $this->getItems();
        foreach ($items as $item) {
            $dataItems[] = $item->getData();
        }
        return $dataItems;
    }

    /**
     * @return Mage_Sales_Model_Quote_Item[]
     */
    protected function getItems() {
        return Mage::getSingleton('xretail/sales_order_create')->getQuote()->getAllVisibleItems();
    }

}
