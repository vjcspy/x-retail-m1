<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 3:25 PM
 */
interface SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutputInterface {
    /**
     * Retrieve Block Output Data
     * @return mixed
     */
    public function getOutput();
}
