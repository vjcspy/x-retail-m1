<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 3:53 PM
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_PaymentMethod extends SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutput implements SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutputInterface {

    /**
     * @var Mage_Adminhtml_Block_Sales_Order_Create_Billing_Method_Form
     */
    protected $block;

    /**
     * PaymentMethod constructor.
     * @param array $args
     */
    public function __construct(array $args) {
        $this->block = Mage::getBlockSingleton('adminhtml/sales_order_create_billing_method_form');
        parent::__construct($args);
    }

    /**
     * Retrieve Block Output Data
     * @return mixed
     */
    public function getOutput() {
        /* @var $block Mage_Adminhtml_Block_Sales_Order_Create_Billing_Method_Form */
        $block = $this->getBlock();
        $payments = array();
        if ($block->hasMethods()) {
            $_methods = $this->getBlock()->getMethods();
            foreach ($_methods as $_method) {
                $_code = $_method->getCode();
                $currentMethod = array();
                $currentMethod['code'] = $_code;
                $currentMethod['title'] = $_method->getTitle();
                $payments[] = $currentMethod;
            }
        }
        return $payments;
    }
}
