<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 3:25 PM
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_ShippingMethod extends SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutput implements SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutputInterface {

    /**
     * @var Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Method_Form
     */
    protected $block;

    /**
     * ShippingMethod constructor.
     * @param array $args
     */
    public function __construct(array $args) {
        $this->block = Mage::getBlockSingleton('adminhtml/sales_order_create_shipping_method_form');
        parent::__construct($args);
    }

    public function getOutput() {
        $this->getQuote()->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        $_shippingRateGroups = $this->block->getShippingRates();
        $carrier = array();
        if ($_shippingRateGroups) {
            foreach ($_shippingRateGroups as $code => $_rates) {
                $carrier[$code] = array();
                $carrier[$code]['name'] = $this->block->getCarrierName($code);
                $carrier[$code]['rates'] = array();
                foreach ($_rates as $_rate) {
                    $_code = $_rate->getCode();
                    $currentRate = array();
                    $currentRate['code'] = $_rate->getCode();
                    if ($_rate->getErrorMessage())
                        $currentRate['error_message'] = $_rate->getErrorMessage();
                    else {
                        if ($this->block->isMethodActive($_code))
                            $currentRate['checked'] = true;
                        else
                            $currentRate['checked'] = false;
                        $currentRate['excl_tax'] = $this->getShippingPrice($_rate->getPrice(), $this->block->helper('tax')->displayShippingPriceIncludingTax());
                        $currentRate['incl_tax'] = $_incl = $this->getShippingPrice($_rate->getPrice(), true);;
                    }
                    $carrier[$code]['rates'][] = $currentRate;
                }
            }
        }
        return $carrier;
    }

    protected function getShippingPrice($price, $flag) {
        return $this->getQuote()->getStore()->convertPrice(
            Mage::helper('tax')->getShippingPrice(
                $price,
                $flag,
                $this->block->getAddress(),
                null,
                //We should send exact quote store to prevent fetching default config for admin store.
                $this->block->getAddress()->getQuote()->getStore()
            ),
            false
        );
    }
}
