<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/15/16
 * Time: 2:01 PM
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Totals
    extends SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutput
    implements SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutputInterface {

    /**
     * Retrieve Block Output Data
     *
     * @return mixed
     */
    public function getOutput() {
        // TODO: Implement getOutput() method.
        if ($this->getQuote()->isVirtual())
            $totals = $this->getQuote()->getBillingAddress();
        else
            $totals = $this->getQuote()->getShippingAddress();

        return [
            'subtotal'                             => $totals->getData('subtotal'),
            //'base_subtotal' => $totals->getData('base_subtotal'),
            //'subtotal_with_discount' => $totals->getData('subtotal_with_discount'),
            //'base_subtotal_with_discount' => $totals->getData('base_subtotal_with_discount'),
            'tax_amount'                           => $totals->getData('tax_amount'),
            //'base_tax_amount' => $totals->getData('base_tax_amount'),
            //'shipping_amount' => $totals->getData('shipping_amount'),
            //'base_shipping_amount' => $totals->getData('base_shipping_amount'),
            //'shipping_tax_amount' => $totals->getData('shipping_tax_amount'),
            //'base_shipping_tax_amount' => $totals->getData('base_shipping_tax_amount'),
            'discount_amount'                      => $totals->getData('discount_amount'),
            //'base_discount_amount' => $totals->getData('base_discount_amount'),
            'grand_total'                          => $totals->getData('grand_total'),
            //'base_grand_total' => $totals->getData('base_grand_total'),
            //'applied_taxes' => $totals->getData('applied_taxes'),
            'subtotal_incl_tax'                    => $totals->getData('subtotal_incl_tax'),
            //'base_subtotal_total_incl_tax' => $totals->getData('base_subtotal_total_incl_tax'),
            //'shipping_incl_tax' => $totals->getData('shipping_incl_tax'),
            //'base_shipping_incl_tax' => $totals->getData('base_shipping_incl_tax'),
            //'vat_id' => $totals->getData('vat_id'),
            //'vat_is_valid' => $totals->getData('vat_is_valid'),
            //'vat_request_id' => $totals->getData('vat_request_id'),
            //'vat_request_date' => $totals->getData('vat_request_date'),
            //'vat_request_success' => $totals->getData('vat_request_success'),
            //'rounding_deltas' => $totals->getData('rounding_deltas'),
            //'tax_shipping_amount' => $totals->getData('tax_shipping_amount'),
            //'base_tax_shipping_amount' => $totals->getData('base_tax_shipping_amount'),
            //'shipping_taxable' => $totals->getData('shipping_taxable'),
            //'base_shipping_taxable' => $totals->getData('base_shipping_taxable'),
            //'is_shipping_incl_tax' => $totals->getData('base_shipping_taxable'),
            //'cart_fixed_rules' => $totals->getData('cart_fixed_rules'),
            //'applied_rule_ids' => $totals->getData('applied_rule_ids'),
            //'base_shipping_hidden_tax_amount' => $totals->getData('base_shipping_hidden_tax_amount'),
            'hidden_tax'                           => $totals->getData('hidden_tax_amount'),
            'xretail_discount_per_items_amount'    => $totals->getData('xretail_discount_per_items_amount'),
            'xretail_discount_per_items_after_tax' => $totals->getData('xretail_discount_per_items_after_tax_amount'),
        ];
    }

    public function getQuote() {
        return Mage::getSingleton('xretail/sales_order_create')->getQuote();
    }
}
