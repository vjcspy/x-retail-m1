<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 11:20 AM
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutput extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract {

    /**
     * @var
     */
    protected $block;

    /**
     * SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Contract_BlockOutput constructor.
     * @param array $args
     */
    public function __construct(array $args) {
        parent::__construct($args);
    }

    /**
     * @return mixed
     */
    public function getBlock() {
        return $this->block;
    }

    /**
     * @param mixed $block
     */
    public function setBlock($block) {
        $this->block = $block;
    }
}
