<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/24/16
 * Time: 9:56 AM
 */
class SM_XRetail_Model_ResourceModel_RuleManagement extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var \Mage_SalesRule_Model_Resource_Rule_Collection
     */
    private $_collection;

    /**
     * SM_XRetail_Model_ResourceModel_RuleManagement constructor.
     */
    public function __construct() {
        $this->_collection = Mage::getResourceModel('salesrule/rule_collection');
        parent::__construct();
    }

    public function index() {
        $searchCriteria = $this->getSearchCriteria();
        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        $c = clone $this->_collection;
        $size = $c->count();
        $this->processPageAndSize($searchCriteria->getData('currentPage'), $searchCriteria->getData('pageSize'));

        $items = array();
        foreach ($this->_collection as $rule) {
            $rule = Mage::getModel('salesrule/rule')->load($rule->getId());

            $conditions = $rule->getConditions()->asArray();
            $ruleData = $rule->getData();

            $coupons = ($ruleData['coupon_type'] == 2 && $ruleData['use_auto_generation']) ? $this->getCouponCodes($ruleData['rule_id']) : null;
            $items[] = array(
                'data'       => $ruleData,
                'conditions' => $conditions,
                'coupons'    => $coupons
            );

        }
        $this->_searchResult->setItems($items);

        $this->_searchResult->setTotalCount($size);

        $this->_searchResult->setSearchCriteria($this->getSearchCriteria());

        return $this->_searchResult->getOutput();
    }

    protected function processPageAndSize(
        $page = 1, $pageSize = 100
    ) {
        $this->_collection->setCurPage($page);
        $this->_collection->setPageSize($pageSize);
        return $this->_collection;
    }

    protected function getCouponCodes($ruleId) {
        $items = array();
        $collection = Mage::getResourceModel('salesrule/coupon_collection')
            ->addRuleToFilter($ruleId)
            ->addGeneratedCouponsFilter();
        foreach ($collection as $item) {
            $items[] = $item->getData();
        }
        return $items;
    }
}