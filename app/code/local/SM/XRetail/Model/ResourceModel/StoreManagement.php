<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 28/03/2016
 * Time: 15:05
 */
class SM_XRetail_Model_ResourceModel_StoreManagement extends SM_XRetail_Model_Api_ServiceAbstract {

    public function index() {
        $websiteData = array();
        foreach (Mage::app()->getWebsites() as $website) {
            $websiteData[$website->getId()] = $website->getData();
            $groupData                      = array();

            foreach ($website->getGroups() as $group) {
                $groupData[$group->getId()] = $group->getData();
                $storeData                  = array();
                foreach ($group->getStores() as $store) {
                    $storeData[$store->getId()] = $store->getData();
                }
                $groupData[$group->getId()] ['stores'] = $storeData;
            }
            $websiteData[$website->getId()] ['groups'] = $groupData;
        }

        return $websiteData;
    }
}