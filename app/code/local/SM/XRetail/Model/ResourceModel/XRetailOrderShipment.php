<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 28/03/2016
 * Time: 11:57
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderShipment extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var array
     */
    private $_shipmentData;
    /**
     * @var Mage_Sales_Model_Order
     */
    private $_order;

    /**
     * @return array
     * @throws \Exception
     */
    public function getInfo() {

        $this->dummy();

        $shipmentData = $this->setShipmentData(new Varien_Object($this->getRequest()->getParam('shipment_data')));
        $orderId = $shipmentData['order_id'];
        if (is_null($orderId))
            throw new Exception('Must have param order_id');
        $shipment = $this->_initShipment();
        return array(
            'order' => $this->_order->getData(),
            'billing_address' => $this->_order->getBillingAddress()->getData(),
            'shipping_address' => $this->_order->getShippingAddress()->getData(),
            'items' => $this->getItemsInfo($shipment),
        );
    }

    /**
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @return array
     */
    protected function getItemsInfo(Mage_Sales_Model_Order_Shipment $shipment) {

        $itemsData = array();
        $_items = $shipment->getAllItems();
        foreach ($_items as $_item) {
            if ($_item->getOrderItem()->getIsVirtual() || $_item->getOrderItem()->getParentItem()) continue;
            $itemsData[] = $_item->getData();
        }
        return $itemsData;
    }

    /**
     * @return Varien_Object
     */
    public function getShipmentData() {

        return $this->_shipmentData;
    }

    /**
     * @param Varien_Object $shipmentData
     */
    public function setShipmentData($shipmentData) {

        $this->_shipmentData = $shipmentData;
        return $this->_shipmentData->getData();
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function createShipment() {

        // $this->dummy();
        $warehouseItems= array();
        $shipmentData = $this->getRequest()->getParam('shipment_data');

        /*these params will be used when observer shipment of magestore warehouse run*/
        $order = Mage::getModel('sales/order')->load($shipmentData['order_id']);

        if(Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()){
            foreach ($order->getAllItems() as $item){
                $warehouseItems['items'][$item->getItemId()] = $order->getWarehouseId();
            }
            $this->getRequest()->setParam('warehouse-shipment', $warehouseItems);
        }

        $shipmentData = $this->setShipmentData(new Varien_Object($shipmentData));

        $data = $shipmentData['shipment'];
        $shipment = $this->_initShipment();
        if (!$shipment)
            throw new Exception("Can't get shipment");

        $shipment->register();
        $comment = '';
        if (!empty($data['comment_text'])) {
            $shipment->addComment(
                $data['comment_text'],
                isset($data['comment_customer_notify']),
                isset($data['is_visible_on_front'])
            );
            if (isset($data['comment_customer_notify'])) {
                $comment = $data['comment_text'];
            }
        }

        if (!empty($data['send_email'])) {
            $shipment->setEmailSent(true);
        }

        $shipment->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
        $responseAjax = new Varien_Object();
        $isNeedCreateLabel = isset($data['create_shipping_label']) && $data['create_shipping_label'];

        if ($isNeedCreateLabel && $this->_createShippingLabel($shipment)) {
            $responseAjax->setOk(true);
        }

        $this->_saveShipment($shipment);

        $shipment->sendEmail(!empty($data['send_email']), $comment);

        if ($isNeedCreateLabel)
            return $responseAjax->toJson();
        else
            return $shipment->getData();

    }

    /**
     * Save shipment and order in one transaction
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment
     *
     * @return Mage_Adminhtml_Sales_Order_ShipmentController
     */
    protected function _saveShipment($shipment) {

        $shipment->getOrder()->setIsInProcess(true);
        $transactionSave = Mage::getModel('core/resource_transaction')
                               ->addObject($shipment)
                               ->addObject($shipment->getOrder())
                               ->save();

        return $this;
    }

    /**
     * Create shipping label for specific shipment with validation.
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment
     *
     * @return bool
     */
    protected function _createShippingLabel(Mage_Sales_Model_Order_Shipment $shipment) {

        if (!$shipment) {
            return false;
        }
        $carrier = $shipment->getOrder()->getShippingCarrier();
        if (!$carrier->isShippingLabelsAvailable()) {
            return false;
        }
        $shipment->setPackages($this->getShipmentData()->getData('packages'));
        $response = Mage::getModel('shipping/shipping')->requestToShipment($shipment);
        if ($response->hasErrors()) {
            Mage::throwException($response->getErrors());
        }
        if (!$response->hasInfo()) {
            return false;
        }
        $labelsContent = array();
        $trackingNumbers = array();
        $info = $response->getInfo();
        foreach ($info as $inf) {
            if (!empty($inf['tracking_number']) && !empty($inf['label_content'])) {
                $labelsContent[] = $inf['label_content'];
                $trackingNumbers[] = $inf['tracking_number'];
            }
        }
        $outputPdf = $this->_combineLabelsPdf($labelsContent);
        $shipment->setShippingLabel($outputPdf->render());
        $carrierCode = $carrier->getCarrierCode();
        $carrierTitle = Mage::getStoreConfig('carriers/' . $carrierCode . '/title', $shipment->getStoreId());
        if ($trackingNumbers) {
            foreach ($trackingNumbers as $trackingNumber) {
                $track = Mage::getModel('sales/order_shipment_track')
                             ->setNumber($trackingNumber)
                             ->setCarrierCode($carrierCode)
                             ->setTitle($carrierTitle);
                $shipment->addTrack($track);
            }
        }
        return true;
    }

    /**
     * Combine array of labels as instance PDF
     *
     * @param array $labelsContent
     *
     * @return Zend_Pdf
     */
    protected function _combineLabelsPdf(array $labelsContent) {

        $outputPdf = new Zend_Pdf();
        foreach ($labelsContent as $content) {
            if (stripos($content, '%PDF-') !== false) {
                $pdfLabel = Zend_Pdf::parse($content);
                foreach ($pdfLabel->pages as $page) {
                    $outputPdf->pages[] = clone $page;
                }
            }
            else {
                $page = $this->_createPdfPageFromImageString($content);
                if ($page) {
                    $outputPdf->pages[] = $page;
                }
            }
        }
        return $outputPdf;
    }

    /**
     * Create Zend_Pdf_Page instance with image from $imageString. Supports JPEG, PNG, GIF, WBMP, and GD2 formats.
     *
     * @param string $imageString
     *
     * @return Zend_Pdf_Page|bool
     */
    protected function _createPdfPageFromImageString($imageString) {

        $image = imagecreatefromstring($imageString);
        if (!$image) {
            return false;
        }

        $xSize = imagesx($image);
        $ySize = imagesy($image);
        $page = new Zend_Pdf_Page($xSize, $ySize);

        imageinterlace($image, 0);
        $tmpFileName = sys_get_temp_dir() . DS . 'shipping_labels_'
            . uniqid(mt_rand()) . time() . '.png';
        imagepng($image, $tmpFileName);
        $pdfImage = Zend_Pdf_Image::imageWithPath($tmpFileName);
        $page->drawImage($pdfImage, 0, 0, $xSize, $ySize);
        unlink($tmpFileName);
        return $page;
    }

    /**
     * @return Mage_Sales_Model_Order_Shipment
     * @throws Exception
     */
    protected function _initShipment() {

        $shipmentData = $this->getShipmentData()->getData();
        $orderId = $shipmentData['order_id'];
        $this->_order = $order = Mage::getModel('sales/order')->load($orderId);

        /**
         * Check order existing
         */
        if (!$order->getId())
            throw new Exception('The order no longer exists.');

        /**
         * Check shipment is available to create separate from invoice
         */
        if ($order->getForcedDoShipmentWithInvoice())
            throw new Exception('Cannot do shipment for the order separately from invoice.');

        /**
         * Check shipment create availability
         */
        if (!$order->canShip())
            throw new Exception('Cannot do shipment for the order.');
        $savedQtys = $this->_getItemQtys();
        $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($savedQtys);

        $tracks = $this->getRequest()->getPost('tracking');
        if ($tracks) {
            foreach ($tracks as $data) {
                if (empty($data['number'])) {
                    throw new Exception('Tracking number cannot be empty.');
                }
                $track = Mage::getModel('sales/order_shipment_track')
                             ->addData($data);
                $shipment->addTrack($track);
            }
        }

        Mage::register('current_shipment', $shipment);
        return $shipment;
    }

    /**
     * Initialize shipment items QTY
     */
    protected function _getItemQtys() {

        $data = $this->getShipmentData()->getData();
        if (isset($data['items'])) {
            $qtys = $data['items'];
        }
        else {
            $qtys = array();
        }
        return $qtys;
    }

    private function dummy() {

        $data = array(
            'order_id' => '226',
            'shipment' =>
                array(
                    'items' =>
                        array(
                            224 => '7',
                            226 => '2',
                        ),
                    'comment_text' => '',
                ),
        );
        $this->getRequest()->setParam('shipment_data', $data);
        return $this;
    }
}