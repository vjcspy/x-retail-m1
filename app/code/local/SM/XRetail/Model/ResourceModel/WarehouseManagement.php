<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 21/07/2016
 * Time: 14:05
 */
class SM_XRetail_Model_ResourceModel_WarehouseManagement extends SM_XRetail_Model_Api_ServiceAbstract{

    const PAGE_SIZE_WAREHOUSE_PRODUCT = 100;

    /**
     * @var Magestore_Inventoryplus_Model_Warehouse
     */
    protected $_warehouseModel;
    /**
     * @var \Mage_Customer_Model_Group
     */
    protected $_warehouseProductModel;

    /**
     * @var \Mage_CatalogInventory_Model_Stock_Item
     */
    protected $_stockItemModel;


    /**
     * SM_XRetail_Model_ResourceModel_WarehouseManagement constructor.
     */
    public function __construct()
    {
        $this->_prefix = Mage::getConfig()->getTablePrefix();

        if (Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()){

            $this->_warehouseModel     = Mage::getModel('inventoryplus/warehouse');
            $this->_warehouseProductModel = Mage::getModel('inventoryplus/warehouse_product');

        }

        $this->_stockItemModel = Mage::getModel('cataloginventory/stock_item');
        parent::__construct();
    }

    public function getListWarehouse()
    {
        if (Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()
            && Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()
        ){

            $collection = $this->_warehouseModel->getCollection();
            $collection->addFieldToSelect(array('is_enable' => 'status'));
            $collection->addFieldToSelect(array('name' => 'warehouse_name'));
            $collection->addFieldToSelect(array('id' => 'warehouse_id'));
            $collection->addFieldToSelect('is_root');
            $collection->load();

            $warehouse = array();

            foreach ($collection as $warehouseModel) {
                $converted = $warehouseModel->getData();
                if($converted['is_enable'] == 2){
                    $converted['is_enable'] = '0';
                }
                $warehouse[] = $converted;
            }

            $this->_searchResult->setItems($warehouse);
            $this->_searchResult->setTotalCount($collection->getSize());
        }else{
            $this->_searchResult->setItems(array($this->dummy()));
            $this->_searchResult->setTotalCount(1);
        }

        return $this->_searchResult->getOutput();
    }

    public function getWarehouseProduct()
    {
        if (Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()
            && Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()
        ){
            $collection = $this->_warehouseProductModel->getCollection();
            $collection->getSelect()->join(
                array('stock' => $this->_prefix . 'cataloginventory_stock_item'),
                'stock.product_id = main_table.product_id',
                array(
                    'product_id',
                    'is_in_stock',
                    'backorders',
                    'min_qty',
                    'stock_id',
                    'is_qty_decimal',
                    'min_sale_qty',
                    'max_sale_qty',
                    'low_stock_date',
                    'manage_stock'
                )
            );

            $collection->addFieldToSelect(array('qty' => 'available_qty'));
            $collection->addFieldToSelect(array('id' => 'warehouse_product_id'));
            $collection->addFieldToSelect('warehouse_id');
        } else {
            $collection = $this->_stockItemModel->getCollection();
            $collection->addFieldToSelect('product_id');
            $collection->addFieldToSelect(array('id' => 'product_id'));
            $collection->addFieldToSelect('is_in_stock');
            $collection->addFieldToSelect('backorders');
            $collection->addFieldToSelect('min_qty');
            $collection->addFieldToSelect('stock_id');
            $collection->addFieldToSelect('is_qty_decimal');
            $collection->addFieldToSelect('min_sale_qty');
            $collection->addFieldToSelect('max_sale_qty');
            $collection->addFieldToSelect('low_stock_date');
            $collection->addFieldToSelect('manage_stock');
            $collection->addFieldToSelect('qty');
        }

        $searchCriteria = $this->getSearchCriteria();


        if(!is_null($searchCriteria->getData('warehouseId'))) {
            $collection->addFieldToFilter('warehouse_id', $searchCriteria->getData('warehouseId'));
        }

        $collection->setCurPage($searchCriteria->getData('currentPage'));

        if (is_null($searchCriteria->getData('pageSize'))) {
            $searchCriteria->setData('pageSize', self::PAGE_SIZE_WAREHOUSE_PRODUCT);
        }

        $collection->setPageSize($searchCriteria->getData('pageSize'));

        $collection->load();

        $warehouse = array();

        /*if extension multi warehouse is not enabled, fix warehouse_id eq to 1*/

        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
            $this->_searchResult->setItems(array());
        } else {
            foreach ($collection as $warehouseModel) {
                $warehouseData = $warehouseModel->getData();
                if (is_null($warehouseModel->getData('warehouse_id'))) {
                    $warehouseData['warehouse_id'] = "1";
                };
                $warehouse[] = $warehouseData;
            }
        }


        $this->_searchResult->setItems($warehouse);
        $this->_searchResult->setTotalCount($collection->getSize());

        return $this->_searchResult->getOutput();
    }

    public function dummy()
    {
        $warehouse = array(
            "id" => "1",
            "name" => "Primary Location",
            "is_enable" => "1",
            "is_root" => "1",
        );

        return $warehouse;
    }
}