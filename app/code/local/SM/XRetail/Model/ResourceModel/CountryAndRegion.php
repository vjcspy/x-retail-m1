<?php

/**
 * Created by khoild@smartosc.com/mr.vjcspy@gmail.com
 * User: vjcspy
 * Date: 23/04/2016
 * Time: 11:14
 */
class SM_XRetail_Model_ResourceModel_CountryAndRegion extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var \Mage_Directory_Model_Country
     */
    private $countryModel;
    /**
     * @var \Mage_Directory_Model_Region
     */
    private $regionModel;

    /**
     * SM_XRetail_Model_ResourceModel_Country constructor.
     */
    public function __construct() {
        $this->countryModel = Mage::getModel('directory/country');
        $this->regionModel  = Mage::getModel('directory/region');
        parent::__construct();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function country() {

        $collection = $this->countryModel->getCollection();

        $searchCriteria = $this->getSearchCriteria();

        $collection->setCurPage($searchCriteria->getData('currentPage'));

        if (is_null($searchCriteria->getData('pageSize')))
            $searchCriteria->setData('pageSize', SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT);

        $collection->setPageSize($searchCriteria->getData('pageSize'));

        $items = array();
        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
            $this->getSearchResult()->setItems(array());
        }
        else
            foreach ($collection as $item) {
                /* @var $item \Mage_Directory_Model_Country */
                $items[] = new SM_XRetail_Model_Api_Data_XCountry(
                    array(
                        'country_name' => $item->getName(),
                        'country_id'   => $item->getCountryId()
                    ));
            }

        return $this->getSearchResult()
                    ->setItems($items)
                    ->setSearchCriteria($searchCriteria)
                    ->setTotalCount($collection->getSize())
                    ->getOutput();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function region() {
        $collection = $this->regionModel->getCollection();

        $searchCriteria = $this->getSearchCriteria();

        $collection->setCurPage($searchCriteria->getData('currentPage'));

        if (is_null($searchCriteria->getData('pageSize')))
            $searchCriteria->setData('pageSize', SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT);

        $collection->setPageSize($searchCriteria->getData('pageSize'));

        $items = array();

        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
            $this->getSearchResult()->setItems(array());
        }
        else
            foreach ($collection as $item) {
                /* @var $item \Mage_Directory_Model_Region */
                $items[] = new SM_XRetail_Model_Api_Data_XRegion(
                    array(
                        'country_id' => $item->getCountryId(),
                        'region_id'  => $item->getRegionId(),
                        'code'       => $item->getCode(),
                        'name'       => $item->getName(),
                    ));
            }

        return $this->getSearchResult()
                    ->setItems($items)
                    ->setSearchCriteria($searchCriteria)
                    ->setTotalCount($collection->getSize())
                    ->getOutput();
    }
}