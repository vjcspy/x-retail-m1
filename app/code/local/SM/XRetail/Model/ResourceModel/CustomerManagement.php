<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/23/16
 * Time: 10:20 PM
 */
class SM_XRetail_Model_ResourceModel_CustomerManagement extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var Mage_Customer_Model_Customer
     */
    protected $_customerModel;
    /**
     * @var \Mage_Customer_Model_Group
     */
    private $customerGroupModel;

    /**
     * SM_XRetail_Model_ResourceModel_CustomerManagement constructor.
     */
    public function __construct() {

        $this->_customerModel     = Mage::getModel('customer/customer');
        $this->customerGroupModel = Mage::getModel('customer/group');
        parent::__construct();
    }

    public function getList() {

        /* @var $collection Mage_Reports_Model_Resource_Customer_Collection */
        $collection = $this->_customerModel->getCollection();
        $collection->addAttributeToSelect('*');
        // Needed to enable filtering on name as a whole
        $collection->addNameToSelect();
        $collection->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
                   ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
                   ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
                   ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
                   ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left')
                   ->joinAttribute('company', 'customer_address/company', 'default_billing', null, 'left');

        $searchCriteria = $this->getSearchCriteria();

        $collection->setCurPage($searchCriteria->getData('currentPage'));

        if (is_null($searchCriteria->getData('pageSize')))
            $searchCriteria->setData('pageSize', SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT);
        $collection->setPageSize($searchCriteria->getData('pageSize'));

        $collection->load();

        $customers = array();

        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
            $this->_searchResult->setItems(array());
        }
        else
            foreach ($collection as $customerModel) {
                /** @var $customerModel Mage_Customer_Model_Customer $customerModel */
                $customer = new SM_XRetail_Model_Api_Data_XCustomer($customerModel->getData());
                $customer->setData('tax_class_id', $customerModel->getTaxClassId());
                $customers[] = $customer;
            }

        $this->_searchResult->setItems($customers);
        $this->_searchResult->setTotalCount($collection->getSize());

        return $this->_searchResult->getOutput();
    }

    public function getXCustomerById($customerId) {

        if (is_null($customerId))
            throw new Exception('Must have customer id');

        /* @var $collection Mage_Reports_Model_Resource_Customer_Collection */
        $collection = $this->_customerModel->getCollection();
        $collection->addAttributeToSelect('*');
        // Needed to enable filtering on name as a whole
        $collection->addNameToSelect();
        $collection->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
                   ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
                   ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
                   ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
                   ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left')
                   ->joinAttribute('company', 'customer_address/company', 'default_billing', null, 'left');

        $collection->addFieldToFilter('entity_id', $customerId);
        $customer = $collection->getFirstItem();

        if (is_null($customer))
            return null;

        $customer = new SM_XRetail_Model_Api_Data_XCustomer($customer->getData());

        return $customer->getOutput();
    }

    public function getCustomerOrders() {
        $searchCriteria = $this->getSearchCriteria();
        $customerId     = $searchCriteria->getData('customer_id');

        if (is_null($customerId))
            throw new Exception('Must have param customer_id');
        $collection = Mage::getResourceModel('sales/order_grid_collection')
                          ->addFieldToSelect('entity_id')
                          ->addFieldToSelect('increment_id')
                          ->addFieldToSelect('customer_id')
                          ->addFieldToSelect('created_at')
                          ->addFieldToSelect('grand_total')
                          ->addFieldToSelect('status')
                          ->addFieldToSelect('order_currency_code')
                          ->addFieldToSelect('store_id')
                          ->addFieldToSelect('billing_name')
                          ->addFieldToSelect('shipping_name')
                          ->addFieldToFilter('customer_id', $customerId)
                          ->setIsCustomerMode(true);

        $collection->setCurPage($searchCriteria->getData('currentPage'));

        if (is_null($searchCriteria->getData('pageSize')))
            $searchCriteria->setData('pageSize', SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT);

        $orders = array();
        foreach ($collection as $item) {
            $orders[] = $item->getData();
        }

        $this->getSearchResult()
             ->setItems($orders)
             ->setTotalCount($collection->getSize())
             ->setSearchCriteria($searchCriteria);

        return $this->getSearchResult()->getOutput();
    }

    private function _initCustomer($idFieldName = 'id') {
        $customerId = (int)$this->getRequest()->getParam($idFieldName);
        $customer   = Mage::getModel('customer/customer');

        if ($customerId) {
            $customer->load($customerId);
        }

        Mage::register('current_customer', $customer);

        return $this;
    }

    public function saveCustomer() {

        // $this->dummyData();

        $data = $this->getRequest()->getParams();

        $this->_initCustomer('customer_id');

        /** @var $customer Mage_Customer_Model_Customer */
        $customer = Mage::registry('current_customer');

        /** @var $customerForm Mage_Customer_Model_Form */
        $customerForm = Mage::getModel('customer/form');
        $customerForm->setEntity($customer)
                     ->setFormCode('adminhtml_customer')
                     ->ignoreInvisible(false);

        $formData = $customerForm->extractData($this->getRequest(), 'account');

        // Handle 'disable auto_group_change' attribute
        if (isset($formData['disable_auto_group_change'])) {
            $formData['disable_auto_group_change'] = empty($formData['disable_auto_group_change']) ? '0' : '1';
        }

        $customerForm->compactData($formData);

        // Unset template data
        if (isset($data['address']['_template_'])) {
            unset($data['address']['_template_']);
        }

        $modifiedAddresses = array();
        if (!empty($data['address'])) {
            /** @var $addressForm Mage_Customer_Model_Form */
            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('adminhtml_customer_address')->ignoreInvisible(false);

            foreach (array_keys($data['address']) as $index) {
                $address = $customer->getAddressItemById($index);
                if (!$address) {
                    $address = Mage::getModel('customer/address');
                }

                $requestScope = sprintf('address/%s', $index);
                $formData     = $addressForm->setEntity($address)
                                            ->extractData($this->getRequest(), $requestScope);

                // Set default billing and shipping flags to address
                $isDefaultBilling = isset($data['account']['default_billing'])
                    && $data['account']['default_billing'] == $index;
                $address->setIsDefaultBilling($isDefaultBilling);
                $isDefaultShipping = isset($data['account']['default_shipping'])
                    && $data['account']['default_shipping'] == $index;
                $address->setIsDefaultShipping($isDefaultShipping);

                $errors = $addressForm->validateData($formData);
                if ($errors !== true) {
                    $er = '';
                    foreach ($errors as $error) {
                        $er .= $error;
                    }
                    throw new Exception($er);
                }

                $addressForm->compactData($formData);

                // Set post_index for detect default billing and shipping addresses
                $address->setPostIndex($index);

                if ($address->getId()) {
                    $modifiedAddresses[] = $address->getId();
                }
                else {
                    $customer->addAddress($address);
                }
            }
        }

        // Default billing and shipping
        if (isset($data['account']['default_billing'])) {
            $customer->setData('default_billing', $data['account']['default_billing']);
        }
        if (isset($data['account']['default_shipping'])) {
            $customer->setData('default_shipping', $data['account']['default_shipping']);
        }
        if (isset($data['account']['confirmation'])) {
            $customer->setData('confirmation', $data['account']['confirmation']);
        }

        // Mark not modified customer addresses for delete
        foreach ($customer->getAddressesCollection() as $customerAddress) {
            if ($customerAddress->getId() && !in_array($customerAddress->getId(), $modifiedAddresses)) {
                $customerAddress->setData('_deleted', true);
            }
        }


        $isNewCustomer = $customer->isObjectNew();

        $sendPassToEmail = false;
        // Force new customer confirmation
        if ($isNewCustomer) {
            $customer->setPassword($data['account']['password']);
            $customer->setForceConfirmed(true);
            if ($customer->getPassword() == 'auto') {
                $sendPassToEmail = true;
                $customer->setPassword($customer->generatePassword());
            }
        }

        Mage::dispatchEvent(
            'adminhtml_customer_prepare_save',
            array(
                'customer' => $customer,
                'request'  => $this->getRequest()
            ));

        $customer->save();

        // Send welcome email
        if ($customer->getWebsiteId() && (isset($data['account']['sendemail']) || $sendPassToEmail)) {
            $storeId = $customer->getSendemailStoreId();
            if ($isNewCustomer) {
                $customer->sendNewAccountEmail('registered', '', $storeId);
            }
            elseif ((!$customer->getConfirmation())) {
                // Confirm not confirmed customer
                $customer->sendNewAccountEmail('confirmed', '', $storeId);
            }
        }

        if (!empty($data['account']['new_password'])) {
            $newPassword = $data['account']['new_password'];
            if ($newPassword == 'auto') {
                $newPassword = $customer->generatePassword();
            }
            $customer->changePassword($newPassword);
            $customer->sendPasswordReminderEmail();
        }

        return $this->getXCustomerById($customer->getEntityId());
    }

    public function getCustomerGroup() {
        $collection     = $this->customerGroupModel->getCollection();
        $searchCriteria = $this->getSearchCriteria();

        $collection->setCurPage($searchCriteria->getData('currentPage'));

        if (is_null($searchCriteria->getData('pageSize')))
            $searchCriteria->setData('pageSize', SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT);
        $collection->setPageSize($searchCriteria->getData('pageSize'));

        $items = array();

        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
            $this->getSearchResult()->setItems(array());
        }
        else {
            foreach ($collection as $item) {
                $items[] = $item->getData();
            }
        }

        return $this->getSearchResult()
                    ->setItems($items)
                    ->setSearchCriteria($searchCriteria)
                    ->setTotalCount($collection->getSize())
                    ->getOutput();
    }

    private function dummyData() {
        $data = array(
            'account' =>
                array(
                    'website_id'       => '0',
                    'group_id'         => '1',
                    'prefix'           => '',
                    'firstname'        => 'Fsdf',
                    'middlename'       => '',
                    'lastname'         => 'gdfg',
                    'suffix'           => '',
                    'email'            => 'saa1aa23@gsdf.com',
                    'dob'              => '',
                    'taxvat'           => '',
                    'gender'           => '',
                    'password'         => '12345678',
                    'default_billing'  => '_item1',
                    'default_shipping' => '_item1',
                ),
            'address' =>
                array(
                    '_item1' =>
                        array(
                            'prefix'     => '',
                            'firstname'  => 'Fsdf',
                            'middlename' => '',
                            'lastname'   => 'gdfg',
                            'suffix'     => '',
                            'company'    => '',
                            'street'     =>
                                array(
                                    0 => 'sdfsdfa',
                                    1 => '',
                                ),
                            'city'       => 'dsfsdf',
                            'country_id' => 'US',
                            'region_id'  => '16',
                            'region'     => '',
                            'postcode'   => 'df2dfdsf',
                            'telephone'  => 'sdfdsfsdf',
                            'fax'        => '',
                            'vat_id'     => '',
                        ),
                ),
        );
        $this->getRequest()->setParams($data);
    }
}
