<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 06/04/2016
 * Time: 09:58
 */
class SM_XRetail_Model_ResourceModel_RealTimeManagement {

    const CACHE_TAG = 'xRetail_RealTime-Management';
    const PREFIX = 'x-retail-real-time-update';

    const TYPE_PRODUCT_NEW = 'product_new';
    const TYPE_PRODUCT_CHANGE = 'product_change';
    const TYPE_PRODUCT_DELETE = 'product_delete';
    const TYPE_CUSTOMER_NEW = 'customer_new';
    const TYPE_CUSTOMER_CHANGE = 'customer_change';
    const TYPE_CUSTOMER_DELETE = 'customer_delete';
    const TYPE_WAREHOUSE_PRODUCT_CHANGE = 'warehouse_product_change';
    const TYPE_COUPON = 'coupon';
    /**
     * @var Zend_Cache_Core
     */
    private $mageCache;
    /**
     * @var SM_XRetail_Model_ResourceModel_RealTimeManagement_Trigger
     */
    private $_trigger;
    /**
     * @var array
     */
    private $_dataReloadedProduct = array();
    private $_dataRemoveObject;
    private $_dataReloadedCustomer = array();
    private $_dataReloadedStock = array();
    /**
     * @var SM_XRetail_Helper_Data
     */
    private $_helper;
    /**
     * @var SM_XRetail_Helper_Realtime_CacheManagement
     */
    private $_xCache;
    /**
     * @var Mage_Catalog_Model_Product
     */
    private $_productModel;
    /**
     * @var Mage_Customer_Model_Customer
     */
    private $_customerModel;

    /**
     * SM_XRetail_Model_ResourceModel_RealTimeManagement constructor.
     */
    public function __construct() {

        $this->_productModel  = Mage::getModel('catalog/product');
        $this->_customerModel = Mage::getModel('customer/customer');
        $this->_xCache        = Mage::helper('xretail/realtime_cacheManagement');
        $this->_helper        = Mage::helper('xretail');
        $this->mageCache      = Mage::app()->getCache();
        $this->_trigger       = Mage::getModel('xretail/resourceModel_realTimeManagement_trigger');
    }


    /**
     * @param $object
     * @param $type
     *
     * @return $this
     */
    public function needReloadRealTime($object, $type) {
        if (!in_array(
            $type,
            array(
                self::TYPE_PRODUCT_NEW,
                self::TYPE_PRODUCT_CHANGE,
                self::TYPE_CUSTOMER_NEW,
                self::TYPE_CUSTOMER_CHANGE,
                self::TYPE_WAREHOUSE_PRODUCT_CHANGE,
                self::TYPE_COUPON))
        )
            return false;

        // store data
        $this->store($object, $type);
        // trigger client
        $this->_trigger->triggerApp();

        return $this;
    }

    /**
     * @param $object
     * @param $type
     *
     * @return bool
     */
    public function storeRemoveObject($object, $type) {

        if (!in_array($type, array(self::TYPE_PRODUCT_DELETE, self::TYPE_CUSTOMER_DELETE, self::TYPE_COUPON)))
            return false;
        $removeType = array('X-retail-update-real-time', 'remove-object');
        $o          = $this->getFromCache($removeType);
        if (!isset($o[$type]))
            $o[$type] = array();
        $o[$type][] = is_array($object) ? $object['id'] : $object;

        // trigger client
        $this->_trigger->triggerApp();

        return $this->saveToCache(json_encode($o), $removeType);
    }

    /**
     * @param $type
     *
     * @return bool|mixed
     */
    public function getRemoveObject($type) {

        if (is_null($this->_dataRemoveObject)) {
            $removeType              = array('X-retail-update-real-time', 'remove-object');
            $this->_dataRemoveObject = $this->getFromCache($removeType);
            $this->mageCache->remove($this->getCacheKey(array(self::PREFIX, $removeType)));
        }

        return isset($this->_dataRemoveObject[$type]) ? $this->_dataRemoveObject[$type] : false;
    }

    /**
     * TODO: get data change/removed product
     *
     * @return array
     */
    public function reload() {

        $dataReload = array();
        if ($this->reloadProduct(true) || $this->reloadProduct() || $this->getRemoveObject(self::TYPE_PRODUCT_DELETE)) {
            $dataReload['product'] = array(
                'new'     => $this->reloadProduct(true),
                'change'  => $this->reloadProduct(),
                'remove' => $this->getRemoveObject(self::TYPE_PRODUCT_DELETE)
            );
        }

        $warehouseProduct = $this->reloadWarehouseProducts();
        if(count($warehouseProduct)){
            $dataReload['warehouse_product'] = array(
                'new'     => array(),
                'change' => $warehouseProduct,
                'remove' => array()
            );
        }

        if ($this->reloadCustomer() || $this->reloadCustomer(true) || $this->getRemoveObject(self::TYPE_CUSTOMER_DELETE))
            $dataReload['customer'] = array(
                'new'     => $this->reloadCustomer(true),
                'change' => $this->reloadCustomer(),
                'remove' => $this->getRemoveObject(self::TYPE_CUSTOMER_DELETE)
            );

        return $dataReload;
    }

    /**
     * TODO: get data new Customers
     *
     * @param $getNew
     *
     * @return array|bool
     */
    protected function reloadCustomer($getNew = false) {

        if (($getNew && !isset($this->_dataReloadedCustomer['new'])) || (!$getNew && !isset($this->_dataReloadedCustomer['change']))) {
            if (!$getNew)
                $c = $this->getFromCache(self::TYPE_CUSTOMER_CHANGE);
            else
                $c = $this->getFromCache(self::TYPE_CUSTOMER_NEW);

            if (empty($c))
                return false;

            $c = array_values($c);

            if (!$getNew) {
                $this->updateListObjectFinished($c, self::TYPE_CUSTOMER_CHANGE);
                $this->_dataReloadedCustomer['change'] = $this->collectDataCustomer($c);
            }
            else {
                $this->updateListObjectFinished($c, self::TYPE_CUSTOMER_NEW);
                $this->_dataReloadedCustomer['new'] = $this->collectDataCustomer($c);
            }
        }
        if (!$getNew)
            return $this->_dataReloadedCustomer['change'];
        else
            return $this->_dataReloadedCustomer['new'];
    }

    /**
     * TODO: get all data customer from array
     *
     * @param $customers
     *
     * @return array
     * @throws \Mage_Core_Exception
     */
    private function collectDataCustomer($customers) {
        $dataCustomers = array();
        /* @var $collection Mage_Reports_Model_Resource_Customer_Collection */
        $collection = $this->_customerModel->getCollection();
        $collection->addAttributeToSelect('*');
        // Needed to enable filtering on name as a whole
        $collection->addNameToSelect();
        $collection->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
                   ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
                   ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
                   ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
                   ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left')
                   ->joinAttribute('company', 'customer_address/company', 'default_billing', null, 'left');

        $collection->addFieldToFilter('entity_id', array('in' => $customers));

        $collection->load();


        foreach ($collection as $customerModel) {
            /** @var $customerModel Mage_Customer_Model_Customer $customerModel */
            $customer = new SM_XRetail_Model_Api_Data_XCustomer($customerModel->getData());
            $customer->setData('tax_class_id', $customerModel->getTaxClassId());
            $dataCustomers[] = $customer->getOutput();
        }

        return $dataCustomers;
    }

    /**
     * @param bool $getNew
     *
     * @return array|bool
     */
    protected function reloadProduct($getNew = false) {

        if (($getNew && !isset($this->_dataReloadedProduct['new'])) || (!$getNew && !isset($this->_dataReloadedProduct['change']))) {
            if (!$getNew)
                $products = ($this->getFromCache(self::TYPE_PRODUCT_CHANGE));
            else
                $products = ($this->getFromCache(self::TYPE_PRODUCT_NEW));

            if (empty($products))
                return false;

            $products = array_values($products);

            $data = $this->collectDataProduct($products);

            $this->_xCache->updateProducts($data);

            if ($getNew) {
                $this->_dataReloadedProduct['new'] = $data;
                $this->updateListObjectFinished($products, self::TYPE_PRODUCT_NEW);
            }
            else {
                $this->_dataReloadedProduct['change'] = $data;
                $this->updateListObjectFinished($products, self::TYPE_PRODUCT_CHANGE);
            }

        }

        if ($getNew)
            return $this->_dataReloadedProduct['new'];
        else
            return $this->_dataReloadedProduct['change'];
    }

    protected function reloadWarehouseProducts() {

        $warehouseProducts = ($this->getFromCache(self::TYPE_WAREHOUSE_PRODUCT_CHANGE));

        $this->_dataReloadedStock = $warehouseProducts;

        $this->updateListObjectFinished($warehouseProducts, self::TYPE_WAREHOUSE_PRODUCT_CHANGE);

        return $this->_dataReloadedStock;
    }

    /**
     * TODO: get all data product
     *
     * @param $products
     *
     * @return array
     * @throws \Mage_Core_Exception
     */
    private function collectDataProduct($products) {
        $dataProducts = array();
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $this->_productModel->getCollection();

        $collection->addAttributeToSelect('*');
        $collection->joinAttribute(
            'status',
            'catalog_product/status',
            'entity_id',
            null,
            'inner'
        );
        $collection->joinAttribute(
            'visibility',
            'catalog_product/visibility',
            'entity_id',
            null,
            'inner'
        );

        $collection->getSelect()
                   ->columns("GROUP_CONCAT(n.website_id SEPARATOR ',') AS website_ids")
                   ->joinLeft(array('n' => 'catalog_product_website'), 'n.product_id = e.entity_id')
                   ->group('e.entity_id');

        $collection->getSelect()
                   ->columns("GROUP_CONCAT(r.category_id SEPARATOR ',') AS category_id")
                   ->joinLeft(array('r' => 'catalog_category_product'), 'r.product_id = e.entity_id', 'r.position')
                   ->group('e.entity_id');

        $collection->addFieldToFilter('entity_id', array('in' => $products));

        $collection->load();


        foreach ($collection as $item) {
            /*thinking: Nếu đã đi vào đây thì việc get từ cache ra không còn ý nghĩa với các data đã có trong $item*/
            $xProduct = new SM_XRetail_Model_Api_Data_XProduct($item->getData());

            /*do: get custom_options*/
            //if ($item->getTypeId() == 'simple') {
                $item = Mage::getModel('catalog/product')->load($item->getId());
                $xProduct->addData($item->getData());
            //}

            // get options
            $xProduct->setData('x_options', $this->_xCache->getXOptions($item));

            // get stock_items
            $xProduct->setData('stock_items', $this->_xCache->getStockItem($item)->getData());

            $dataProducts[] = $xProduct->getOutput();

        }

        return $dataProducts;
    }

    protected function reloadCoupon() {

    }

    /**
     * Check Object type to save cache
     *
     * @param $object
     * @param $type
     *
     * @return $this
     */
    protected function store($object, $type) {
        if (in_array($type, array(self::TYPE_CUSTOMER_NEW, self::TYPE_CUSTOMER_CHANGE, self::TYPE_PRODUCT_NEW, self::TYPE_PRODUCT_CHANGE, self::TYPE_WAREHOUSE_PRODUCT_CHANGE))) {
            $o      = $this->getFromCache($type);
            $id     = is_array($object) ? $object['id'] : $object;
            $o[$id] = $id;

            if($type == self::TYPE_WAREHOUSE_PRODUCT_CHANGE){
                $this->saveToCache($object, $type);
            }else{
                $this->saveToCache($o, $type);
            }
        }
        else
            $this->saveToCache("NEW", $type);

        return $this;
    }

    /**
     * Get reload data from cache
     *
     * @param $type
     *
     * @return array|mixed
     */
    protected function getFromCache($type) {
        return ($c = $this->mageCache->load($this->getCacheKey(array(self::PREFIX, $type)))) === false ? array() : json_decode($c, true);
    }

    /**
     * Save reload data to cache
     *
     * @param $data
     * @param $type
     *
     * @return bool
     */
    protected function saveToCache($data, $type) {

        if (is_array($data))
            $data = json_encode($data);

        return $this->mageCache->save(
            $data,
            $this->getCacheKey(array(self::PREFIX, $type)),
            array(self::CACHE_TAG),
            SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract::XCACHE_LIFE_TIME);

    }

    /**
     * Reset all cache data real time
     * Thường sẽ không dùng cái này, bởi vì trong trường hợp đang generate new data thì lại có object update
     *
     * @return bool
     */
    protected function resetRealTime() {

        return $this->mageCache->clean('matchingTag', array(self::CACHE_TAG));
    }

    /**
     * @param $dataFinished
     * @param $type
     *
     * @return bool|SM_XRetail_Model_ResourceModel_RealTimeManagement
     */
    protected function updateListObjectFinished($dataFinished, $type) {

        if (!in_array(
            $type,
            array(
                self::TYPE_PRODUCT_NEW,
                self::TYPE_PRODUCT_CHANGE,
                self::TYPE_CUSTOMER_NEW,
                self::TYPE_CUSTOMER_CHANGE,
                self::TYPE_WAREHOUSE_PRODUCT_CHANGE,
                self::TYPE_COUPON
            ))
        )
            return false;

        $objectReload = $this->getFromCache($type);

        if (is_array($objectReload)) {
            foreach ($dataFinished as $o) {
                $id = is_array($o) ? $o['id'] : $o;
                unset($objectReload[$id]);
            }

            return $this->saveToCache($objectReload, $type);
        }
        else
            return false;

    }

    /**
     * Get key for cache
     *
     * @param array $data
     *
     * @return string
     */
    protected function getCacheKey($data) {

        $serializeData = array();
        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $serializeData[$key] = $value->getId();
            }
            else {
                $serializeData[$key] = $value;
            }
        }

        return md5(serialize($serializeData));
    }

    public function prepareRealtimeProductWarehouse($productId){
        $warehouseProducts =  Mage::helper('xretail/integrate')->getWarehouseProduct($productId, array());
        Mage::getModel('xretail/resourceModel_realTimeManagement')->needReloadRealTime($warehouseProducts,SM_XRetail_Model_ResourceModel_RealTimeManagement::TYPE_WAREHOUSE_PRODUCT_CHANGE);
    }
}