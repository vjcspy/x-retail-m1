<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/10/16
 * Time: 9:32 AM
 */
class SM_XRetail_Model_ResourceModel_Config extends SM_XRetail_Model_Api_ServiceAbstract {

    protected $_apiConfig;

    public function __construct() {

        $this->_apiConfig = Mage::getModel('xretail/api_configuration');
        parent::__construct();
    }

    public function show() {

        $searchCriteria = $this->getSearchCriteria();
        $path           = $searchCriteria->getData('path');
        if (is_null($path))
            throw new Exception('Require field searchCriteria[path]');

        if (is_array($path)) {
            $data = array();
            foreach ($path as $item) {
                $data[$item] = $this->_apiConfig->getStoreConfig($item, $searchCriteria->getData('store'));
            }

            return $data;
        }
        else
            return $this->_apiConfig->getStoreConfig($path, $searchCriteria->getData('store'));
    }

    public function update() {

        // $this->dummy();

        $configData = $this->getRequest()->getParam('configData');

        foreach ($configData as $config) {
            $v = $config['value'];
            $n = $config['name'];

            if ($config['name'] == 'integration.magestore_warehouse' && $config['value']) {
                if(!Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()){
                    throw new Exception(' Magestore Inventory extension is not available to integrate!');
                }
            }

            $this->_apiConfig->setConfig('xretail/config/' . $n, $v);
        }

        return $configData;
    }

    private function dummy() {

        $data = array(
            array(
                'name'  => 'test1',
                'value' => 'value 1'
            ),
            array(
                'name'  => 'test2',
                'value' => 'value 2'
            )
        );

        $this->getRequest()->setParam('configData', $data);
    }
}
