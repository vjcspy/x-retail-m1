<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 11:10 AM
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderOnline extends SM_XRetail_Model_Api_ServiceAbstract {

    const DISCOUNT_WHOLE_ORDER = 'discount_whole_order';

    /**
     * @var bool
     * Flag condition to collect rule
     */
    static $IS_COLLECT_RULE = true;

    /**
     * @var bool
     */
    static $IS_ALLOW_CHECK_OUT_BACK_ORDER = false;

    /**
     * flag to ensure quote not use again
     */
    const RELOAD_IF_ERROR = false;

    /**
     * @var SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Items
     */
    protected $_blockItems;

    /**
     * @var SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_ShippingMethod
     */
    protected $_blockShippingMethod;

    /**
     * @var SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_PaymentMethod
     */
    protected $_blockPaymentMethod;

    /**
     * @var SM_XRetail_Model_ResourceModel_XRetailOrderOnline_Block_Totals
     */
    protected $_blockTotals;

    /**
     * @var \SM_XRetail_Helper_DiscountPerItems_Data
     */
    protected $_discountPerItemHelper;
    /**
     * @var \SM_XRetail_Helper_Realtime_Data
     */
    private $realtimeHelperData;
    /**
     * @var \SM_XRetail_Model_ResourceModel_XRetailOrderRefund
     */
    private $retailOrderRefund;
    /**
     * @var \SM_XRetail_Model_ResourceModel_XRetailOrderInvoice
     */
    private $retailOrderInvoice;
    /**
     * @var \SM_XRetail_Helper_CustomSales_Data
     */

    private $retailOrderShipment;
    /**
     * @var \SM_XRetail_Helper_CustomSales_Data
     */

    /**
     * @var Mage_Core_Helper_Abstract
     */
    private $_integrate;

    /**
     * @var \Mage_Core_Helper_Abstract|\SM_XRetail_Helper_CustomSales_Data
     */
    private $_customerSalesHelper;

    /**
     * @var \Mage_Sales_Model_Order
     */
    private $_currentOrder;
    /**
     * payment data before exchange
     *
     * @var
     */
    private $_oldPaymentData;
    /**
     * @var \Mage_Core_Helper_Abstract|\SM_XRetail_Helper_Data
     */
    private $retailHelper;

    /**
     * SM_XRetail_Model_ResourceModel_XRetailOrderOnline constructor.
     */
    public function __construct() {

        $this->_blockItems            = Mage::getModel('xretail/resourceModel_xRetailOrderOnline_block_items');
        $this->_blockShippingMethod   = Mage::getModel('xretail/resourceModel_xRetailOrderOnline_block_shippingMethod');
        $this->_blockPaymentMethod    = Mage::getModel('xretail/resourceModel_xRetailOrderOnline_block_paymentMethod');
        $this->_blockTotals           = Mage::getModel('xretail/resourceModel_xRetailOrderOnline_block_totals');
        $this->_discountPerItemHelper = Mage::helper('xretail/discountPerItems_data');
        $this->realtimeHelperData     = Mage::helper('xretail/realtime_data');
        $this->retailOrderRefund      = Mage::getModel('xretail/resourceModel_xRetailOrderRefund');
        $this->retailOrderInvoice     = Mage::getModel('xretail/resourceModel_xRetailOrderInvoice');
        $this->retailOrderShipment    = Mage::getModel('xretail/resourceModel_xRetailOrderShipment');
        $this->_customerSalesHelper   = Mage::helper('xretail/customSales_data');
        $this->_integrate             = Mage::helper('xretail/integrate');
        $this->retailHelper           = Mage::helper('xretail');
        parent::__construct();
    }

    /**
     * XRetail Index
     *
     * @return array
     */
    public function index() {

        // $this->dummyData();
        $this->_initSession();

        return $this->getDataBlockOutput(
            [
                'items',
            ]
        );
    }

    /**
     * XRetail load block
     *
     * @return array
     * @throws \Exception
     */
    public function loadBlock() {

        //$this->dummyData();

        // check default shipping method and custom sales && split items;
        $this->checkShippingMethod()->checkCustomSalesItem()->checkSplitItem()->checkBackOrder()->checkDiscountWholeOrder();

        try {
            $this->_initSession()
                 ->_processActionData();
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            if (self::RELOAD_IF_ERROR)
                $this->_reloadQuote();
            else
                $this->clear();
        }

        if (isset($message))
            throw new Exception($e->getMessage());

        return $this->getDataBlockOutput(
            [
                'items',
                'totals'
            ]
        );
    }

    /**
     * TO use default shipping method
     *
     * @return $this
     */
    private function checkShippingMethod() {

        $order = $this->getRequest()->getParam('order');
        if (!isset($order['shipping_method'])) {
            $order['shipping_method'] = 'xretail_shipping_default_shipping_xretail';
            $this->getRequest()->setParam('order', $order);
        }
        elseif (in_array($order['shipping_method'], ['xpos_shipping', 'xretail_shipping_xretail_dummy_1_code'])) {
            $order['shipping_method'] = 'xretail_shipping_xretail_dummy_1_code';
            $this->getRequest()->setParam('order', $order);
            if (isset($order['shipping_amount'])) {
                $this->realtimeHelperData->register(SM_XRetail_Model_Shipping_XretailCarrier::XRETAIL_SHIPPING_AMOUNT, $order['shipping_amount']);
            }
        }

        return $this;
    }

    /**
     * @return $this
     * Add addition data for split items
     */
    private function checkSplitItem() {
        $items = $this->getRequest()->getParam('items');
        $dc    = [];
        if ($items && is_array($items)) {
            foreach ($items as $item) {
                if (!isset($dc[$item['product_id']]))
                    $dc[$item['product_id']] = 1;
                else {
                    $dc[$item['product_id']]++;
                    $iz = true;
                }
            }
        }
        if (isset($iz)) {
            foreach ($dc as $id => $c) {
                if ($c > 1)
                    $this->addAdditionDataSplitItem($items, $id);
            }
            $this->getRequest()->setParam('items', $items);
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function checkBackOrder() {
        if ($this->getRequest()->getParam('allow_out_of_stock') == 1 || self::$IS_ALLOW_CHECK_OUT_BACK_ORDER) {
            self::$IS_ALLOW_CHECK_OUT_BACK_ORDER = true;
            Mage::helper('catalog/product')->setSkipSaleableCheck(true);
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function checkDiscountWholeOrder() {
        $order = $this->getRequest()->getParam('order');
        if (isset($order['whole_order_discount'])
            && isset($order['whole_order_discount']['value'])
            && $order['whole_order_discount']['value'] > 0
        ) {
            self::$IS_COLLECT_RULE = true;
            $this->retailHelper->register(self::DISCOUNT_WHOLE_ORDER, $order['whole_order_discount']);
        }
        else
            $this->retailHelper->register(self::DISCOUNT_WHOLE_ORDER, false);

        return $this;
    }

    /**
     * @return $this
     */
    private function checkCustomSalesItem() {
        $items = $items = $this->getRequest()->getParam('items');
        if (!!$items && is_array($items)) {
            foreach ($items as &$item) {
                if ($item['product_id'] == 'custom_sale') {
                    $iz                 = true;
                    $item['product_id'] = $this->_customerSalesHelper->getCustomSalesId();
                }
            }
        }
        if (isset($iz) && $iz === true)
            $this->getRequest()->setParam('items', $items);

        return $this;
    }

    private function addAdditionDataSplitItem(&$items, $id) {
        foreach ($items as $k => $item) {
            if ($item['product_id'] == $id) {
                $items[$k]['split_product'] = [
                    'label' => 'X-Retail Split Item',
                    'value' => $k
                ];
            }
        }

        return $items;
    }

    /**
     * API SAVE ORDER
     *
     * @return array|string
     */
    public function saveOrder() {

        $this->loadBlock();

        $order = $this->_getOrderCreateModel()
                      ->setIsValidate(true)
                      ->createOrder();

        // add retail mode to order
        $this->addDataModeToOrder($order);

        $this->_getSession()->clear();

        $this->createShipment($order);

        return $this->outputOrder($order);
    }

    public function prepareShipmentData($orderData, $order) {
        $shippingData = [];

        $shippingData['order_id']     = $order->getId();
        $shippingData['warehouse_id'] = $order->getWarehouseId();

        $shippingData['shipment']['items'] = [];
        foreach ($orderData['items'] as $item) {
            $shippingData['shipment']['items'][$item['id']] = $item['qty'];
        }
        $shippingData['shipment']['comment_text'] = "Shipment from X-Retail!";

        return $shippingData;
    }

    public function saveOrderAndInvoice() {
        $this->loadBlock();

        $this->_currentOrder = $order = $this->_getOrderCreateModel()
                                             ->setIsValidate(true)
                                             ->createOrder();

        // add retail mode to order
        $this->addDataModeToOrder($order);

        $this->_getSession()->clear();

        $this->createShipment($order);

        $orderCreatedData = $order->getData();

        $orderData    = $this->getRequest()->getParam('order');
        $purchaseType = $this->getRequest()->getParam('purchase_type');

        if ($orderData['payment_method'] == SM_XRetail_Model_Payment_XRetailMultiplePayment::CODE
            && $purchaseType == SM_XRetail_Model_Payment_XRetailMultiplePayment::PURCHASE_TYPE
        ) {
            $data = [
                'order'   => [
                    'order_id' => $orderCreatedData['entity_id'],
                    // 'layaway_comment' => 'su dung layaway'
                ],
                'payment' => [],
            ];
            $this->getRequest()->setParam('layaway_data', $data);
            $this->retailOrderInvoice->updateLayaway();
        }
        else {
            $data = [
                'order_id' => $orderCreatedData['entity_id'],
                'invoice'  =>
                    [
                        'items'        =>
                            [// Nếu truyền lên mảng rỗng thì sẽ invoice toàn bộ items
                            ],
                        'comment_text' => '',
                    ],
            ];
            $this->getRequest()->setParam('invoice_data', $data);
            $this->retailOrderInvoice->invoiceOrder();
        }


        return $this->outputOrder($order);
    }

    /**
     *  Create shipment for order
     *
     * @param $orderData
     * @param $order
     */
    protected function createShipment($order) {
        $orderData = $this->getRequest()->getParams();

        if (!$orderData['has_shipment']) {
            $shipmentData = $this->prepareShipmentData($orderData, $order);

            $this->getRequest()->setParam('shipment_data', $shipmentData);

            $this->retailOrderShipment->createShipment();
        }

    }

    /**
     * Process request data with additional logic for saving quote and creating order
     *
     * @return $this
     * @throws Exception
     */
    protected function _processActionData() {

        if ($this->_dataConfig->getOrderCreateAllowEvent()) {
            $eventData = [
                'order_create_model' => $this->_getOrderCreateModel(),
                'request_model'      => $this->getRequest(),
                'session'            => $this->_getSession(),
            ];

            Mage::dispatchEvent('adminhtml_sales_order_create_process_data_before', $eventData);
        }

        if ($data = $this->getRequest()->getParam('order')) {
            $this->_getOrderCreateModel()->importPostData($data);
        }

        if ($warehouseId = $this->getRequest()->getParam('warehouse_id')) {
            $this->_getOrderCreateModel()->getQuote()->setWarehouseId($warehouseId);
        }

        if (self::$IS_COLLECT_RULE)
            $this->_getOrderCreateModel()->initRuleData();

        $this->_getOrderCreateModel()->getBillingAddress();

        if (!$this->_getOrderCreateModel()->getQuote()->isVirtual()) {
            $syncFlag       = $this->getRequest()->getPost('shipping_as_billing');
            $shippingMethod = $this->_getOrderCreateModel()->getShippingAddress()->getShippingMethod();
            if (is_null($syncFlag)
                && $this->_getOrderCreateModel()->getShippingAddress()->getSameAsBilling()
                && empty($shippingMethod)
            ) {
                $this->_getOrderCreateModel()->setShippingAsBilling(1);
            }
            else {
                $this->_getOrderCreateModel()->setShippingAsBilling((int)$syncFlag);
            }
        }

        if (!$this->_getOrderCreateModel()->getQuote()->isVirtual() && $this->getRequest()->getPost('reset_shipping')) {
            $this->_getOrderCreateModel()->resetShippingMethod(true);
        }

        if ($this->getRequest()->has('items')) {
            $items = $this->getRequest()->getParam('items');
            $items = $this->_processFiles($items);
            $this->_getOrderCreateModel()->addProducts($items);
        }

        if ($this->_dataConfig->getOrderCreateAllowEvent()) {
            $eventData = [
                'order_create_model' => $this->_getOrderCreateModel(),
                'request'            => $this->getRequest()->getPost(),
            ];

            Mage::dispatchEvent('adminhtml_sales_order_create_process_data', $eventData);
        }

        // Discount per item
        $this->_discountPerItemHelper->processData($this->getRequest()->getParam('items'));

        $this->_getOrderCreateModel()
             ->getShippingAddress()
             ->setLimitCarrier([SM_XRetail_Model_Shipping_XretailCarrier::CODE])
             ->setCollectShippingRates(true);

        if (isset($data['payment_data']) && $data['payment_method'] == 'xretail_multiple_payment') {
            $data['payment_data']['store_id']   = $this->getRequest()->getParam('store_id');
            $data['payment_data']['check_date'] = date('Y-m-d H:i:s');
            $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($data['payment_data']);
            $this->_getOrderCreateModel()->setPaymentData($data['payment_data']);
        }
        else
            $this->_getOrderCreateModel()->collectShippingRates();

        $this->_getQuote()->save();

        $giftmessages = $this->getRequest()->getPost('giftmessage');
        if ($giftmessages) {
            $this->_getGiftmessageSaveModel()->setGiftmessages($giftmessages)
                 ->saveAllInQuote();
        }

        /*Add coupon code*/
        $data       = $this->getRequest()->getParam('order');
        $couponCode = '';
        if (isset($data) && isset($data['coupon']['code'])) {
            $couponCode = trim($data['coupon']['code']);
        }
        if (!empty($couponCode))
            if ($this->_getQuote()->getCouponCode() !== $couponCode)
                throw new Exception($couponCode . ' coupon code is not valid.');


        return $this;
    }

    /**
     * @return $this
     */
    protected function removeAllItems() {

        //remove all items
        $quoteItems = $this->_getOrderCreateModel()->getQuote()->getAllItems();
        foreach ($quoteItems as $item) {
            $this->_getOrderCreateModel()->getQuote()->removeItem($item->getId());
        }

        return $this;
    }

    /**
     * Process buyRequest file options of items
     *
     * @param array $items
     *
     * @return array
     */
    protected function _processFiles($items) {

        /* @var $productHelper Mage_Catalog_Helper_Product */
        $productHelper = Mage::helper('catalog/product');
        foreach ($items as $id => $item) {
            $buyRequest = new Varien_Object($item);
            $params     = ['files_prefix' => 'item_' . $id . '_'];
            $buyRequest = $productHelper->addParamsToBuyRequest($buyRequest, $params);
            if ($buyRequest->hasData()) {
                $items[$id] = $buyRequest->toArray();
            }
        }

        return $items;
    }

    /**
     * @return $this
     */
    protected function _reloadQuote() {

        $id = $this->_getQuote()->getId();
        $this->_getQuote()->load($id);

        return $this;
    }

    /**
     * @return Mage_Core_Model_Session_Abstract_Varien
     */
    public function clear() {

        return $this->_getSession()->clear();
    }


    /**
     * Retrieve block data
     *
     * @param array $blockData
     *
     * @return array
     */
    protected function getDataBlockOutput($blockData = ['items', 'shipping_method', 'billing_method', 'totals']) {

        $dataBlockOutput = [];
        if (in_array('items', $blockData))
            $dataBlockOutput['items'] = $this->_blockItems->getOutput();
        if (in_array('shipping_method', $blockData))
            $dataBlockOutput['shipping_method'] = $this->_blockShippingMethod->getOutput();
        if (in_array('billing_method', $blockData))
            $dataBlockOutput['billing_method'] = $this->_blockPaymentMethod->getOutput();
        if (in_array('totals', $blockData))
            $dataBlockOutput['totals'] = $this->_blockTotals->getOutput();

        $dataBlockOutput['customer'] = $this->_getSession()->getQuote()->getCustomer()->getData();

        return $dataBlockOutput;
    }

    /**
     * @return $this
     */
    protected function _initSession() {

        $this->clear();

        /**
         * Identify customer
         */

        if ($customerId = $this->getRequest()->getParam('customer_id')) {
            $this->_getSession()->setCustomerId((int)$customerId);
        }

        /**
         * Identify store
         */
        if ($storeId = $this->getRequest()->getParam('store_id')) {
            $this->_getSession()->setStoreId((int)$storeId);
        }

        /**
         * Identify currency
         */
        if ($currencyId = $this->getRequest()->getParam('currency_id')) {
            $this->_getSession()->setCurrencyId((string)$currencyId);
            $this->_getOrderCreateModel()->setRecollect(true);
        }

        if (!is_null(Mage::registry('from_api')))
            Mage::unregister('from_api');
        Mage::register('from_api', true);

        //Notify other modules about the session quote
        if ($this->_dataConfig->getOrderCreateAllowEvent())
            Mage::dispatchEvent(
                'create_order_session_quote_initialized',
                ['session_quote' => $this->_getSession()]
            );

        return $this;
    }

    /**
     * Retrieve session object
     *
     * @return SM_XRetail_Model_Session_Quote
     */
    protected function _getSession() {

        return Mage::getSingleton('xretail/session_quote')->reloadQuote();
    }

    /**
     * Retrieve quote object
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote() {

        return $this->_getOrderCreateModel()->getQuote();
    }

    /**
     * @return SM_XRetail_Model_Sales_Order_Create
     */
    protected function _getOrderCreateModel() {

        return Mage::getSingleton('xretail/sales_order_create');
    }

    /**
     *For test
     *
     * @param bool $isExchange
     */
    private function dummyData($isExchange = false) {
        $data = [
            'items'       =>
                [
                    // 'q' =>
                    //     array(
                    //         'qty'                => '1',
                    //         // 'custom_price'       => '310',
                    //         'use_discount'       => '1',
                    //         // 'discount_per_items' => 100,
                    //         'product_id'         => 895
                    //     ),'q1' =>
                    //[
                    //    'qty'                => '2',
                    //    // 'custom_price'       => '310',
                    //    //'use_discount' => '1',
                    //    'discount_per_items' => 10,
                    //    'product_id'         => 896
                    //],
                    //447 =>
                    //    [
                    //        'qty'                => '1',
                    //        'bundle_option_qty'  =>
                    //            [
                    //                24 => '1',
                    //                23 => '1',
                    //            ],
                    //        'bundle_option'      =>
                    //            [
                    //                24 => '91',
                    //                23 => '88',
                    //            ],
                    //        'product_id'         => 447,
                    //        'discount_per_items' => 50,
                    //    ],
                    //                    877 =>
                    //                        array(
                    //                            'qty'             => '1',
                    //                            'super_attribute' =>
                    //                                array(
                    //                                    92  => '20',
                    //                                    180 => '78',
                    //                                ),
                    //                        ),
                    //                    555 =>
                    //                        array(
                    //                            'qty'         => '',
                    //                            'super_group' =>
                    //                                array(
                    //                                    547 => '1',
                    //                                    548 => '1',
                    //                                    551 => '1',
                    //                                ),
                    //                        ),
                    // 234 =>
                    //     array(
                    //         'qty'          => '1',
                    //         'action'       => '',
                    //         'custom_price' => '400',
                    //         'use_discount' => '1',
                    //         'name'         => 'Test custom sales',
                    //         'product_id'   => 'custom_sale'
                    //     )
                ],
            'customer_id' => '24',
            'store_id'    => '1',
            'order'       =>
                [
                    'billing_address'      =>
                        [
                            'firstname'  => 'Jack',
                            'lastname'   => 'Fitz',
                            'street'     =>
                                [
                                    0 => '7NWillowSt',
                                ],
                            'city'       => 'Montclair',
                            'country_id' => 'US',
                            'region_id'  => '41',
                            'region'     => 'NewJersey',
                            'postcode'   => '07042',
                            'telephone'  => '222-555-4190',
                        ],
                    'shipping_address'     =>
                        [
                            'firstname'  => 'Jack',
                            'lastname'   => 'Fitz',
                            'street'     =>
                                [
                                    0 => '7NWillowSt',
                                ],
                            'city'       => 'Montclair',
                            'country_id' => 'US',
                            'region_id'  => '41',
                            'region'     => 'NewJersey',
                            'postcode'   => '07042',
                            'telephone'  => '222-555-4190',
                        ],
                    'payment_method'       => 'xretail_multiple_payment',
                    'payment_data'         => [
                        'xpayment_dummy1' => 50,
                        //'xpayment_dummy2' => 0
                    ],
                    'shipping_method'      => 'xretail_shipping_xretail_dummy_1_code',
                    'coupon'               => [
                        //'code' => 123
                    ],
                    //'whole_order_discount' => [
                    //    'value'         => 50,
                    //    'isPercentMode' => true
                    //]
                ],
        ];
        if ($isExchange) {
            $data['creditmemo'] = [
                'items'               =>
                    [
                        1128 =>
                            [
                                'qty' => '1',
                            ],
                    ],
                'order_id'            => 281,
                'do_offline'          => '1',
                'comment_text'        => '',
                'shipping_amount'     => '0',
                'adjustment_positive' => '0',
                'adjustment_negative' => '0',
            ];
        }
        $this->getRequest()->setParams($data);
    }


    public function exchangeOrder() {
        // Phải hiểu exchange là 2 bước: Tạo lại order -> mua thêm product mới -> refund old product

        //dummy
        // $this->dummyData(true);

        $creditMemoData = $this->getRequest()->getParam('creditmemo');
        $orderId        = $creditMemoData['order_id'];

        /*Kiểm tra ở đây nếu có data items mới => thực hiện exchange thì mới reorder*/
        if (!is_null($items = $this->getRequest()->getParam('items')) && count($items) > 0) {
            $order           = Mage::getModel('sales/order')->load($orderId);
            $paymentInstance = $order->getPayment()->getMethodInstance();
            if ($paymentInstance->getCode() == SM_XRetail_Model_Payment_XRetailMultiplePayment::CODE) {
                /*Nếu order cũ sử dụng layaway thì phải lưu thông tin cũ để sau khi reorder + lại*/
                $this->_oldPaymentData = unserialize($paymentInstance->getInfoInstance()->getAdditionalData());
            }


            /*DO: close order cũ*/
            /*  + thực hiện fully refund order cũ khi có bất kể exchange nào,
             *  mục đích để return stock những items để cho
             *  lượt mua exchange k xảy ra lỗi liên quan tới stock*/
            foreach ($creditMemoData['items'] as &$item) {
                foreach ($items as $newExchangeItem) {
                    if ($item['product_id'] == $newExchangeItem['product_id']) {
                        $item['qty'] = $item['qty'] + $newExchangeItem['qty'];
                    }
                }
            }

            $this->retailOrderRefund->refundOrder($creditMemoData);


            /*
             * Data client gửi lên bao gồm thông tin:
             *  + order mới: bao gồm cả sản phẩm cũ(không bị refund) và sản phẩm mới
             *  + creditmemo data: sản phẩm bị refund
             * */

            $this->saveOrderAndInvoice();
//                //->reorder($orderId)//Không cần sử dụng data cũ nữa. Bởi vì client sẽ gửi lên thông tin của order gồm những items sẽ mua mới và những item không bị refund
//                //->removeAllItems()

        }
        else {
            throw new Exception('Exchange must have new items');
        }

        /*Phải update thông tin payment cũ nếu payment đó sử dụng layaway.
         * Ví dụ 1 trường hợp đó là: trước đó mua 1 items giá 100 mà chỉ trả có 10 sau đó refund thì sao?
         * Cách này mình có thể xử lý là update lại totalpaid về = với grand total để có thể refund được.
         * sau đó lấy data layaway cũ update vào, data layaway mới update vào => total paid sẽ về đúng
        */

        // update lai layaway data sau khi refund Với điều kiện là order đấy sử dụng partial payment
        if ($this->_currentOrder && $this->_oldPaymentData
            && ($order->getTotalPaid() < $order->getGrandTotal())
        ) {
            $this->retailOrderInvoice->updateLayaway(
                [
                    'order'   => [
                        'order_id'        => $this->_currentOrder->getId(),
                        'layaway_comment' => 'Partial Refund with layaway'
                    ],
                    'payment' => $this->_oldPaymentData
                ]
            );
        }


        return $this->outputOrder($this->_currentOrder);
    }


    /**
     * @param null $orderId
     *
     * @return $this
     * @throws \Exception
     */
    protected function reorder($orderId = null) {
        $this->_getSession()->clear();

        if (is_null($orderId))
            $orderId = $this->getRequest()->getParam('order_id');

        $order = Mage::getModel('sales/order')->load($orderId);
        if (!Mage::helper('sales/reorder')->canReorder($order)) {
            throw new Exception("Can't reorder");
        }
        $paymentInstance = $order->getPayment()->getMethodInstance();
        if ($paymentInstance->getCode() == SM_XRetail_Model_Payment_XRetailMultiplePayment::CODE) {
            /*Nếu order cũ sử dụng layaway thì phải lưu thông tin cũ để sau khi reorder + lại*/
            $this->_oldPaymentData = unserialize($paymentInstance->getInfoInstance()->getAdditionalData());
        }

        if ($order->getId()) {
            $order->setReordered(true);
            $this->_getSession()->setUseOldShippingMethod(true);
            $this->_getOrderCreateModel()->initReOderForExchangeAndRefund($order);
        }
        else {
            throw new Exception("Can't get order id");
        }

        return $this;
    }

    private function outputOrder(Mage_Sales_Model_Order $order) {
        $orderData          = $order->getData();
        $orderData['items'] = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $itemData = $item->getData();
            if ($item->getHasChildren()) {
                $itemData['children'] = [];
                foreach ($item->getChildrenItems() as $childItem) {
                    $itemData['children'][] = $childItem->getData('item_id');
                }
            }
            $orderData['items'][] = $itemData;
        }

        return $orderData;
    }

    private function addDataModeToOrder(Mage_Sales_Model_Order $order) {
        /*Add data online/offline to order*/
        if (!SM_XRetail_Model_ResourceModel_OrderManagement::$IS_OFFLINE_MODE)
            $order->setData('retail_order_create_mode', SM_XRetail_Model_ResourceModel_OrderManagement::ONLINE_MODE)->save();
        else
            $order->setData('retail_order_create_mode', SM_XRetail_Model_ResourceModel_OrderManagement::OFFLINE_MODE)->save();

        return $this;
    }
}
