<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/24/16
 * Time: 10:24 AM
 */
class SM_XRetail_Model_ResourceModel_OrderManagement extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * Order states
     */
    const STATE_NEW = 'new';
    const STATE_PENDING_PAYMENT = 'pending_payment';
    const STATE_PENDING = 'pending';
    const STATE_PROCESSING = 'processing';
    const STATE_COMPLETE = 'complete';
    const STATE_CLOSED = 'closed';
    const STATE_CANCELED = 'canceled';
    const STATE_HOLDED = 'holded';
    const STATE_PAYMENT_REVIEW = 'payment_review';
    const STATE_LAYAWAY = 'layaway';

    /**
     * Đây là cờ để biết được order đang tạo là online hay offline
     * Sẽ add thêm field 'retail_order_create_mode'
     *
     * @var bool
     */
    static $IS_OFFLINE_MODE = false;
    const ONLINE_MODE = 'online';
    const OFFLINE_MODE = 'offline';

    /**
     * @var Mage_Sales_Model_Order
     */
    protected $_orderModel;

    /**
     * @var Mage_Sales_Model_Order_Config
     */
    protected $_salesOrderConfigModel;
    /**
     * @var \Mage_Sales_Model_Order_Item
     */
    private $_orderItemModel;
    private $xretailOrderOnline;

    /**
     * SM_XRetail_Model_ResourceModel_OrderManagement constructor.
     */
    public function __construct() {
        $this->_orderModel            = Mage::getModel('sales/order');
        $this->_salesOrderConfigModel = Mage::getSingleton('sales/order_config');
        $this->_orderItemModel        = Mage::getModel('sales/order_item');
        // $this->xretailOrderOnline     = Mage::getModel('xretail/resourceModel_xRetailOrderOnline');
        parent::__construct();
    }

    public function saveOrderOffline() {
        SM_XRetail_Model_ResourceModel_XRetailOrderOnline::$IS_COLLECT_RULE = false;
        SM_XRetail_Model_ResourceModel_OrderManagement::$IS_OFFLINE_MODE    = true;

        return Mage::getModel('xretail/resourceModel_xRetailOrderOnline')->saveOrderAndInvoice();
    }

    /**
     * TODO: api to save oder
     *
     * @return mixed
     * @throws bool
     */
    public function saveOrder() {
        $this->dummyData();
        $request                 = $this->getRequest()->getParams();
        $data                    = $request['order'];
        $data['shipping_method'] = "xretail_shipping_xretail_dummy_1_code";
        //set Session Customer
        $this->_getSession()->setCustomerId((int)$request['customer_id']);
        $quote    = $this->_getOrderCreateModel()->getQuote();
        $customer = Mage::getModel('customer/customer')->load($request['customer_id']);
        $quote->assignCustomer($customer);
        //Set StoreId
        $storeId = 1;
        $quote->setStoreId($storeId);
        $this->_getSession()->setStoreId((int)$storeId);
        //set BillingAdd/Shipping Add to Quote
        unset($data['billing_addressing']['street']);
        $orderBillingAdd                   = $data['billing_address'];
        $orderShippingAdd                  = $data['shipping_address'];
        $data['billing_address']['street'] = Mage::helper('xretail/customer')->getStreet($orderBillingAdd['street']);
        unset($data['shipping_address']['street']);
        $data['shipping_address']['street'] = Mage::helper('xretail/customer')->getStreet($orderShippingAdd['street']);
        $quote->getBillingAddress()->addData($data['billing_address']);
        $quote->getShippingAddress()->addData($data['shipping_address']);
        $quote->save();

        //remove old Item in Quote and add current item to quote
        $quoteItems = $quote->getAllItems();
        foreach ($quoteItems as $item) {
            $quote->removeItem($item->getId());
        }
        $quoteItems = $request['items'];
        $items      = array();
        foreach ($quoteItems as $item => $itemdetails) {
            if (@$itemdetails['qty'] != 0)
                $items[$item] = $itemdetails;
        }
        $items = $this->_processFiles($items);
        $this->_getOrderCreateModel()->addProducts($items);
        $this->_getOrderCreateModel()->setShippingMethod($data['shipping_method']);
        $this->_getOrderCreateModel()->getShippingAddress()
             ->setShippingMethod($data['shipping_method'])
             ->setLimitCarrier(array(SM_XRetail_Model_Shipping_XretailCarrier::CODE))
             ->setCollectShippingRates(true);
        $this->_getOrderCreateModel()->collectShippingRates();
        $this->_getOrderCreateModel()->setPaymentMethod($data['payment_method']);
        $order = $this->_getOrderCreateModel()
                      ->setIsValidate(true)
                      ->createOrder();

        // $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment();
        // $shipment->register();
        //
        // Mage::getModel('core/resource_transaction')
        //     ->addObject($order)
        //     ->addObject($shipment)
        //     ->save();
        // /* Complete the order */
        // $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
        // $invoice->register();
        // Mage::getModel('core/resource_transaction')
        //     ->addObject($invoice)
        //     ->addObject($order)->save();

        $order->save();
        $this->_getSession()->clear();

        return $order->getData();

    }

    /**
     * TODO: api to change state order
     *
     * @return float|mixed
     * @throws Exception
     */
    public function changeState() {
        $searchCriteria = $this->getSearchCriteria();

        //validate data:
        if (is_null($orderId = $searchCriteria->getData('order_id')) && is_null($orderIncrementId = $searchCriteria->getData('order_increment_id')))
            throw new \Exception('Not found field: searchCriteria[order_id] && searchCriteria[order_increment_id]');
        if (is_null($orderState = $searchCriteria->getData('order_state')))
            throw new \Exception('Not found field: searchCriteria[order_state]');
        if (!in_array(
            $orderState,
            array(
                self::STATE_CANCELED,
                self::STATE_CLOSED,
                self::STATE_COMPLETE,
                self::STATE_HOLDED,
                self::STATE_LAYAWAY,
                self::STATE_PROCESSING,
                self::STATE_PENDING_PAYMENT))
        )
            throw new \Exception('Not allow state: ' . $orderState);

        $comment = $searchCriteria->getData('comment');
        if (!is_null($orderId))
            return $this->changeStateOrderById($orderId, $orderState, $comment)->getData();
        else
            return $this->changeStateOrderById($orderIncrementId, $orderState, $comment, true)->getData();

    }

    public function pull() {
        $collection = $this->_orderModel->getCollection();
        $collection->setOrder('created_at', 'DESC');
        $searchCriteria = $this->getSearchCriteria();
        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        $c    = clone $collection;
        $size = $c->count();
        $collection->setCurPage($searchCriteria->getData('currentPage'));
        $collection->setPageSize($searchCriteria->getData('pageSize'));

        $orders = array();
        foreach ($collection as $item) {
            $orders[$item->getId()]          = $item->getData();
            $orders[$item->getId()]['items'] = $this->getItemsInOrder($item->getId());
        }

        return $this->getSearchResult()
                    ->setSearchCriteria($searchCriteria)
                    ->setItems($orders)
                    ->setTotalCount($size)
                    ->getOutput();
    }

    private function getItemsInOrder($orderId) {
        $collection = $this->_orderItemModel->getCollection();
        $collection->addFieldToFilter('order_id', $orderId);
        $items = array();
        foreach ($collection as $item) {
            $items[] = $item->getData();
        }

        return $items;
    }

    /**
     * @param $id
     * @param $orderState
     *
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    public function changeStateOrderById($id, $orderState, $comment = '', $isIncrementId = false) {
        /* @var $order Mage_Sales_Model_Order */
        if (!$isIncrementId)
            $order = $this->_orderModel->load($id);
        else
            $order = $this->_orderModel->loadByIncrementId($id);

        if($orderState === $order->getState()){
            return;
        }

        $status = $this->_salesOrderConfigModel->getStatusLabel($orderState);
        $order->setState($orderState, $status, $comment);

        return $order->save();
    }

    /**
     * @return Mage_Adminhtml_Model_Session_Quote
     */
    protected function _getSession() {
        return Mage::getSingleton('xretail/session_quote')->reloadQuote();
    }

    /**
     * @return Mage_Adminhtml_Model_Sales_Order_Create|SM_XPos_Model_Adminhtml_Sales_Order_Create
     */
    protected function _getOrderCreateModel() {
        return Mage::getSingleton('xretail/sales_order_create');
    }

    /**
     * Process buyRequest file options of items
     *
     * @param array $items
     *
     * @return array
     */
    protected function _processFiles($items) {
        /* @var $productHelper Mage_Catalog_Helper_Product */
        $productHelper = Mage::helper('catalog/product');
        foreach ($items as $id => $item) {
            $buyRequest = new Varien_Object($item);
            $params     = array('files_prefix' => 'item_' . $id . '_');
            $buyRequest = $productHelper->addParamsToBuyRequest($buyRequest, $params);
            if ($buyRequest->hasData()) {
                $items[$id] = $buyRequest->toArray();
            }
        }

        return $items;
    }

    /**
     *For test
     *
     * @param bool $isExchange
     */
    private function dummyData($isExchange = false) {
        $data = array(
            'items'       =>
                array(
                    894 =>
                        array(
                            'qty'                => '1',
                            'custom_price'       => '310',
                            'use_discount'       => '1',
                            'discount_per_items' => 10,
                            'product_id'         => 899
                        ),
                    //                    447 =>
                    //                        array(
                    //                            'qty'                => '1',
                    //                            'bundle_option_qty'  =>
                    //                                array(
                    //                                    24 => '1',
                    //                                    23 => '1',
                    //                                ),
                    //                            'bundle_option'      =>
                    //                                array(
                    //                                    24 => '91',
                    //                                    23 => '88',
                    //                                ),
                    //                            'product_id'         => 447,
                    //                            'discount_per_items' => 200,
                    //                        ),
                    //                    877 =>
                    //                        array(
                    //                            'qty'             => '1',
                    //                            'super_attribute' =>
                    //                                array(
                    //                                    92  => '20',
                    //                                    180 => '78',
                    //                                ),
                    //                        ),
                    //                    555 =>
                    //                        array(
                    //                            'qty'         => '',
                    //                            'super_group' =>
                    //                                array(
                    //                                    547 => '1',
                    //                                    548 => '1',
                    //                                    551 => '1',
                    //                                ),
                    //                        ),
                    //                    888 =>
                    //                        array(
                    //                            'qty'          => '1',
                    //                            'action'       => '',
                    //                            'custom_price' => '400',
                    //                            'use_discount' => '1',
                    //                            'name'         => 'Test custom sales'
                    //                        )
                ),
            'customer_id' => '24',
            'store_id'    => '1',
            'order'       =>
                array(
                    'billing_address'  =>
                        array(
                            'firstname'  => 'Jack',
                            'lastname'   => 'Fitz',
                            'street'     =>
                                array(
                                    0 => '7NWillowSt',
                                ),
                            'city'       => 'Montclair',
                            'country_id' => 'US',
                            'region_id'  => '41',
                            'region'     => 'NewJersey',
                            'postcode'   => '07042',
                            'telephone'  => '222-555-4190',
                        ),
                    'shipping_address' =>
                        array(
                            'firstname'  => 'Jack',
                            'lastname'   => 'Fitz',
                            'street'     =>
                                array(
                                    0 => '7NWillowSt',
                                ),
                            'city'       => 'Montclair',
                            'country_id' => 'US',
                            'region_id'  => '41',
                            'region'     => 'NewJersey',
                            'postcode'   => '07042',
                            'telephone'  => '222-555-4190',
                        ),
                    'payment_method'   => 'xpayment_dummy1',
                    'payment_data'     => array(
                        'xpayment_dummy1' => 100,
                        'xpayment_dummy2' => 200
                    ),
                    'shipping_method'  => 'xretail_shipping_xretail_dummy_1_code'
                ),
        );
        if ($isExchange) {
            $data['creditmemo'] = array(
                'items'               =>
                    array(
                        // 754 =>
                        //     array(
                        //         'qty' => '1',
                        //     ),
                    ),
                'order_id'            => 332,
                'do_offline'          => '1',
                'comment_text'        => '',
                'shipping_amount'     => '0',
                'adjustment_positive' => '0',
                'adjustment_negative' => '0',
            );
        }
        $this->getRequest()->setParams($data);
    }
}
