<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/22/16
 * Time: 5:05 PM
 */
class SM_XRetail_Model_ResourceModel_ProductManagement extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var Mage_Catalog_Model_Product
     */
    protected $_productModel;

    /**
     * @var SM_XRetail_Helper_Realtime_RealTimeManager
     */
    protected $_realtimeHelper;
    /**
     * @var \SM_XRetail_Model_Api_Configuration
     */
    protected $_apiConfig;
    /**
     * @var \Mage_Eav_Model_Entity_Type
     */
    protected $eavEntityTypeModel;
    /**
     * @var \Mage_Eav_Model_Resource_Entity_Attribute_Set
     */
    protected $eavAttrSetResourceModel;
    /**
     * @var SM_XRetail_Helper_CustomSales_Data
     */
    private $_customSalesHelper;


    /**
     * SM_XRetail_Model_ResourceModel_ProductManagement constructor.
     */
    public function __construct() {

        $this->_productModel = Mage::getModel('catalog/product');
        $this->_realtimeHelper = Mage::helper('xretail/realtime_realTimeManager');
        $this->_customSalesHelper = Mage::helper('xretail/customSales_data');
        $this->_apiConfig = Mage::getModel('xretail/api_configuration');
        $this->eavEntityTypeModel = Mage::getModel('eav/entity_type');
        $this->eavAttrSetResourceModel = Mage::getResourceModel('eav/entity_attribute_set');

        return parent::__construct();
    }

    /**
     * TODO: API TO GET PRODUCT
     *
     * @return array
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function loadXProduct() {

        $collection = false;
        $searchCriteria = $this->getSearchCriteria();
        $loadByStoreId = $searchCriteria->getData('storeId');

        if (is_null($loadByStoreId))
            Mage::app()->setCurrentStore(0);
        else
            Mage::app()->setCurrentStore($loadByStoreId);

        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        $forceMode = $searchCriteria->getData('forceMode') == 1 ? true : false;

        $items = false;
        $prefixProcessLoading = is_null($loadByStoreId) ? 'product_data' : 'product_data_by_store_' . $loadByStoreId;

        /*
         * Get data from xCache when loaded full data or PageSize-Load = PageSize-Cache.
        */
        if (!$forceMode
            && ($this->_xCache->isLoadedFullData($prefixProcessLoading)
                || SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT === SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract::PAGE_SIZE)
        ) {
            if (!$this->_xCache->isLoadedFullData($prefixProcessLoading)
                || !$searchCriteria->getData('pageSize')
            )
                $items = $this->_xCache->getProducts
                (
                    $searchCriteria->getData('currentPage'),
                    SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT,
                    $loadByStoreId
                );
            else {
                $items = $this->_xCache->getProducts
                (
                    $searchCriteria->getData('currentPage'),
                    $searchCriteria->getData('pageSize'),
                    $loadByStoreId
                );
            }
        }
        if (!$items) {
            /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
            $collection = $this->_productModel->getCollection();

            $collection->addAttributeToSelect('*');
            $collection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner'
            );
            $collection->joinAttribute(
                'visibility',
                'catalog_product/visibility',
                'entity_id',
                null,
                'inner'
            );

            $collection->getSelect()
                       ->columns("GROUP_CONCAT(n.website_id SEPARATOR ',') AS website_ids")
                       ->joinLeft(array('n' => 'catalog_product_website'), 'n.product_id = e.entity_id')
                       ->group('e.entity_id');

            $collection->getSelect()
                       ->columns("GROUP_CONCAT(r.category_id SEPARATOR ',') AS category_id")
                       ->joinLeft(array('r' => 'catalog_category_product'), 'r.product_id = e.entity_id', 'r.position')
                       ->group('e.entity_id');

            $collection = $this->processSearch($collection);
            $collection->load();

            $items = array();
            if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
                $this->_searchResult->setClientKey('XProduct_Server_Key');
                $this->_searchResult->setItems(array());
                if (!$forceMode)
                    $this->_xCache->updateIndexLoading
                    (
                        $searchCriteria->getData('currentPage'),
                        $collection->getSize(),
                        true,
                        $prefixProcessLoading
                    );
            }
            else {
                foreach ($collection as $item) {
                    if ($item->getId() == $this->_customSalesHelper->getCustomSalesId())
                        continue;

                    /*thinking: Nếu đã đi vào đây thì việc get từ cache ra không còn ý nghĩa với các data đã có trong $item*/
                    $xProduct = new SM_XRetail_Model_Api_Data_XProduct($item->getData());

                    /*do: get custom_options by config*/
                    $typeGetCustomOptions = $this->_apiConfig->getStoreConfig('xretail/config/typeGetCustomOptions');
                    if (in_array($item->getTypeId(), preg_split('/,+/', $typeGetCustomOptions))) {
                        $item = Mage::getModel('catalog/product')->load($item->getId());
                        $xProduct->addData($item->getData());
                    }
                    // get options
                    $xProduct->setData('x_options', $this->_xCache->getXOptions($item));

                    /* TODO: NEED REFACTOR */
                    /* BEGIN */
                    // get price excluding tax
                    $xProduct->setData('price', $item->getPrice());

                    $xProduct->setData('store_rate', 0);

                    //get store tax
                    if($finalPriceExcludingTax = Mage::helper('tax')->getPrice($item, $item->getFinalPrice(), false )){
                        $xProduct->setData('store_rate', round((($item->getPrice()/$finalPriceExcludingTax) - 1) * 100));
                    }

                    //set price_incl_tax
                    $xProduct->setData('origin_price', $item->getPrice());

                    /* END*/

                    // get stock_items
                    $xProduct->setData('stock_items', $this->_xCache->getStockItem($item)->getData());

                    // load from cache
                    /*$getFromCache = $this->_xCache->loadStockItemFromXCache($item->getId());*/

                    $items[] = $xProduct;
                }

                if (!$forceMode)
                    //add to cache: Chỉ save vào cache khi mà đang load liên tục product, cung page_size và bắt đầu từ page 1 đến page cuối cùng. Sau kh đến page cuối cùng thì có 1 cacheKey thì lúc đó ko save vào nữa.
                    if ($this->_xCache->isContinuousLoading
                        (
                            $searchCriteria->getData('currentPage'),
                            SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT,
                            $collection->getSize(),
                            $prefixProcessLoading
                        )
                        && !$this->_xCache->isLoadedFullData($prefixProcessLoading)
                    ) {

                        $this->_xCache->addProduct($items, 'id', $loadByStoreId);

                        $this->_xCache->updateIndexLoading
                        (
                            $searchCriteria->getData('currentPage'),
                            $collection->getSize(),
                            false,
                            $prefixProcessLoading
                        );
                    }
                    else
                        throw  new \Exception('An error occurred during loading. Please reload data from page 1');
            }
        }
        $this->_searchResult->setItems($items);

        $size = $collection ? $collection->getSize() : $this->_xCache->getTotalCount();
        $this->_searchResult->setTotalCount($size);

        $output = ($this->_searchResult->getOutput());

        return $output;
    }

    /**
     * TODO: API TO GET OPTIONS PRODUCT
     *
     * @return array
     * @throws Exception
     */
    public function loadXOption() {

        $searchCriteria = $this->getSearchCriteria();
        $collection = $this->_productModel->getCollection();

        $collection = $this->processSearch($collection);
        $collection->load();

        $items = array();
        foreach ($collection as $item) {
            $item = Mage::getModel('catalog/product')->load($item->getId());
            $xProduct = new SM_XRetail_Model_Api_Data_XProduct($item->getData());
            $xProduct->setData('x_options', $this->_xCache->getXOptions($item));
            $items[] = $xProduct;
        }

        $this->_searchResult->setItems($items);
        $this->_searchResult->setTotalCount($collection->getSize());

        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
            $this->_searchResult->setClientKey('XOptions_Server_Key');
            $this->_searchResult->setItems(array());
        }

        $output = ($this->_searchResult->getOutput());

        return $output;
    }

    /**
     * TODO: API TO GET INVENTORY PRODUCT
     *
     * @return array
     * @throws Exception
     */
    public function loadXInventory() {

        $searchCriteria = $this->getSearchCriteria();
        $collection = $this->_productModel->getCollection();

        $collection = $this->processSearch($collection);

        $collection->load();

        $items = array();
        foreach ($collection as $item) {
            $xProduct = new SM_XRetail_Model_Api_Data_XProduct($item->getData());
            $xProduct->setData('stock_items', $this->_xCache->getStockItem($item)->getData());
            $items[] = $xProduct;
        }

        $this->_searchResult->setItems($items);
        $this->_searchResult->setTotalCount($collection->getSize());

        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
            $this->_searchResult->setClientKey('XInventory_Server_Key');
            $this->_searchResult->setItems(array());
        }

        $output = ($this->_searchResult->getOutput());

        return $output;
    }

    /**
     * @return array
     */
    public function getCustomSaleId() {

        return array('id' => Mage::helper('xretail/customSales_data')->getCustomSalesId());
    }

    /**
     * @return array|bool
     */
    public function reloadRealtime() {

        return $this->_realtimeHelper->reloadProduct();
    }

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     * @throws Exception
     */
    protected function processSearch(Mage_Catalog_Model_Resource_Product_Collection $collection) {

        $searchCriteria = $this->getSearchCriteria();

        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        if (!is_null($id = $searchCriteria->getData('product_id'))) {
            $collection->addFieldToFilter('entity_id', array('eq' => $id));
        }
        if (!is_null($sku = $searchCriteria->getData('sku'))) {
            $collection->addAttributeToFilter('sku', array('eq' => $sku));
        }

        $collection->setCurPage($searchCriteria->getData('currentPage'));

        if ($searchCriteria->getData('forceMode') == 1 && !is_null($searchCriteria->getData('pageSize'))) {
            $collection->setPageSize($searchCriteria->getData('pageSize'));
        }
        else
            $collection->setPageSize(SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT);

        return $collection;
    }

    /**
     * Retrieve attribute set of product with attributes
     */
    public function productAttrSet() {
        $productEntity = $this->eavEntityTypeModel->loadByCode('catalog_product');
        $productEntityTypeId = $productEntity->getEntityTypeId();

        $this->eavAttrSetResourceModel;
    }

    public function setTaxClass() {
        $data = $this->getRequest()->getParams();
        if (!isset($data['tax_class_id']))
            throw new Exception('Must have param tax_class_id');

        $taxAttr = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'tax_class_id');
        Mage::getSingleton('catalog/product_action')->updateAttributes(
        // Set an array of product ids to update
            array($this->_customSalesHelper->getCustomSalesId()),
            // Set an arry with attribute ids and their value ids to update
            array($taxAttr->getId() => $data['tax_class_id']),
            // Store Id
            0
        );

        return ['data' => 'success'];
    }
}
