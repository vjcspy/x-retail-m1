<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/10/16
 * Time: 10:06 AM
 */
class SM_XRetail_Model_ResourceModel_Catalog extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var Mage_Catalog_Model_Category
     */
    protected $_catalogCategoryModel;

    protected $_collection;

    protected $_leftRightCategory;

    /**
     * SM_XRetail_Model_ResourceModel_Catalog constructor.
     */
    public function __construct() {

        $this->_catalogCategoryModel = Mage::getModel('catalog/category');
        $this->_collection = $this->_catalogCategoryModel->getCollection();
        $this->categoryProduct = Mage::getModel('catalog/category_indexer_product');
        parent::__construct();
    }

    public function getNestedData() {

        return Mage::getModel('xretail/nestedModel_data')->convertData();
    }


    /**
     * @return mixed
     */
    public function index() {

        $searchCriteria = $this->getSearchCriteria();
        if (is_null($searchCriteria->getData('currentPage')))
            $searchCriteria->setData('currentPage', 1);

        $c = clone $this->_collection;
        $size = $c->count();
        $this->processPageAndSize($searchCriteria->getData('currentPage'), $searchCriteria->getData('pageSize'));

        if ($this->getSearchCriteria()->getData('type') == 'level')
            return ($this->grouped());
        else
            $this->_searchResult->setItems($this->getCatData());
        $this->_searchResult->setTotalCount($size);

        $this->_searchResult->setSearchCriteria($this->getSearchCriteria());

        return $this->_searchResult->getOutput();
    }

    protected function processPageAndSize($page = 1, $pageSize = 100) {

        $this->_collection->setCurPage($page);
        $this->_collection->setPageSize($pageSize);

        return $this->_collection;
    }


    /**
     * @var
     */
    protected $_catData;

    /**
     * @return array
     */
    protected function getCatData() {

        if (is_null($this->_catData)) {
            $collection = $this->_collection->addAttributeToSelect('*');
            if ($collection->getLastPageNumber() < $this->getSearchCriteria()->getData('currentPage')) {
                return array();
            }
            $this->_catData = array();
            foreach ($collection as $item) {
                $this->_catData[] = $this->_catalogCategoryModel->load($item->getId())->getData();
            }
        }

        return $this->_catData;
    }

    /**
     * @var
     */
    protected $_groupedCat;

    /**
     * @return array
     */
    protected function grouped() {

        if (is_null($this->_groupedCat)) {
            $catData = $this->getCatData();
            $this->_groupedCat = array();
            foreach ($catData as $cate) {
                $this->saveCateToGroup($cate);
            }
        }

        return $this->_groupedCat;
    }

    /**
     * @param $cate
     */
    protected function saveCateToGroup($cate) {

        $paths = explode('/', $cate['path']);
        if (empty($paths) || $paths === false)
            return;
        $paths = array_reverse($paths);
        if (!isset($this->_groupedCat[$paths[0]]))
            $this->_groupedCat[$paths[0]] = array(
                'cate' => array(),
                'level' => $cate['level']
            );
        $this->_groupedCat[$paths[0]]['cate'][] = $cate;
    }

    /**
     * @var
     */
    protected $_leveledCat;

    /**
     * @return mixed
     */
    protected function leveled() {

        if (is_null($this->_leveledCat)) {
            $groupedCat = $this->grouped();
            foreach ($groupedCat as $group) {
                if (!isset($this->_leveledCat[$group['level']])) {
                    $this->_leveledCat[$group['level']] = array();
                }
                foreach ($group['cate'] as $cate) {
                    $this->_leveledCat[$group['level']][] = $cate;
                }
            }
        }

        return $this->_leveledCat;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getProductInCategory() {
        $searchCriteria = $this->getSearchCriteria();
        $categoryId = $searchCriteria->getData('category_id');
        if (is_null($categoryId))
            throw new Exception('Must have param searchCriteria[category_id]');

        $products = Mage::getModel('catalog/category')->load($categoryId)
                        ->getProductCollection()
                        ->addAttributeToSelect('*');// add all attributes - optional

        return $this->getSearchResult()->setItems($products->getAllIdsCache())->getOutput();
    }

}
