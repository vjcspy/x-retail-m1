<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 13/06/2016
 * Time: 14:08
 */
class SM_XRetail_Model_ResourceModel_Shop_EavAttribute extends Mage_Eav_Model_Resource_Entity_Attribute {

    /**
     * @var \Mage_Eav_Model_Entity_Type
     */
    protected $eavEntityTypeModel;

    public function __construct() {
        $this->eavEntityTypeModel = Mage::getModel('eav/entity_type');

        return parent::__construct();
    }

    /**
     * @return null|string
     */
    protected function getProductEntityId() {
        $productEntity = $this->eavEntityTypeModel->loadByCode('catalog_product');

        return $productEntity->getEntityTypeId();
    }

    /**
     * @param null $entityTypeId
     *
     * @return array
     */
    public function getAttributeSet($entityTypeId = null) {
        $adapter = $this->_getReadAdapter();

        $select = $adapter->select()
                          ->from(
                              array('attribute_set' => $this->getTable('eav/attribute_set')),
                              array('attribute_set_id', 'entity_type_id', 'attribute_set_name', 'sort_order'));
        $bind   = array();
        if (is_numeric($entityTypeId)) {
            $bind[':entity_type_id'] = $entityTypeId;
            $select->where('attribute_set.entity_type_id = :entity_type_id');
        }
        $result = $adapter->fetchAll($select, $bind);

        return $result;
    }

    /**
     * @param null $entityTypeId
     *
     * @return array
     */
    public function getEntityAttrInfo($entityTypeId = null) {
        $adapter = $this->_getReadAdapter();

        $select = $adapter->select()
                          ->from(
                              array('entity' => $this->getTable('eav/entity_attribute')),
                              array('attribute_set_id', 'attribute_id', 'attribute_set_id', 'attribute_group_id', 'sort_order'))
                          ->joinLeft(
                              array('attribute_group' => $this->getTable('eav/attribute_group')),
                              'entity.attribute_group_id = attribute_group.attribute_group_id',
                              array(
                                  'group_sort_order'     => 'sort_order',
                                  'attribute_group_name' => 'attribute_group_name'
                              ))
                          ->joinLeft(
                              array('attribute_set' => $this->getTable('eav/attribute_set')),
                              'entity.attribute_set_id = attribute_set.attribute_set_id',
                              array(
                                  'set_sort_order'     => 'sort_order',
                                  'attribute_set_name' => 'attribute_set_name'
                              ))
                          ->joinLeft(
                              array('attribute' => $this->getTable('eav/attribute')),
                              'entity.attribute_id = attribute.attribute_id',
                              array(
                                  'attribute_code',
                                  'backend_type',
                                  'frontend_input',
                                  'frontend_label',
                                  'is_required'
                              ));
        $bind   = array();
        if (is_numeric($entityTypeId)) {
            $bind[':entity_type_id'] = $entityTypeId;
            $select->where('entity.entity_type_id = :entity_type_id');
        }
        $result = $adapter->fetchAll($select, $bind);


        return $result;
    }
}