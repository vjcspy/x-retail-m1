<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 14/06/2016
 * Time: 10:16
 */
class SM_XRetail_Model_ResourceModel_Shop_Category extends SM_XRetail_Model_Api_ServiceAbstract {


    /**
     * @var \Mage_Catalog_Model_Category
     */
    protected $catalogCategoryModel;

    /**
     * @var
     */
    protected $_categories;

    protected $_rootCat = array();

    /**
     * SM_XRetail_Model_ResourceModel_Shop_Category constructor.
     */
    public function __construct() {
        $this->catalogCategoryModel = Mage::getModel('catalog/category');

        return parent::__construct();
    }

    /**
     *
     */
    public function getCategoryNodes() {
        /*
         * Tất cả mọi category đều bắt đầu từ root
         * */
        $this->_rootCat[]           = $this->getRootCat()->getData();
        $this->_rootCat[0]['nodes'] = $this->getNodes($this->_rootCat[0]);

        $this->resolveNodes($this->_rootCat[0]);

        return $this->getSearchResult()->setItems($this->_rootCat)->getOutput();
    }

    protected function resolveNodes(&$parentCat) {
        foreach ($parentCat['nodes'] as $k => $node) {
            $parentCat['nodes'][$k]['nodes'] = $this->getNodes($parentCat['nodes'][$k]);
            $this->resolveNodes($parentCat['nodes'][$k]);
        }

        return $this;
    }

    protected function getNodes($cat) {
        $catId = $cat['entity_id'];
        $nodes = array();

        foreach ($this->getCategories() as $item) {
            if ($item['parent_id'] == $catId)
                $nodes[] = $item;
        }

        return $nodes;
    }


    /**
     * @param int $storeId
     *
     * @return array
     */
    protected function getCategories($storeId = 0) {
        if (is_null($this->_categories)) {
            /* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
            $collection        = $this->catalogCategoryModel->getCollection();
            $this->_categories = array();
            foreach ($collection as $cat) {
                $catEntity                        = Mage::getModel('catalog/category')->setStoreId($storeId)->load($cat->getId());
                $this->_categories[$cat->getId()] = $catEntity->getData();
            }
        }

        return $this->_categories;
    }

    /**
     * @return \Varien_Object
     */
    protected function getRootCat() {
        /* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
        $collection = $this->catalogCategoryModel->getCollection();

        return Mage::getModel('catalog/category')->load($collection->addFieldToFilter('level', 0)->getFirstItem()->getId());
    }
}