<?php

require_once(BP
             . DS
             . 'app'
             . DS
             . 'code'
             . DS
             . 'core'
             . DS
             . 'Mage'
             . DS
             . 'Adminhtml'
             . DS
             . 'controllers'
             . DS
             . 'Catalog'
             . DS
             . 'ProductController.php');

class SM_XRetail_Model_ResourceModel_Shop_Product extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * Can get from ProductManagement
     */
    const PRODUCT_ENTITY_TYPE_ID = 4;

    /**
     * @var \Mage_Catalog_Model_Product
     */
    protected $productModel;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $collection;
    /**
     * @var \SM_XRetail_Model_ResourceModel_Shop_EavAttribute
     */
    protected $eavAttribute;
    private   $_requestData;


    /**
     * SM_XRetail_Model_ResourceModel_Shop_Product constructor.
     */
    public function __construct() {
        $this->productModel = Mage::getModel('catalog/product');
        $this->eavAttribute = Mage::getModel('xretail/resourceModel_shop_eavAttribute');
        parent::__construct();
    }

    public function getCountryOfManufacture() {
        $source = Mage::getModel('catalog/product_attribute_source_countryofmanufacture');

        return $this->getSearchResult()
                    ->setItems($source->getAllOptions())
                    ->getOutput();
    }

    /**
     * GRID FOR SHOP
     *
     * @return array
     * @throws \Mage_Core_Exception
     */
    public function grid() {
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $this
            ->productModel
            ->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('type_id')
            ->addFieldToFilter('type_id', ['in' => ['simple', 'configurable']])
            ->addAttributeToSelect('image');

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinField(
                'qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');

            $collection->addAttributeToSelect('price');
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        }
        $this->setCollection($collection)->processSearch();
        $data = [];
        /** @var $product Mage_Catalog_Model_Product */
        foreach ($this->getCollection() as $product) {
            $data[$product->getId()] = $product->getData();

            $data[$product->getId()]['origin_image'] = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getImage());
        }

        return $this->getSearchResult()->setItems($data)->setTotalCount($collection->getSize())->getOutput();
    }

    public function detail($productId = null) {
        if (is_null($productId) || $productId == "") {
            $id = $this->getSearchCriteria()->getData('product_id');
            if (is_null($id))
                throw new Exception('Must have param searchCriteria[product_id]');
        }
        else
            $id = $productId;

        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $this
            ->productModel
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('type_id', ['in' => ['simple', 'configurable']])
            ->addFieldToFilter('entity_id', $id)
            ->addAttributeToSelect('image');

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinField(
                'qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');

            $collection->addAttributeToSelect('price');
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        }

        $item = $collection->getFirstItem();
        $data = $item->getData();

        $imageData = [];
        $product   = $this->productModel->load($item->getId());
        foreach ($product->getMediaGalleryImages() as $image) {
            $imageData[] = $image->getData();
        }
        $data['image_gallery'] = ($imageData);

        $categories   = $product->getCategoryIds();
        $categoryData = [];
        foreach ($categories as $category) {
            $categoryData[] = $category;
        }
        $data['categories'] = $categoryData;

        $data['stock_items'] = $this->_xCache->getStockItem($item)->getData();

        return $this->getSearchResult()->setItems([$data])->getOutput();
    }

    public function saveProduct() {
        $data = $this->getRequest()->getParams();
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        //$file = fopen('product_crete_data.txt', 'w');
        //fwrite($file, json_encode($data));
        //fclose($file);die;
        //
        //$data = json_decode(fread($file, filesize('product_crete_data.txt')), true);
        //$this->getRequest()->setParams($data);
        $productData = [
            'id'           => $this->getProductPostData('entity_id'),
            'tab'          => 'product_info_tabs_group_25',
            'back'         => 'edit',
            'category_ids' => $this->getProductPostData('category_ids'),
            'product'      =>
                [
                    //'entity_id'                         => $this->getProductPostData('entity_id'),
                    'name'                              => $this->getProductPostData('name'),
                    'description'                       => $this->getProductPostData('description'),
                    'short_description'                 => $this->getProductPostData('short_description'),
                    'sku'                               => $this->getProductPostData('sku'),
                    'weight'                            => '0.1000',
                    'news_from_date'                    => $this->getProductPostData('news_from_date'),
                    'news_to_date'                      => $this->getProductPostData('news_to_date'),
                    'status'                            => $this->getProductPostData('status'),
                    'url_key'                           => $this->getProductPostData('url_key'),
                    'visibility'                        => $this->getProductPostData('visibility'),
                    'country_of_manufacture'            => $this->getProductPostData('country_of_manufacture'),
                    'color'                             => $this->getProductPostData('color'),
                    'size'                              => $this->getProductPostData('size'),
                    'manufacturer'                      => $this->getProductPostData('manufacturer'),
                    'featured'                          => '1',
                    'price'                             => $this->getProductPostData('price'),
                    'special_price'                     => $this->getProductPostData('special_price'),
                    'special_from_date'                 => $this->getProductPostData('special_from_date'),
                    'special_to_date'                   => $this->getProductPostData('special_to_date'),
                    'msrp_enabled'                      => '2',
                    'msrp_display_actual_price_type'    => '4',
                    'msrp'                              => '',
                    'tax_class_id'                      => $this->getProductPostData('tax_class_id'),
                    'meta_title'                        => '',
                    'meta_keyword'                      => '',
                    'meta_description'                  => '',
                    'image'                             => 'no_selection',
                    'small_image'                       => 'no_selection',
                    'thumbnail'                         => 'no_selection',
                    'media_gallery'                     =>
                        [
                            'images' => json_encode($data['image_gallery']),
                            'values' => '{"image":"no_selection","small_image":"no_selection","thumbnail":"no_selection"}',
                        ],
                    'is_recurring'                      => '0',
                    'recurring_profile'                 => '',
                    'custom_design'                     => '',
                    'custom_design_from'                => '',
                    'custom_design_to'                  => '',
                    'custom_layout_update'              => '',
                    'page_layout'                       => '',
                    'options_container'                 => 'container1',
                    'use_config_gift_message_available' => '1',
                    'stock_data'                        =>
                        [
                            'use_config_manage_stock'          => $data['stock_items']['use_config_manage_stock'],
                            'original_inventory_qty'           => $data['stock_items']['qty'],
                            'qty'                              => $data['stock_items']['qty'],
                            'use_config_min_qty'               => $data['stock_items']['use_config_min_qty'],
                            'use_config_min_sale_qty'          => $data['stock_items']['use_config_min_sale_qty'],
                            'use_config_max_sale_qty'          => $data['stock_items']['use_config_max_sale_qty'],
                            'is_qty_decimal'                   => $data['stock_items']['is_qty_decimal'],
                            'is_decimal_divided'               => $data['stock_items']['is_decimal_divided'],
                            'use_config_backorders'            => $data['stock_items']['use_config_backorders'],
                            'use_config_notify_stock_qty'      => $data['stock_items']['use_config_notify_stock_qty'],
                            'use_config_enable_qty_increments' => $data['stock_items']['use_config_enable_qty_increments'],
                            'use_config_qty_increments'        => $data['stock_items']['use_config_qty_increments'],
                            'is_in_stock'                      => $data['stock_items']['is_in_stock'],
                        ],
                    'website_ids'                       =>
                        [
                            0 => '1',
                        ],
                ],
        ];

        $request = new  Zend_Controller_Request_Http();
        $request->setParams($productData);
        $request->setPost($productData);
        $request->setParam('set', $data['attribute_set_id']);
        $response   = new Zend_Controller_Response_Http();
        $controller = new Mage_Adminhtml_Catalog_ProductController($request, $response);
        $controller->saveAction();
    }

    private function getProductPostData($key) {
        if (is_null($this->_requestData)) {
            $this->_requestData = $this->getRequest()->getParams();
        }
        if (isset($this->_requestData[$key]) && !is_null($this->_requestData[$key]))
            return $this->_requestData[$key];
        else
            return '';
    }

    /**
     * @return $this
     */
    public function productAttrInfo() {
        $attrs = $this->eavAttribute->getEntityAttrInfo(self::PRODUCT_ENTITY_TYPE_ID);

        return $this->getSearchResult()->setItems($attrs)->setTotalCount(count($attrs))->getOutput();
    }

    /**
     * @return $this
     */
    public function productAttrSet() {
        $attrsSet = $this->eavAttribute->getAttributeSet(self::PRODUCT_ENTITY_TYPE_ID);

        return $this->getSearchResult()->setItems($attrsSet)->setTotalCount(count($attrsSet))->getOutput();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function processSearch() {
        $params = $this->getSearchCriteria()->getData();

        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $this->getCollection();

        foreach ($params as $k => $v) {
            if (!in_array($k, ['pageSize', 'currentPage', 'forceMode', 'dir', 'orderColumn'])) {
                if (preg_match('/>/', $v)) {
                    $collection->addFieldToFilter($k, ['gteq' => str_replace('>', '', $v)]);
                }
                elseif (preg_match('/</', $v)) {
                    $collection->addFieldToFilter($k, ['lteq' => str_replace('<', '', $v)]);
                }
                else
                    $collection->addFieldToFilter($k, $v);
            }
        }

        $collection->setPage($params['currentPage'], $params['pageSize']);

        if (isset($params['orderColumn'])) {
            if (!isset($params['dir']) || !in_array($params['dir'], ['DESC', 'ASC']))
                $params['dir'] = 'ASC';
            $collection->addOrder($params['orderColumn'], $params['dir']);
        }

        $this->setCollection($collection);

        return $this;
    }

    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getCollection() {
        return $this->collection;
    }

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     *
     * @return $this
     */
    protected function setCollection($collection) {
        $this->collection = $collection;

        return $this;
    }

    public function saveImage() {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $product = Mage::getModel('catalog/product')->load($this->getProductPostData('product_id'));
        if (file_exists($this->getProductPostData('image_path'))) {
            $product->addImageToMediaGallery($this->getProductPostData('image_path'), ['image', 'small_image', 'thumbnail'], false, false);

            $product->save();

            return $this->detail($this->getProductPostData('product_id'));
        }
        else {
            throw new Exception('File Image not found');
        }
    }
}