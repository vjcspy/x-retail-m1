<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/25/16
 * Time: 10:26 AM
 */
class  SM_XRetail_Model_ResourceModel_XRetailOrderInvoice extends SM_XRetail_Model_Api_ServiceAbstract {

    const CHECK_COMPLETE_ORDER = false;

    static $IS_WRONG_TOTAL = false;
    /**
     * @var Varien_Object
     */
    private $_invoiceData;

    /**
     * @var \SM_XRetail_Model_ResourceModel_OrderManagement
     */
    protected $_orderManagement;

    /**
     * @var
     */
    private $_totalPaid;

    /**
     * @var
     */
    private $_layawayData;

    /**
     * @var \SM_XRetail_Helper_Data
     */
    private $_dataHelper;

    /**
     * SM_XRetail_Model_ResourceModel_XRetailOrderInvoice constructor.
     */
    public function __construct() {

        $this->_orderManagement = Mage::getModel('xretail/resourceModel_orderManagement');
        $this->_dataHelper = Mage::helper('xretail');
        parent::__construct();
    }


    /**
     * @return mixed
     * @throws \Exception
     * @throws bool
     */
    public function invoiceOrder($requestData = null) {

        if (empty($requestData)) {
            $this->setInvoiceData($this->getRequest()->getParam('invoice_data'));
        }
        else {
            $this->setInvoiceData($requestData);
        }

        $data = $this->getInvoiceData()->getData('invoice');
        try {
            $invoice = $this->_initInvoice();
            if ($invoice) {

                if (!empty($data['capture_case'])) {
                    $invoice->setRequestedCaptureCase($data['capture_case']);
                }

                if (!is_null($this->getInvoiceData()->getData('comment_text'))) {
                    $invoice->addComment(
                        $this->getInvoiceData()->getData('comment_text'),
                        isset($data['comment_customer_notify']),
                        isset($data['is_visible_on_front'])
                    );
                }

                $invoice->register();

                if (!empty($data['send_email'])) {
                    $invoice->setEmailSent(true);
                }

                $invoice->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
                $invoice->getOrder()->setIsInProcess(true);

                if ($this->getInvoiceData()->getData('total_paid')) {
                    $order = $invoice->getOrder();
                    $order->setTotalPaid($this->getInvoiceData()->getData('total_paid'))
                          ->setBaseTotalPaid(
                              Mage::app()->getStore($invoice->getStoreId())->convertPrice(
                                  $this->getInvoiceData()->getData('total_paid')))
                          ->save();
                    if ($this->getInvoiceData()->getData('total_paid') < $order->getGrandTotal()) {
                        $this->_orderManagement->changeStateOrderById(
                            $order->getIncrementId(),
                            SM_XRetail_Model_ResourceModel_OrderManagement::STATE_LAYAWAY,
                            $this->getInvoiceData()->getData(
                                'layaway_comment'),
                            true);
                    }
                }
                $transactionSave = Mage::getModel('core/resource_transaction')
                                       ->addObject($invoice)
                                       ->addObject($invoice->getOrder());

                $shipment = false;
                if (!empty($data['do_shipment']) || (int)$invoice->getOrder()->getForcedDoShipmentWithInvoice()) {
                    $shipment = $this->_prepareShipment($invoice);
                    if ($shipment) {
                        $shipment->setEmailSent($invoice->getEmailSent());
                        $transactionSave->addObject($shipment);
                    }
                }
                $transactionSave->save();

                if (isset($shippingResponse) && $shippingResponse->hasErrors()) {
                    throw new Exception(
                        'The invoice and the shipment  have been created. The shipping label cannot be created at the moment.');
                }

                // send invoice/shipment emails
                $comment = isset($data['comment_customer_notify']) ? $data['comment_text'] : '';
                try {
                    $invoice->sendEmail(!empty($data['send_email']), $comment);
                }
                catch (Exception $e) {
                    Mage::logException($e);
                    throw new Exception('Unable to send the invoice email.');
                }
                if ($shipment) {
                    try {
                        $shipment->sendEmail(!empty($data['send_email']));
                    }
                    catch (Exception $e) {
                        Mage::logException($e);
                        throw new Exception('Unable to send the shipment email.');
                    }
                }

                return $invoice->getData();
            }
            else {
                throw new Exception("Cant' create invoice");
            }
        }
        catch (Exception $e) {
            Mage::logException($e);

            return $e->getMessage();
        }
    }

    public function updateLayaway($layawayData = null) {

//        $this->dummyLayaway();
        if (is_null($layawayData) || !$layawayData)
            $data = $this->getRequest()->getParam('layaway_data');
        else
            $data = $layawayData;

        $this->checkIsOfflineModeInvoice($data);
        if (!isset($data['payment']))
            $data['payment'] = array();

        $order = Mage::getModel('sales/order')->load($data['order']['order_id']);

        $this->processOrderLayaway($data, $order);

        $o = $order->getPayment()->getMethodInstance()->getInfoInstance()->setAdditionalData(
            serialize($this->_layawayData))->save();

        $oData = $o->getData();
        if (self::$IS_WRONG_TOTAL) {
            $oData['is_wrong_total'] = 1;
        }
        return $oData;
    }

    /**
     * TODO: update total paid, order state
     *
     * @param array $layawayData
     * @param \Mage_Sales_Model_Order $order
     *
     * @return $this
     * @throws \Exception
     */
    private function processOrderLayaway(array $layawayData, Mage_Sales_Model_Order $order) {

        if (self::CHECK_COMPLETE_ORDER
            && in_array(
                $order->getState(),
                array(
                    SM_XRetail_Model_ResourceModel_OrderManagement::STATE_COMPLETE,
                    // SM_XRetail_Model_ResourceModel_OrderManagement::STATE_PENDING_PAYMENT
                ))
        ) {
            throw new Exception("Can't edit complete order");
        }

        $paymentInstance = $order->getPayment()->getMethodInstance();
        if (is_null($paymentInstance))
            throw new Exception("Can't find payment method of this order");
        if ($paymentInstance->getCode() !==
            SM_XRetail_Model_Payment_XRetailMultiplePayment::CODE
        )
            throw new Exception("Layaway must use Multiple Payment method");

        $oldPaymentData = unserialize($paymentInstance->getInfoInstance()->getAdditionalData());

        $totalPaid = 0;
        foreach ($oldPaymentData as $k => $p) {
            if (in_array($k, array('store_id', 'check_date')))
                continue;
            else
                $totalPaid += floatval($p);
        }

        $this->_totalPaid = $totalPaid + $this->getTotalPaidFromLayawayData($layawayData);

        // order layaway must created invoice
        if (!$order->hasInvoices())
            $this->invoiceOrder(
                array(
                    'order_id' => $order->getEntityId(),
                    'invoice' =>
                        array(
                            'items' =>
                                array(// Nếu truyền lên mảng rỗng thì sẽ invoice toàn bộ items
                                ),
                            'comment_text' => 'Create Invoice for layaway order',
                        ),
                    /*Nếu mà truyền total_paid vào thì đã là manual => sử dụng split payment => layaway
                   * Tức là sẽ tạo invoice ngay khi tạo order để sử dụng layaway
                   */
                    'total_paid' => $this->_totalPaid,
                    'layaway_comment' => 'Create Invoice for layaway order'
                ));

        // calculate payment layaway
        $this->sumLayawayData($oldPaymentData, $layawayData['payment']);

        //save total paid
        $order->setTotalPaid($this->_totalPaid);
        $order->setBaseTotalPaid(
            $this->_totalPaid / (Mage::app()->getStore($oldPaymentData['store_id'])->convertPrice(1, false)));
        $order->save();

        $layawayComment
            = isset($layawayData['order']['layaway_comment']) ? $layawayData['order']['layaway_comment'] : '';

        if ($order->getGrandTotal() <= $this->_totalPaid) {
            if ($order->getGrandTotal() < $this->_totalPaid) {
                if (SM_XRetail_Model_ResourceModel_OrderManagement::$IS_OFFLINE_MODE) {
                    self::$IS_WRONG_TOTAL = true;
                }
                else
                    throw new Exception('Total paid larger than Grand Total');
            }
            if ($order->hasShipments()) {
                $this->_orderManagement->changeStateOrderById(
                    $order->getIncrementId(),
                    SM_XRetail_Model_ResourceModel_OrderManagement::STATE_COMPLETE,
                    $layawayComment,
                    true);
            }
            else {
                $this->_orderManagement->changeStateOrderById(
                    $order->getIncrementId(),
                    SM_XRetail_Model_ResourceModel_OrderManagement::STATE_PENDING,
                    $layawayComment,
                    true);
            }
        }
        else {
            if ($order->hasShipments()) {
                $this->_orderManagement->changeStateOrderById(
                    $order->getIncrementId(),
                    SM_XRetail_Model_ResourceModel_OrderManagement::STATE_PROCESSING,
                    $layawayComment,
                    true);
            }
            else {
                $this->_orderManagement->changeStateOrderById(
                    $order->getIncrementId(),
                    SM_XRetail_Model_ResourceModel_OrderManagement::STATE_PENDING,
                    $layawayComment,
                    true);
            }
        }

        return $this;
    }

    private function sumLayawayData($o, $n) {

        $storeId = $o['store_id'];

        unset($o['store_id']);
        unset($o['check_date']);
        unset($n['store_id']);
        unset($n['check_date']);

        $this->_layawayData = $this->_dataHelper->arraySumIdenticalKeys($o, $n);
        $this->_layawayData['store_id'] = $storeId;

        return $this;
    }

    private function getTotalPaidFromLayawayData($layawayData) {

        $totalPaid = 0;
        if (isset($layawayData['payment'])) {
            foreach ($layawayData['payment'] as $k => $p) {
                if (in_array($k, array('store_id', 'check_date')))
                    continue;
                else
                    $totalPaid += floatval($p);
            }

        }

        return $totalPaid;
    }

    /**
     * Prepare shipment
     *
     * @param Mage_Sales_Model_Order_Invoice $invoice
     *
     * @return Mage_Sales_Model_Order_Shipment
     */
    protected function _prepareShipment($invoice) {

        $savedQtys = $this->_getItemQtys();
        $shipment = Mage::getModel('sales/service_order', $invoice->getOrder())->prepareShipment($savedQtys);
        if (!$shipment->getTotalQty()) {
            return false;
        }


        $shipment->register();
        $tracks = $this->getInvoiceData()->getData('tracking');
        if ($tracks) {
            foreach ($tracks as $data) {
                $track = Mage::getModel('sales/order_shipment_track')
                             ->addData($data);
                $shipment->addTrack($track);
            }
        }

        return $shipment;
    }

    /**
     * @return Varien_Object
     */
    public function getInvoiceData() {

        return $this->_invoiceData;
    }

    /**
     * @param $invoiceData
     */
    public function setInvoiceData($invoiceData) {

        $this->_invoiceData = new Varien_Object($invoiceData);
    }

    /**
     * @param bool $update
     *
     * @return \Mage_Sales_Model_Order_Invoice
     * @throws \Exception
     */
    protected function _initInvoice($update = false) {

        $orderId = $this->getInvoiceData()->getData('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        /**
         * Check order existing
         */
        if (!$order->getId()) {
            throw new Exception('The order no longer exists.');
        }
        /**
         * Check invoice create availability
         */
        if (!$order->canInvoice()) {
            throw new Exception('The order does not allow creating an invoice.');
        }

        $savedQtys = $this->_getItemQtys();
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice($savedQtys);

        if (!$invoice->getTotalQty()) {
            throw new Exception('Cannot create an invoice without products.');
        }


        Mage::register('current_invoice', $invoice);

        return $invoice;
    }

    /**
     * Get requested items qty's from request
     */
    protected function _getItemQtys() {

        $data = $this->getRequest()->getParam('invoice');
        $qtys = isset($data['items']) ? $data['items'] : array();

        return $qtys;
    }

    /**
     * @return $this
     */
    protected function dummy() {

        $data = array(
            'order_id' => '339',
            'invoice' =>
                array(
                    'items' =>
                        array(
                            // Nếu truyền lên mảng rỗng thì sẽ invoice toàn bộ items
                            760 => '2',
                        ),
                    'comment_text' => '',
                ),
            /*Nếu mà truyền total_paid vào thì đã là manual => sử dụng split payment => layaway
           * Tức là sẽ tạo invoice ngay khi tạo order để sử dụng layaway
           */
            'total_paid' => 200,
            'layaway_comment' => ''
        );
        $this->getRequest()->setParam('invoice_data', $data);

        return $this;
    }

    protected function dummyLayaway() {

        $data = array(
            'order' => array(
                'order_id' => 202,
                'layaway_comment' => 'su dung layaway'
            ),
            'payment' => array(
//                'xpayment_dummy1' => 4.75
//'xpayment_dummy2' => 4.75,
//                'store_id'        => '1',
//                'check_date'      => '2016-03-21 09:07:15',
            ),
        );
        $this->getRequest()->setParam('layaway_data', $data);

        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    private function checkIsOfflineModeInvoice($data) {
        if (isset($data['is_offline_mode']) && $data['is_offline_mode'])
            SM_XRetail_Model_ResourceModel_OrderManagement::$IS_OFFLINE_MODE = true;
        return $this;
    }
}