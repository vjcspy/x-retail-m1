<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 3/19/16
 * Time: 10:55 AM
 *
 * @property float creditmemoGrandTotal
 * @property float creditmemoBaseGrandTotal
 */
class SM_XRetail_Model_ResourceModel_XRetailOrderRefund extends SM_XRetail_Model_Api_ServiceAbstract {

    const RETAIL_REFUND_ADJUST_CODE = 'retail_refund_adjust';
    const BASE_RETAIL_REFUND_ADJUST_CODE = 'base_retail_refund_adjust';

    /**
     * @var []
     */
    protected $_creditmemoData = array();

    /**
     * @var \SM_XRetail_Helper_Data
     */
    protected $_helper;

    /**
     * @var
     */
    protected $_itemInfo;

    /**
     * @var
     */
    protected $_order;
    /**
     * @var \SM_XRetail_Model_ResourceModel_XRetailOrderOnline
     */
    private $xretailOrderOnline;
    /**
     * @var \Mage_Sales_Model_Order
     */
    private $orderModel;

    /**
     * @var Mage_Sales_Model_Order
     */
    private $order;
    /**
     * @var
     */
    static $retailRefundAdjust;
    /**
     * @var
     */
    private $oldTotalPaid;
    /**
     * @var
     */
    private $stateRefund;
    private $baseOldTotalPaid;
    private $sqlDirect;


    /**
     * SM_XRetail_Model_ResourceModel_XRetailOrderRefund constructor.
     */
    public function __construct() {
        $this->_helper    = Mage::helper('xretail');
        $this->orderModel = Mage::getModel('sales/order');
        $this->sqlDirect  = Mage::helper('xretail/sqlDirect');
        parent::__construct();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getInfo() {
        $orderId = $this->getRequest()->getParam('order_id');
        if (is_null($orderId))
            throw new Exception('Must have param order_id');
        $this->_creditmemoData['order_id'] = $orderId;
        $creditmemo                        = $this->_initCreditmemo();

        return array(
            'order'            => $this->_order->getData(),
            'billing_address'  => $this->_order->getBillingAddress()->getData(),
            'shipping_address' => $this->_order->getShippingAddress()->getData(),
            'items'            => $this->getItemsInfo($creditmemo),
        );
    }

    public function cancelOrder() {
        $cancelOrder      = $this->getRequest()->getParam('cancel_data');
        $data             = array(
            'items'               =>
                array(),
            'order_id'            => 332,
            'do_offline'          => '1',
            'comment_text'        => '',
            'shipping_amount'     => '0',
            'adjustment_positive' => '0',
            'adjustment_negative' => '0',
        );
        $data['order_id'] = $cancelOrder['order_id'];

        return $this->refundOrder($data);
    }

    /**
     * @param \Mage_Sales_Model_Order_Creditmemo $creditmemo
     *
     * @return mixed
     */
    protected function getItemsInfo(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        if (is_null($this->_itemInfo)) {
            $items = $creditmemo->getAllItems();
            if (count($items) > 0) {
                foreach ($items as $item) {
                    $this->_itemInfo[] = $item->getData();
                }
            }
        }

        return $this->_itemInfo;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function refundOrder($creditmemoData = null) {
        // $this->dummy();

        $this->checkRetailRefund();

        if (is_null($creditmemoData) || !$creditmemoData) {
            $this->_creditmemoData = $this->getRequest()->getParam('creditmemo');
        } else {
            $this->getRequest()->setParam('creditmemo', $creditmemoData);
            $this->_creditmemoData = $creditmemoData;
        }

        $this->_searchCriteria['shipping_amount'] = 0;

        if (is_null($this->_creditmemoData))
            throw new Exception('Must have param creditmemo');

        $creditmemo = $this->_initCreditmemo();

        if ($creditmemo) {

            $this->checkTotalRefund($creditmemo);
            if (($creditmemo->getGrandTotal() <= 0) && (!$creditmemo->getAllowZeroGrandTotal())) {
                Mage::throwException(
                    ('Credit memo\'s total must be positive.')
                );
            }
            $comment = '';
            if (!empty($data['comment_text'])) {
                $creditmemo->addComment(
                    $data['comment_text'],
                    isset($data['comment_customer_notify']),
                    isset($data['is_visible_on_front'])
                );
                if (isset($data['comment_customer_notify'])) {
                    $comment = $data['comment_text'];
                }
            }

            if (isset($data['do_refund'])) {
                $creditmemo->setRefundRequested(true);
            }

            if (isset($data['do_offline'])) {
                $creditmemo->setOfflineRequested((bool)(int)$data['do_offline']);
            }

            $creditmemo->register();
            
            if (!empty($data['send_email'])) {
                $creditmemo->setEmailSent(true);
            }

            $creditmemo->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
            $this->_saveCreditmemo($creditmemo);
            $creditmemo->sendEmail(!empty($data['send_email']), $comment);
            
            $this->resetTotalPaid();
            return $creditmemo->getData();
        }
        else {
            throw new Exception('Can\'t credit memo');
        }
    }

    /**
     * Check if creditmeno can be created for order
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @return bool
     * @throws \Exception
     */
    protected function _canCreditmemo($order) {
        /**
         * Check order existing
         */
        if (!$order->getId())
            throw new Exception ('The order no longer exists.');


        /**
         * Check creditmemo create availability
         */
        if (!$order->canCreditmemo())
            throw new Exception ('Cannot create credit memo for the order.');

        return true;
    }

    /**
     * Initialize creditmemo model instance
     *
     * @return Mage_Sales_Model_Order_Creditmemo
     */
    protected function _initCreditmemo($update = false) {

        $data = $this->_creditmemoData;

        $order = $this->order;

        if (!$this->_canCreditmemo($order)) {
            return false;
        }

        $savedData = $this->_getItemData();

        $qtys        = array();
        $backToStock = array();
        foreach ($savedData as $orderItemId => $itemData) {
            if (isset($itemData['qty'])) {
                $qtys[$orderItemId] = $itemData['qty'];
            }
            if (isset($itemData['back_to_stock'])) {
                $backToStock[$orderItemId] = true;
            }
        }
        $data['qtys'] = $qtys;

        $service    = Mage::getModel('sales/service_order', $order);
        $creditmemo = $service->prepareCreditmemo($data);

        /**
         * Process back to stock flags
         */
        foreach ($creditmemo->getAllItems() as $creditmemoItem) {
            $orderItem = $creditmemoItem->getOrderItem();
            $parentId  = $orderItem->getParentItemId();
            if (isset($backToStock[$orderItem->getId()])) {
                $creditmemoItem->setBackToStock(true);
            }
            elseif ($orderItem->getParentItem() && isset($backToStock[$parentId]) && $backToStock[$parentId]) {
                $creditmemoItem->setBackToStock(true);
            }
            elseif (empty($savedData)) {
                $creditmemoItem->setBackToStock(Mage::helper('cataloginventory')->isAutoReturnEnabled());
            }
            else {
                $creditmemoItem->setBackToStock(false);
            }
        }

        $args = array('creditmemo' => $creditmemo, 'request' => $this->getRequest());
        Mage::dispatchEvent('adminhtml_sales_order_creditmemo_register_before', $args);

//        Mage::register('current_creditmemo', $creditmemo);
        return $creditmemo;
    }

    /**
     * Get requested items qtys and return to stock flags
     */
    protected function _getItemData() {
        $data = $this->_creditmemoData;

        if (isset($data['items'])) {
            $qtys = $data['items'];
        }
        else {
            $qtys = array();
        }

        return $qtys;
    }

    /**
     * Save creditmemo and related order, invoice in one transaction
     *
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     *
     * @return $this
     */
    protected function _saveCreditmemo($creditmemo) {
        $transactionSave = Mage::getModel('core/resource_transaction')
                               ->addObject($creditmemo)
                               ->addObject($creditmemo->getOrder());
        if ($creditmemo->getInvoice()) {
            $transactionSave->addObject($creditmemo->getInvoice());
        }
        $transactionSave->save();

        return $this;
    }

    private function dummy() {
        $data = array(
            'items'               =>
                array(
                    3826 =>
                        array(
                            'qty' => '1',
                        ),
                ),
            'order_id'            => 2123,
            'do_offline'          => '1',
            'comment_text'        => '',
            'shipping_amount'     => '0',
            'adjustment_positive' => '0',
            'adjustment_negative' => '0',
        );
        $this->getRequest()->setParam('creditmemo', $data);
    }

    /**
     * @return \SM_XRetail_Model_ResourceModel_XRetailOrderOnline
     */
    private function getXRetailOrderOnline() {
        if (is_null($this->xretailOrderOnline))
            $this->xretailOrderOnline = Mage::getModel('xretail/resourceModel_xRetailOrderOnline');

        return $this->xretailOrderOnline;
    }

    /**
     * Data có 2 trường là:  'adjustment_positive' và 'adjustment_negative'
     * Khi chủ store ngoài muốn refund theo giá của sản phầm mà muốn thêm vào tiền phí hoặc tiền bồi thường thì sẽ thêm vào giá trị vào trường này
     *
     * Function này kiểm tra xem order có sử dụng partial payment sau đó đi refund hay không.
     * Nếu sử dụng partial thì sẽ để total paid = grand total để có thể refund được
     *
     * @return int
     * @throws \Exception
     */
    private function checkRetailRefund() {
        $data                   = $this->getRequest()->getParam('creditmemo');
        $this->order            = $this->orderModel->load($data['order_id']);
        $this->oldTotalPaid     = $this->order->getTotalPaid();
        $this->baseOldTotalPaid = $this->order->getBaseTotalPaid();

        $paymentInstance = $this->order->getPayment()->getMethodInstance();

        /*
         * Nếu order sử dụng partial payment thì set lại total paid để có thể refund được.
         * Sau bước refund thì set lại
         * Tuyệt đối không được change total paid. Nếu sửa thì sai bản chất.
         * */

        if ($paymentInstance->getCode() == SM_XRetail_Model_Payment_XRetailMultiplePayment::CODE
            && $this->order->getTotalPaid() < $this->order->getGrandTotal()) {
            $this->order->setTotalPaid($this->order->getGrandTotal())
                        ->setBaseTotalPaid($this->order->getBaseGrandTotal())->save();

            return $this->stateRefund = 1;
        }

        return $this->stateRefund = -1;
    }

    private function checkTotalRefund(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        if ($this->stateRefund === 1) {
            if ($creditmemo->getGrandTotal() > $this->oldTotalPaid) {
                $this->creditmemoGrandTotal = $creditmemo->getGrandTotal();
                $this->creditmemoBaseGrandTotal = $creditmemo->getBaseGrandTotal();
                $reCalGrandTotal     = $this->oldTotalPaid;
                $reCalBaseGrandTotal = round($reCalGrandTotal / Mage::app()->getStore($this->order->getStoreId())->convertPrice(1, false, false), 2);

                $creditmemo->setData(self::RETAIL_REFUND_ADJUST_CODE, $creditmemo->getGrandTotal() - $this->oldTotalPaid)
                           ->setData(self::BASE_RETAIL_REFUND_ADJUST_CODE, $creditmemo->getBaseGrandTotal() - $reCalBaseGrandTotal)
                           ->setGrandTotal($reCalGrandTotal)
                           ->setBaseGrandTotal($reCalGrandTotal);

            }
        }

        return $this;
    }

    private function resetTotalPaid(){
        if($this->stateRefund === 1){
        $prefix = (string)Mage::getConfig()->getTablePrefix();
        if ($prefix)
            $prefix = $prefix . '.';

        $this->sqlDirect->fetchQuery(
            "UPDATE " .
            $prefix . "`sales_flat_order` SET retail_refund_adjust = " . floatval($this->creditmemoGrandTotal - $this->oldTotalPaid) .
            ",base_retail_refund_adjust=" . floatval($this->creditmemoBaseGrandTotal - round($this->oldTotalPaid / Mage::app()->getStore($this->order->getStoreId())->convertPrice(1, false, false), 2)) .
            ",base_total_due=" . floatval($this->order->getBaseGrandTotal() - $this->baseOldTotalPaid - floatval($this->creditmemoBaseGrandTotal - round($this->oldTotalPaid / Mage::app()->getStore($this->order->getStoreId())->convertPrice(1, false, false), 2))) .
            ",total_due=" .floatval($this->order->getGrandTotal() - $this->oldTotalPaid - floatval($this->creditmemoGrandTotal - $this->oldTotalPaid)) .
            ",total_paid=" .floatval($this->oldTotalPaid) .
            ",base_total_paid=" .floatval($this->baseOldTotalPaid) ."  WHERE entity_id = " . (int)$this->order->getId()
        );}
    }
}