<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/14/16
 * Time: 10:35 AM
 */
class SM_XRetail_Model_Shipping_ShippmentManager extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var SM_XRetail_Model_Api_Configuration
     */
    protected $_config;

    /**
     *
     */
    const PATH_SHIPMENT_CONFIG = 'xretail/config/data';
    /**
     *
     */
    const SHIPMENT_PARAM = 'shipment_data';

    /**
     * SM_XRetail_Model_Shipping_ShippmentManager constructor.
     */
    public function __construct() {
        $this->_config = Mage::getSingleton('xretail/api_configuration');
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function update() {
        //dummy data
        $this->dummyData();

        $shipmentData = $this->getRequest()->getParam(self::SHIPMENT_PARAM);
        $this->_config->setConfig(self::PATH_SHIPMENT_CONFIG, json_encode($shipmentData));

        return json_decode($this->_config->getConfig(self::PATH_SHIPMENT_CONFIG)->getData('value'), true);
    }

    /**
     *
     */
    protected function dummyData() {
        $data = array(
            array(
                'method_code'  => 'xretail_dummy_1_code',
                'method_title' => 'X-POS Shipping',
                'method_price' => 10,
                'method_cost'  => 10,
            ),
            // array(
            //     'method_code'  => 'xretail_dummy_2_code',
            //     'method_title' => 'xretail_dummy_2_title',
            //     'method_price' => 20,
            //     'method_cost'  => 20,
            // ),
            // array(
            //     'method_code'  => 'xretail_dummy_3_code',
            //     'method_title' => 'xretail_dummy_3_title',
            //     'method_price' => 30,
            //     'method_cost'  => 30,
            // ),
            // array(
            //     'method_code'  => 'xretail_dummy_4_code',
            //     'method_title' => 'xretail_dummy_4_title',
            //     'method_price' => 40,
            //     'method_cost'  => 40,
            // ),
        );
        $this->getRequest()->setParam(self::SHIPMENT_PARAM, $data);
    }
}
