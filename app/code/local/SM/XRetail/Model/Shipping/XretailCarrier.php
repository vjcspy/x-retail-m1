<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/14/16
 * Time: 10:17 AM
 */
class SM_XRetail_Model_Shipping_XretailCarrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {

    /**
     * @var string
     */
    protected $_code = self::CODE;

    protected $getAmountFromClient = true;
    const XRETAIL_SHIPPING_AMOUNT = 'xretail_shipping_amount';
    /**
     *
     */
    const XRETAIL_SHIPPING_TITLE = 'X-Retail Shipping';
    const CODE = 'xretail_shipping';

    const ENABLE_FOR_ADMIN_STORE = true;

    /**
     * @var
     */
    protected $_dataShipment;

    /**
     * @var SM_XRetail_Model_Api_Configuration
     */
    protected $_config;
    /**
     * @var
     */
    protected $_rates;
    private $realtimeHelperData;

    /**
     * SM_XRetail_Model_Shipping_XretailCarrier constructor.
     */
    public function __construct() {

        $this->_config            = Mage::getSingleton('xretail/api_configuration');
        $this->realtimeHelperData = Mage::helper('xretail/realtime_data');
        parent::__construct();
    }

    /**
     * Collect and get rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     *
     * @return Mage_Shipping_Model_Rate_Result|bool|null
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request) {

        $result = Mage::getModel('shipping/rate_result');
        /* @var $result Mage_Shipping_Model_Rate_Result */

        if ((Mage::app()->getStore()->isAdmin() && self::ENABLE_FOR_ADMIN_STORE) || $this->fromApi())
            foreach ($this->getDummyMethod() as $r) {
                $result->append($r);
            }

        return $result;
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods() {

        if (is_null($this->_dataShipment)) {
            $data                                = json_decode($this->_config->getConfig('xretail/config/data')->getData('value'), true);
            $this->_dataShipment                 = array();
            $this->_dataShipment['allow_method'] = array();
            $this->_dataShipment['method_data']  = array();
            foreach ($data as $item) {
                $this->_dataShipment['allow_method'][$item['method_code']] = $item['method_title'];
                $this->_dataShipment['method_data'][$item['method_code']]  = $item;
            }

            $this->_dataShipment['allow_method']['default_shipping_xretail'] = 'Default Shipping X-Retail';
            $this->_dataShipment['method_data']['default_shipping_xretail']  = array(
                'method_title' => 'Default Shipping X-Retail',
                'method_price' => 0,
                'method_cost'  => 0
            );

        }

        return $this->_dataShipment['allow_method'];
    }

    /**
     * @return array
     */
    protected function getDummyMethod() {

        if (is_null($this->_rates)) {
            $this->_rates = array();
            foreach ($this->getAllowedMethods() as $method => $title) {
                $rate = Mage::getModel('shipping/rate_result_method');
                /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
                $rate->setCarrier($this->_code);
                $rate->setCarrierTitle(self::XRETAIL_SHIPPING_TITLE);
                $rate->setMethod($method);
                $rate->setMethodTitle($this->_dataShipment['method_data'][$method]['method_title']);
                if ($this->getAmountFromClient &&
                    !is_null($this->realtimeHelperData->registry(SM_XRetail_Model_Shipping_XretailCarrier::XRETAIL_SHIPPING_AMOUNT))
                ) {
                    $rate->setPrice($this->realtimeHelperData->registry(SM_XRetail_Model_Shipping_XretailCarrier::XRETAIL_SHIPPING_AMOUNT));
                    $rate->setCost($this->realtimeHelperData->registry(SM_XRetail_Model_Shipping_XretailCarrier::XRETAIL_SHIPPING_AMOUNT));
                }
                $this->_rates[] = $rate;
            }
        }

        return $this->_rates;
    }

    protected function fromApi() {

        return Mage::registry('from_api') == true ? true : false;
    }
}
