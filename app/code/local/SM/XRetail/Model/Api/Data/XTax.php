<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/25/16
 * Time: 5:50 PM
 */
class SM_XRetail_Model_Api_Data_XTax extends SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract {
    /**
     *
     * @return int|null
     */
    public function getTaxCalculationRateId() {
        // TODO: Implement getTaxCalculationRateId() method.
        return $this->getData('tax_calculation_rate_id');
    }

    /**
     *
     * @return int|null
     */
    public function getTaxCalculationRuleId() {
        // TODO: Implement getTaxCalculationRuleId() method.
        return $this->getData('tax_calculation_rule_id');
    }

    /**
     *
     * @return int|null
     */
    public function getCustomerTaxClassId() {
        // TODO: Implement getCustomerTaxClassId() method.
        return $this->getData('customer_tax_class_id');
    }

    /**
     *
     * @return int|null
     */
    public function getProductTaxClassId() {
        // TODO: Implement getProductTaxClassId() method.
        return $this->getData('product_tax_class_id');
    }

    /**
     *
     * @return int|null
     */
    public function getPriority() {
        // TODO: Implement getPriority() method.
        return $this->getData('priority');
    }

    /**
     *
     * @return int|null
     */
    public function getPosition() {
        // TODO: Implement getPosition() method.
        return $this->getData('position');
    }

    /**
     *
     * @return int|null
     */
    public function getCalculateSubtotal() {
        // TODO: Implement getCalculateSubtotal() method.
        return $this->getData('calculate_subtotal');
    }

    /**
     *
     * @return string|null
     */
    public function getValue() {
        // TODO: Implement getValue() method.
        return $this->getData('value');
    }

    /**
     *
     * @return string|null
     */
    public function getTaxCountryId() {
        // TODO: Implement getTaxCountryId() method.
        return $this->getData('tax_country_id');
    }

    /**
     *
     * @return int|null
     */
    public function getTaxRegionId() {
        // TODO: Implement getTaxRegionId() method.
        return $this->getData('tax_region_id');
    }

    /**
     *
     * @return string|null
     */
    public function getTaxPostcode() {
        // TODO: Implement getTaxPostcode() method.
        return $this->getData('tax_postcode');
    }

    /**
     *
     * @return string|null
     */
    public function getCode() {
        // TODO: Implement getCode() method.
        return $this->getData('code');
    }

    /**
     *
     * @return string|null
     */
    public function getTitle() {
        // TODO: Implement getTitle() method.
        return $this->getData('title');
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setTaxCalculationRateId($id) {
        // TODO: Implement setTaxCalculationRateId() method.
        return $this->setData('tax_calculation_rate_id', $id);
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setTaxCalculationRuleId($id) {
        // TODO: Implement setTaxCalculationRuleId() method.
        return $this->setData('tax_calculation_rule_id', $id);
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setCustomerTaxClassId($id) {
        // TODO: Implement setCustomerTaxClassId() method.
        return $this->setData('customer_tax_class_id', $id);
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setProductTaxClassId($id) {
        // TODO: Implement setProductTaxClassId() method.
        return $this->setData('product_tax_class_id', $id);
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setPriority($id) {
        // TODO: Implement setPriority() method.
        return $this->setData('priority', $id);
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setPosition($id) {
        // TODO: Implement setPosition() method.
        return $this->setData('position', $id);
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setCalculateSubtotal($id) {
        // TODO: Implement setCalculateSubtotal() method.
        return $this->setData('calculate_subtotal', $id);
    }

    /**
     *
     * @param string $id
     * @return $this
     */
    public function setValue($id) {
        // TODO: Implement setValue() method.
        return $this->setData('value', $id);
    }

    /**
     *
     * @param string $id
     * @return $this
     */
    public function setTaxCountryId($id) {
        // TODO: Implement setTaxCountryId() method.
        return $this->setData('tax_country_id', $id);
    }

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setTaxRegionId($id) {
        // TODO: Implement setTaxRegionId() method.
        return $this->setData('tax_region_id', $id);
    }

    /**
     *
     * @param string $id
     * @return $this
     */
    public function setTaxPostcode($id) {
        // TODO: Implement setTaxPostcode() method.
        return $this->setData('tax_postcode', $id);
    }

    /**
     *
     * @param string $id
     * @return $this
     */
    public function setCode($id) {
        // TODO: Implement setCode() method.
        return $this->setData('code', $id);
    }

    /**
     *
     * @param string $id
     * @return $this
     */
    public function setTitle($id) {
        // TODO: Implement setTitle() method.
        return $this->setData('title', $id);
    }
}
