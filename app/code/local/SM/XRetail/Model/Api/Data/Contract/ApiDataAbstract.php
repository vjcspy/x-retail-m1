<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/22/16
 * Time: 6:15 PM
 */
class SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract extends Varien_Object {

    /**
     *
     */
    const GET_METHOD = 'get';
    /**
     * @var []
     */
    protected $_dataOutput;

    /**
     * @return mixed
     */
    public function getOutput() {
        if (is_null($this->_dataOutput)) {
            $methods = $this->getAllGetApiMethod();
            foreach ($methods as $method) {
                if (substr($method, 0, 3) === self::GET_METHOD) {
                    $key = $this->_underscore(substr($method, 3));
                    $this->_dataOutput[$key] = call_user_func_array(array($this, $method), array());
                    if ($this->_dataOutput[$key] instanceof SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract)
                        $this->_dataOutput[$key] = $this->_dataOutput[$key]->getOutput();
                }
            }
        }
        return $this->_dataOutput;
    }

    /**
     * @var []
     */
    protected $_allGetApiMethod;

    /**
     * @return mixed
     */
    public function getAllGetApiMethod() {
        if (is_null($this->_allGetApiMethod)) {
            $class = new ReflectionClass(get_class($this));
            $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
            foreach ($methods as $method) {
                if ($method->getDeclaringClass()->getName() == get_class($this))
                    $this->_allGetApiMethod[] = $method->getName();

            }
        }
        return $this->_allGetApiMethod;
    }
}
