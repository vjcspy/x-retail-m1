<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/22/16
 * Time: 6:12 PM
 */
class SM_XRetail_Model_Api_Data_XProduct extends SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract {

    public function getId() {

        return $this->getData('entity_id');
    }

    public function getSku() {

        return $this->getData('sku');
    }

    public function getName() {

        return $this->getData('name');
    }

    public function getAttributeSetId() {

        return $this->getData('attribute_set_id');
    }

    public function getPrice() {

        return $this->getData('price');
    }

    public function getOriginPrice() {

        return $this->getData('origin_price');
    }

    public function getStoreRate() {

        return $this->getData('store_rate');
    }


    public function getStatus() {

        return $this->getData('status');
    }

    public function getVisibility() {

        return $this->getData('visibility');
    }

    public function getTypeId() {

        return $this->getData('type_id');
    }

    public function getTaxClassId() {

        return $this->getData('tax_class_id');
    }

    public function getCategoryId() {
        if (!!$this->getData('category_id'))
            return explode(',', $this->getData('category_id'));
        else
            return array();
    }

    public function getWebsites() {
        if (!!$this->getData('website_ids'))
            return explode(',', $this->getData('website_ids'));
        else
            return array();
    }

    public function getCustomAttributes() {

        if (Mage::helper('xretail/dataConfig')->getApiGetCustomAttributes()) {
            $customAtt  = array();
            $attributes = Mage::helper('xretail/product')->getAllCustomAttributes();
            foreach ($attributes as $attribute) {
                $val = $this->getData($attribute['value']);
                if (!is_null($val))
                    $customAtt[$attribute['value']] = $val;
            }

            return $customAtt;
        }
        else
            return array('bocked_by_config');
    }

    public function getXOptions() {

        return $this->getData('x_options');
    }

    public function getStockItems() {

        return $this->getData('stock_items');
    }

    public function getDescription() {

        return $this->getData('description');
    }

    public function getShortDescription() {

        return $this->getData('short_description');
    }

    public function getSpecialPrice() {

        return $this->getData('special_price');
    }

    public function getSpecialFromDate() {

        return $this->getData('special_from_date');
    }

    public function getSpecialToDate() {

        return $this->getData('special_to_date');
    }

    public function getOriginImage() {

        if (is_null($this->getImage()) || $this->getImage() == 'no_selection' || !$this->getImage())
            return null;
        else
            return Mage::getModel('catalog/product_media_config')->getMediaUrl($this->getImage());
    }

    public function getUrlKey() {
        return $this->getData('url_key');
    }
}
