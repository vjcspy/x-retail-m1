<?php

/**
 * Created by khoild@smartosc.com/mr.vjcspy@gmail.com
 * User: vjcspy
 * Date: 23/04/2016
 * Time: 11:31
 */
class SM_XRetail_Model_Api_Data_XRegion extends SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract {

    /**
     * @return mixed
     */
    public function getName() {
        return $this->getData('name');
    }

    /**
     * @return mixed
     */
    public function getCountryId() {
        return $this->getData('country_id');
    }

    /**
     * @return mixed
     */
    public function getRegionId() {
        return $this->getData('region_id');
    }

    /**
     * @return mixed
     */
    public function getCode() {
        return $this->getData('code');
    }

}