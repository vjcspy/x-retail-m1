<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/23/16
 * Time: 10:33 PM
 */
class SM_XRetail_Model_Api_Data_XCustomer extends SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract {

    protected $_add;

    public function getId() {
        return $this->getData('entity_id');
    }

    public function getGroupId() {
        return $this->getData('group_id');
    }

    public function getDefaultBilling() {
        return $this->getData('default_billing');
    }

    public function getDefaultShipping() {
        return $this->getData('default_shipping');
    }

    public function getEmail() {
        return $this->getData('email');
    }

    public function getFirstName() {
        return $this->getData('firstname');
    }

    public function getLastName() {
        return $this->getData('lastname');
    }

    public function getGender() {
        return $this->getData('gender');
    }

    public function getStoreId() {
        return $this->getData('store_id');
    }

    public function getWebsiteId() {
        return $this->getData('website_id');
    }

    public function getAddress() {
        if (is_null($this->_add)) {
            /* @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer')->load($this->getData('entity_id'));
            foreach ($customer->getAddresses() as $address) {
                $this->_add[] = $address->getData();
            }
        }
        return $this->_add;
    }

    public function getTaxClassId() {
        return $this->getData('tax_class_id');
    }

}
