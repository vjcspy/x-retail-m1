<?php

/**
 * Created by khoild@smartosc.com/mr.vjcspy@gmail.com
 * User: vjcspy
 * Date: 23/04/2016
 * Time: 10:49
 */
class SM_XRetail_Model_Api_Data_XCountry extends SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract {

    /**
     * @return mixed
     */
    public function getName() {
        return $this->getData('country_name');
    }

    /**
     * @return mixed
     */
    public function getCountryId() {
        return $this->getData('country_id');
    }
}