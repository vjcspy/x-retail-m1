<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/22/16
 * Time: 5:30 PM
 */
abstract class SM_XRetail_Model_Api_ServiceAbstract extends Mage_Core_Model_Abstract {

    /**
     * @var Mage_Core_Controller_Request_Http
     */
    public $_request;

    /**
     * @var SM_XRetail_Model_Api_SearchResult
     */
    public $_searchResult;

    /**
     * @var SM_XRetail_Helper_Data
     */
    public $_dataConfig;

    /**
     * @return SM_XRetail_Model_Api_SearchResult
     */
    public function getSearchResult() {
        return $this->_searchResult;
    }

    /**
     * @param SM_XRetail_Model_Api_SearchResult $searchResult
     */
    public function setSearchResult($searchResult) {
        $this->_searchResult = $searchResult;
    }

    /**
     * @var SM_XRetail_Helper_Realtime_CacheManagement
     */
    protected $_xCache;

    /**
     * SM_XRetail_Model_ResourceModel_ProductManagement constructor.
     */
    public function __construct() {
        $this->_request = Mage::app()->getRequest();
        $this->_searchResult = Mage::getModel('xretail/api_searchResult');
        $this->_dataConfig = Mage::helper('xretail/dataConfig');
        $this->_xCache = Mage::helper('xretail/realtime_cacheManagement');
        parent::__construct();
    }

    /**
     * @var Varien_Object
     */
    public $_searchCriteria;

    /**
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest() {
        return $this->_request;
    }

    /**
     * Retrieve Search Criteria As DataObject
     * @return Varien_Object
     * @throws Exception
     */
    public function getSearchCriteria() {
        if (is_null($this->_searchCriteria))
            if (is_null($this->getRequest()->getParam('searchCriteria')))
                throw new Exception('Not found field: searchCriteria');
            else
                $this->_searchCriteria = new Varien_Object($this->getRequest()->getParam('searchCriteria'));
        return $this->_searchCriteria;
    }
}
