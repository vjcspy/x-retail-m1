<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/22/16
 * Time: 5:47 PM
 */
class SM_XRetail_Model_Api_SearchResult extends Varien_Object {

    /**
     * @var SM_XRetail_Helper_Data
     */
    protected $_dataConfig;

    /**
     * SM_XRetail_Model_Api_SearchResult constructor.
     */
    public function __construct() {
        $this->_dataConfig = Mage::helper('xretail');
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getItems() {
        return $this->getData('items');
    }

    /**
     * @param array $items
     * @return $this
     */
    public function setItems(array $items) {
        $this->setData('items', $items);
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTotalCount() {
        return $this->getData('total_count');
    }

    /**
     * @param $totalCount
     * @return $this
     */
    public function setTotalCount($totalCount) {
        $this->setData('total_count', $totalCount);
        return $this;
    }

    /**
     * Get Client Cache Key
     *
     * @return string|null
     */
    public function getClientKey() {
        return $this->getData('client_key');
    }

    /**
     * Set Client Cache Key
     *
     * @param string $clientKey
     * @return $this
     */
    public function setClientKey($clientKey) {
        $this->setData('client_key', $clientKey);
        return $this;
    }


    /**
     * @return mixed
     */
    public function getSearchCriteria() {
        return $this->getData('search_criteria');
    }

    /**
     * @param Varien_Object $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(Varien_Object $searchCriteria) {
        $this->setData('search_criteria', $searchCriteria->getData());
        return $this;
    }

    /**
     * @return array
     */
    public function getOutput() {
        $items = array();

        foreach ($this->getItems() as $item) {
            if ($item instanceof SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract)
                /* @var $item SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract */
                $items[] = $item->getOutput();
            else
                $items[] = $item;
        }

        return $this->_dataConfig->formatDataOutput(array(
            'items' => $items,
            'search_criteria' => $this->getSearchCriteria(),
            'total_count' => $this->getTotalCount()
        ));
    }
}
