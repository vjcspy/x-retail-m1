<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 11/10/2016
 * Time: 14:49
 */
class SM_XRetail_Model_Tax_Sales_Total_Quote_Tax extends Mage_Tax_Model_Sales_Total_Quote_Tax {


    /**
     * @var \Mage_Core_Helper_Abstract|\SM_XRetail_Helper_Data
     */
    private $retailHelper;

    /**
     * SM_XRetail_Model_Tax_Sales_Total_Quote_Tax constructor.
     */
    public function __construct() {
        $this->retailHelper = Mage::helper('xretail');
        parent::__construct();
    }

    protected function _processHiddenTaxes() {
        if ($this->retailHelper->registry(SM_XRetail_Model_Totals_Quote_DiscountPerItems::HIDDEN_TAX))
            $this->_hiddenTaxes = array_merge(
                $this->_hiddenTaxes,
                $this->retailHelper->registry(SM_XRetail_Model_Totals_Quote_DiscountPerItems::HIDDEN_TAX));

        parent::_processHiddenTaxes();
    }
}