<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 2:49 PM
 */
class SM_XRetail_Model_ApiControllerInterceptor extends Mage_Core_Model_Abstract {

    /**
     * @param Varien_Event_Observer $observer
     * @return mixed
     */
    protected $postRequests = array(
        'save-order' => 'createOrder',
        'xorder' => 'createOrder',
        'loadblock' => 'createOrder',
        'customer-save' => 'Customer',
        'refund' => 'Exchange',
        'exchange' => 'Exchange',
    );

    protected $getRequests = array(
        'customer' => 'Customer',
        'xupdate' => 'Update',
        'xproduct' => 'Product',
        'xtax-rule' => 'TaxRule',
        'xtax-rate' => 'TaxRate',
        'store' => 'Store'
    );

    public function checkPath(Varien_Event_Observer $observer)
    {
        $apiController = $observer->getData('apiController');
        // get data as json
        $data = json_decode(file_get_contents('php://input'), true);
        if (!is_null($data)){
            $pathInfo = $apiController->getRequest()->getOriginalPathInfo();
            $postAction = substr($pathInfo, 17);
            if(array_key_exists($postAction,  $this->postRequests)){
                $data['action'] = $postAction;
                $data = Mage::helper('xretail/transformers_'.$this->postRequests[$postAction])->transformPostData($data);
            }
            $apiController->getRequest()->setParams($data);
        }

        return $apiController->checkPath();
    }

    public function transformer(Varien_Event_Observer $observer)
    {
        $apiController = $observer->getData('apiController');
        $params = array_keys($apiController->getRequest()->getParams());

        if(array_key_exists($params[0], $this->getRequests)){
            $rawOutPut = $apiController->getOutput();
            if(array_key_exists('items', $rawOutPut) && $params[0] != 'xupdate'){
                $rawOutPut['items'] = Mage::helper('xretail/transformers_'.$this->getRequests[$params[0]])->transformPullData($rawOutPut['items']);
            }else{
                $rawOutPut = Mage::helper('xretail/transformers_'.$this->getRequests[$params[0]])->transformPullData($rawOutPut);
            }

            $apiController->setOutput($rawOutPut);
        }
    }
}