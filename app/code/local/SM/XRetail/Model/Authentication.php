<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/24/16
 * Time: 10:40 AM
 */
class SM_XRetail_Model_Authentication {

    private $_configuration;
    const PATH_KEY = 'core/config/key_x';
    const HEADER_AUTHENTICATION_CODE = 'Authorization-Code';
    const HEADER_KEY_NAME = 'Black-Hole';

    public function __construct() {
        $this->_configuration = Mage::getModel('xretail/api_configuration');
    }

    public function login() {
        $user = Mage::getSingleton('admin/session')->login('admin', 'admin123', null);

        Mage::getSingleton('admin/session')->setUser($user);
    }

    public function authenticate(SM_XRetail_V1Controller $controller) {
        if ($controller->getRequest()->getHeader(self::HEADER_KEY_NAME) == 'demo')
            return $this;

        if (!($controller->getRequest()->getHeader(self::HEADER_KEY_NAME)) ||
            $controller->getRequest()->getHeader(self::HEADER_KEY_NAME) !== $this->_configuration->getConfig(self::PATH_KEY)->getValue()
        ) {
            $controller->setStatusCode(403);
            throw new Exception('Forbidden');
        }

        return $this;
    }

    public function getBlackHole(SM_XRetail_V1Controller $controller) {
        if (!$controller->getRequest()->getHeader(self::HEADER_AUTHENTICATION_CODE)) {
            throw new Exception('Require xxx data');
        }

        //
        if (
        $this->callLicenseApi($controller->getRequest()->getHeader(self::HEADER_AUTHENTICATION_CODE))
        ) {
            $w = md5(microtime());
            $this->_configuration->setConfig(self::PATH_KEY, $w);

            return array(
                'Black-Hole' => $w,
            );
        }
        else {
            $controller->setStatusCode(403);
            throw new Exception('Forbidden');
        }

    }

    private function callLicenseApi($licenseId) {
        return true;
    }
}
