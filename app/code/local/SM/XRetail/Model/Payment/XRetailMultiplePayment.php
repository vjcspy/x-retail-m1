<?php

class SM_XRetail_Model_Payment_XRetailMultiplePayment extends Mage_Payment_Model_Method_Abstract {
    protected $_code = self::CODE;
    const CODE = 'xretail_multiple_payment';
    const PURCHASE_TYPE = 'partial';
    protected $_canUseInternal = true;
    protected $_canUseCheckout = false;
    protected $_infoBlockType = 'xretail/multiplePayment_info_xpaymentMultiple';
    protected $_formBlockType = 'xretail/multiplePayment_form_xpaymentMultiple';

    //protected $_canUseForMultishipping = false;
    //protected $_isGateway = true;
    //protected $_canAuthorize = true;

    protected function _construct() {
        parent::_construct();
    }

    public function authorize(Varien_Object $payment, $amount) {
        Mage::log("Dummypayment\tIn authorize");

        return $this;
    }

    public function assignData($data) {
        parent::assignData($data);
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $data = $data->getData();

        $dataSplit = array();

        foreach ($data as $k => $v) {
            if ($v != 0 && $k != 'enable') {
                $dataSplit[$k] = $v;
            }
        }
        $this->getInfoInstance()->setAdditionalData(serialize($dataSplit));

        return $this;
    }


    public function validate() {
        return parent::validate();
    }

    public function isApplicableToQuote($quote, $checksBitMask) {
        $isApply = parent::isApplicableToQuote($quote, $checksBitMask);
        if (!$isApply)
            return $isApply;
        else
            return $this->fromApi() ? true : false;
    }

    protected function fromApi() {
        return Mage::registry('from_api') == true ? true : false;
    }
}
