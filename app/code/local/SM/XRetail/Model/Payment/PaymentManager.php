<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 7/24/15
 * Time: 10:49 AM
 */
class SM_XRetail_Model_Payment_PaymentManager extends SM_XRetail_Model_Api_ServiceAbstract {

    /**
     * @var Mage_Core_Model_Config_Data
     */
    protected $_configDataModel;

    /**
     * @var SM_XRetail_Model_ResourceModel_Config
     */
    protected $_apiConfig;

    const STRING_PATH_PREFIX = 'payment/xpayment_dummy';

    protected $_start = 0;

    public function __construct() {
        $this->_configDataModel = Mage::getModel('core/config_data');
        $this->_apiConfig = Mage::getModel('xretail/api_configuration');
        parent::__construct();
    }

    public function index() {
        $dataPayment = array();
        for ($i = 1; $i <= 20; $i++) {
            $code = 'xpayment_dummy' . $i;
            $dataPayment[$code] = $this->_apiConfig->getStoreConfig('payment/xpayment_dummy' . $i);;
            $dataPayment[$code]['code'] = $code;
        }
        return $dataPayment;
    }

    public function update() {
        //for test
//        $this->dummyData();
        $this->setDataToPaymentConfig($this->getRequest()->getParam('payment_data'));
        return ($this->getRequest()->getParam('payment_data'));
    }

    protected function dummyData() {
        $dummyData = array(
            'payment_data' => array(
                array(
                    'active' => 1,
                    'title' => 'test 1',
                    'allowspecific' => 0,
                    'order_status' => 'pending',
                    'specificcountry' => null,
                    "code" => "xpayment_dummy1",

                ),
                array(
                    'active' => 1,
                    'title' => 'test 2',
                    'allowspecific' => 0,
                    'order_status' => 'pending',
                    'specificcountry' => null,
                    "code" => "xpayment_dummy2"
                )
            ));
        $this->getRequest()->setParams($dummyData);
    }

    protected function setDataToPaymentConfig($paymentData) {
        $this->deActiveAll();
        if (is_array($paymentData))
            foreach ($paymentData as $data) {
                preg_match_all('!\d+!', $data['code'], $p);
                $path = self::STRING_PATH_PREFIX . $p[0][0] . '/';
                if (isset($data['active'])) {
                    $this->setConfig($path . 'active', $data['active']);
                }
                if (isset($data['title'])) {
                    $this->setConfig($path . 'title', $data['title']);
                }
                if (isset($data['order_status'])) {
                    $this->setConfig($path . 'order_status', $data['order_status']);
                }
                if (isset($data['allowspecific'])) {
                    $this->setConfig($path . 'allowspecific', $data['allowspecific']);
                }
                if (isset($data['sort_order'])) {
                    $this->setConfig($path . 'sort_order', $data['sort_order']);
                }
                if (isset($data['specificcountry'])) {
                    $this->setConfig($path . 'specificcountry', $data['specificcountry']);
                }
            }
    }

    /**
     * DeActive all dummy payment
     * @return bool
     */
    protected function deActiveAll() {
        for ($i = 1; $i <= 20; $i++) {
            $path = self::STRING_PATH_PREFIX . $i . '/';
            $this->setConfig($path . 'active', 0);
        }
        return true;
    }

    protected function getConfig($path) {
        return Mage::getModel('core/config_data')->load($path, 'path');
    }

    public function getPaymentAllowSplit() {
        $paymentArray = array('xpayment_cashpayment', 'xpayment_ccpayment', 'xpayment_cc1payment', 'xpayment_cc2payment', 'xpayment_cc3payment', 'xpayment_cc4payment');
        $paymentAllowArray = array();
        foreach ($paymentArray as $paymentCode) {
            if (Mage::getStoreConfig('xpayment/' . $paymentCode . '/active')) {
                $paymentAllowArray[] = $paymentCode;
            }
        }
        return $paymentAllowArray;
    }

    protected function setConfig($path, $string) {
        try {
            Mage::getModel('core/config_data')
                ->load($path, 'path')
                ->setValue($string)
                ->setPath($path)
                ->save();
        } catch (Exception $e) {
            throw new Exception(Mage::helper('cron')->__('Unable to save the config data with path ' . $path));
        }
    }
}
