<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/10/2016
 * Time: 11:25
 */
class SM_XRetail_Model_SalesRule_Quote_WholeOrderDiscount_Observer {

    /**
     * @var \Mage_Core_Helper_Abstract|\SM_XRetail_Helper_Data
     */
    private $xretailHelper;

    /**
     * SM_XRetail_Model_SalesRule_Quote_WholeOrderDiscount_Observer constructor.
     */
    public function __construct() {
        $this->xretailHelper = Mage::helper('xretail');
    }

    /**
     * @param \Varien_Event_Observer $observer
     *
     * @throws \Exception
     */
    public function filterRules(Varien_Event_Observer $observer) {
        $collection = $observer->getData('collection');
        if ($collection instanceof Mage_SalesRule_Model_Resource_Rule_Collection) {
            $discountWholeOrder = $this->xretailHelper->registry(SM_XRetail_Model_ResourceModel_XRetailOrderOnline::DISCOUNT_WHOLE_ORDER);
            if (!!$discountWholeOrder) {
                if (!isset($discountWholeOrder['isPercentMode']))
                    throw new Exception("Can't get type discount whole order");

                if ($discountWholeOrder['isPercentMode'] == true)
                    $rule = $this->getRule()->addData($this->getRulePercentData($discountWholeOrder));
                else
                    $rule = $this->getRule()->addData($this->getRuleFixAmountData($discountWholeOrder));

                $collection->clear();
                $collection->addItem($rule);
            }
        }
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function getRulePercentData($data) {
        return Mage::getModel('xretail/salesRule_quote_wholeOrderDiscount_rule_percentOfProductPriceDiscount')->getRule($data);
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function getRuleFixAmountData($data) {
        return Mage::getModel('xretail/salesRule_quote_wholeOrderDiscount_rule_fixAmountDiscount')->getRule($data);
    }

    /**
     * @return false|\Mage_Core_Model_Abstract|\Mage_SalesRule_Model_Rule
     */
    private function getRule() {
        return Mage::getModel('salesrule/rule');
    }
}