<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 31/05/2016
 * Time: 16:18
 */
class SM_XRetail_Model_SalesRule_Quote_Discount extends Mage_SalesRule_Model_Quote_Discount {

    public function collect(Mage_Sales_Model_Quote_Address $address) {
        if (!SM_XRetail_Model_ResourceModel_XRetailOrderOnline::$IS_COLLECT_RULE)
            return $this;
        else
            return parent::collect($address);
    }
}