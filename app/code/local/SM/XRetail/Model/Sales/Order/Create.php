<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/7/16
 * Time: 3:26 PM
 */
class SM_XRetail_Model_Sales_Order_Create extends Mage_Adminhtml_Model_Sales_Order_Create {

    public function __construct() {

        parent::__construct();
        $this->_session = Mage::getSingleton('xretail/session_quote');
    }

    /**
     * Add product to current order quote
     * $product can be either product id or product model
     * $config can be either buyRequest config, or just qty
     *
     * @param   int|Mage_Catalog_Model_Product $product
     * @param array|float|int|Varien_Object    $config
     *
     * @return Mage_Adminhtml_Model_Sales_Order_Create
     * @throws Mage_Core_Exception
     */
    public function addProduct($product, $config = 1) {

        SM_XRetail_Helper_CustomSales_Data::setFlagCollect();
        //        parent::addProduct($product, $config);
        if (!is_array($config) && !($config instanceof Varien_Object)) {
            $config = array('qty' => $config);
        }
        $config = new Varien_Object($config);

        if (!($product instanceof Mage_Catalog_Model_Product)) {
            $productId = $product;
            $product   = Mage::getModel('catalog/product')
                             ->setStore($this->getSession()->getStore())
                             ->setStoreId($this->getSession()->getStoreId())
                             ->load($product);
            if (!$product->getId()) {
                Mage::throwException(
                    Mage::helper('adminhtml')->__('Failed to add a product to cart by id "%s".', $productId)
                );
            }
        }

        // Split product
        if (isset($config['split_product'])) {
            $customOptions = $product->getCustomOption('additional_options');
            if (is_null($customOptions))
                $customOptions = array();
            $customOptions['split_product'] = $config->getData('split_product');
            $product->addCustomOption('additional_options', serialize($customOptions));
        }

        $stockItem = $product->getStockItem();
        if ($stockItem && $stockItem->getIsQtyDecimal()) {
            $product->setIsQtyDecimal(1);
        }
        else {
            $config->setQty((int)$config->getQty());
        }
        $product->setCartQty($config->getQty());

        $item = $this->getQuote()->addProductAdvanced(
            $product,
            $config,
            Mage_Catalog_Model_Product_Type_Abstract::PROCESS_MODE_FULL
        );

        if (is_string($item)) {
            if ($product->getTypeId() != Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE) {
                $item = $this->getQuote()->addProductAdvanced(
                    $product,
                    $config,
                    Mage_Catalog_Model_Product_Type_Abstract::PROCESS_MODE_LITE
                );
            }
            if (is_string($item)) {
                Mage::throwException($item);
            }
        }

        $itemPrice = array();
        $itemPrice['custom_price'] = -1;
        if (isset($config['custom_price'])) {
            if(Mage::getStoreConfig('tax/calculation/price_includes_tax')
                && !Mage::getStoreConfig('tax/calculation/apply_tax_on')
                &&  !Mage::getStoreConfig('tax/calculation/cross_border_trade_enabled')
            ){
                $itemPrice['custom_price'] = $this->_parseCustomPrice($config['real_custom_price']);
            }else{
                $itemPrice['custom_price'] = $this->_parseCustomPrice($config['custom_price']);
            }
        }
        //XPOS-1447: If grouped product then do not process further since their children already be added in addProductAdvanced()
        if (isset($config['super_group'])) {
            return $this;
        }
//        $itemQty = (float)$config['qty'];
        /*old code using $itemQty => caused problem that subtracting quantity of stock item wrong*/
        if (empty($config['action']) || !empty($config['configured'])) {
            if ($config['has_custom_price']) {
                if($item->getParentItem()){
                    $item->getParentItem()->setCustomPrice($itemPrice['custom_price']);
                    $item->getParentItem()->setOriginalCustomPrice($itemPrice['custom_price']);
                } else {
                    $item->setCustomPrice($itemPrice['custom_price']);
                    $item->setOriginalCustomPrice($itemPrice['custom_price']);
                }
                $item->getProduct()->setIsSuperMode(true);
                $item->getProduct()->unsSkipCheckRequiredOption();
            }
            //$item->checkData();
        }
//        var_dump($item->getData());
        $item->checkData();

        $this->setRecollect(true);

        SM_XRetail_Helper_CustomSales_Data::setFlagCollect(false);

        return $this;
    }

    public function setSession(Mage_Adminhtml_Model_Session_Quote $session_Quote) {

        $this->_session = $session_Quote;

        return $this;
    }

    /**
     * Add multiple products to current order quote
     *
     * @param   array $products
     *
     * @return Exception|Mage_Adminhtml_Model_Sales_Order_Create
     * @throws Exception
     */
    public function addProducts(array $products) {

        foreach ($products as $productId => $config){
            $config['qty'] = isset($config['qty']) ? (float)$config['qty'] : 1;
            if (isset($config['product_id'])) {
                $productId = (int)$config['product_id'];
            }

            if (!$productId) {
                throw new Exception('Not found product Id');
            }
            $this->addProduct($productId, $config);
        }
        return $this;
    }

    protected function _validate() {

        $customerId = $this->getSession()->getCustomerId();
        if (is_null($customerId)) {
            Mage::throwException(Mage::helper('adminhtml')->__('Please select a customer.'));
        }

        if (!$this->getSession()->getStore()->getId()) {
            Mage::throwException(Mage::helper('adminhtml')->__('Please select a store.'));
        }
        $items = $this->getQuote()->getAllItems();

        if (count($items) == 0) {
            $this->_errors[] = Mage::helper('adminhtml')->__('You need to specify order items.');
        }

        foreach ($items as $item) {
            $messages = $item->getMessage(false);
            if ($item->getHasError() && is_array($messages) && !empty($messages)) {
                $this->_errors = array_merge($this->_errors, $messages);
            }
        }

        if (!$this->getQuote()->isVirtual()) {
            if (!$this->getQuote()->getShippingAddress()->getShippingMethod()) {
                $this->_errors[] = Mage::helper('adminhtml')->__('Shipping method must be specified.');
            }
        }

        if (!$this->getQuote()->getPayment()->getMethod()) {
            $this->_errors[] = Mage::helper('adminhtml')->__('Payment method must be specified.');
        }
        else {
            $method = $this->getQuote()->getPayment()->getMethodInstance();
            if (!$method) {
                $this->_errors[] = Mage::helper('adminhtml')->__('Payment method instance is not available.');
            }
            else {
                if (!$method->isAvailable($this->getQuote())) {
                    $this->_errors[] = Mage::helper('adminhtml')->__('Payment method is not available.');
                }
                else {
                    try {
                        $method->validate();
                    } catch (Mage_Core_Exception $e) {
                        $this->_errors[] = $e->getMessage();
                    }
                }
            }
        }

        if (!empty($this->_errors))
            Mage::throwException(json_encode($this->_errors));

        return $this;
    }

    /**
     * Initialize data for price rules
     *
     * @return Mage_Adminhtml_Model_Sales_Order_Create
     */
    public function initRuleData() {
        if (is_null(Mage::registry('rule_data')))
            Mage::register(
                'rule_data',
                new Varien_Object(
                    array(
                        'store_id'          => Mage::getSingleton('xretail/session_quote')->getStore()->getId(),
                        'website_id'        => Mage::getSingleton('xretail/session_quote')->getStore()->getWebsiteId(),
                        'customer_group_id' => $this->getCustomerGroupId(),
                    )
                )
            );

        return $this;
    }

    public function initReOderForExchangeAndRefund(Mage_Sales_Model_Order $order) {
        if (!$order->getReordered()) {
            $this->getSession()->setOrderId($order->getId());
        }
        else {
            $this->getSession()->setReordered($order->getId());
        }

        /**
         * Check if we edit quest order
         */
        $this->getSession()->setCurrencyId($order->getOrderCurrencyCode());
        if ($order->getCustomerId()) {
            $this->getSession()->setCustomerId($order->getCustomerId());
        }
        else {
            $this->getSession()->setCustomerId(false);
        }

        $this->getSession()->setStoreId($order->getStoreId());

        //Notify other modules about the session quote
        Mage::dispatchEvent(
            'init_from_order_session_quote_initialized',
            array('session_quote' => $this->getSession()));

        /**
         * Initialize catalog rule data with new session values
         */
        $this->initRuleData();
        foreach ($order->getItemsCollection(
            array_keys(Mage::getConfig()->getNode('adminhtml/sales/order/create/available_product_types')->asArray()),
            true
        ) as $orderItem) {
            /* @var $orderItem Mage_Sales_Model_Order_Item */
            if (!$orderItem->getParentItem()) {
                if ($order->getReordered()) {
                    $qty = $orderItem->getQtyOrdered();
                }
                else {
                    $qty = $orderItem->getQtyOrdered() - $orderItem->getQtyShipped() - $orderItem->getQtyInvoiced();
                }

                if ($qty > 0) {
                    $item = $this->initFromOrderItem($orderItem, $qty);
                    if (is_string($item)) {
                        Mage::throwException($item);
                    }
                }
            }
        }

        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress) {
            $addressDiff = array_diff_assoc($shippingAddress->getData(), $order->getBillingAddress()->getData());
            unset($addressDiff['address_type'], $addressDiff['entity_id']);
            $shippingAddress->setSameAsBilling(empty($addressDiff));
        }

        $this->_initBillingAddressFromOrder($order);
        $this->_initShippingAddressFromOrder($order);

        if (!$this->getQuote()->isVirtual() && $this->getShippingAddress()->getSameAsBilling()) {
            $this->setShippingAsBilling(1);
        }

        $this->setShippingMethod($order->getShippingMethod());
        $this->getQuote()->getShippingAddress()->setShippingDescription($order->getShippingDescription());

        $this->getQuote()->getPayment()->addData($order->getPayment()->getData());


        $orderCouponCode = $order->getCouponCode();
        if ($orderCouponCode) {
            $this->getQuote()->setCouponCode($orderCouponCode);
        }

        if ($this->getQuote()->getCouponCode()) {
            $this->getQuote()->collectTotals();
        }

        Mage::helper('core')->copyFieldset(
            'sales_copy_order',
            'to_edit',
            $order,
            $this->getQuote()
        );

        Mage::dispatchEvent(
            'sales_convert_order_to_quote',
            array(
                'order' => $order,
                'quote' => $this->getQuote()
            ));

        if (!$order->getCustomerId()) {
            $this->getQuote()->setCustomerIsGuest(true);
        }

        if (false) {
            if ($this->getSession()->getUseOldShippingMethod(true)) {
                /*
                 * if we are making reorder or editing old order
                 * we need to show old shipping as preselected
                 * so for this we need to collect shipping rates
                 */
                $this->collectShippingRates();
            }
            else {
                /*
                 * if we are creating new order then we don't need to collect
                 * shipping rates before customer hit appropriate button
                 */
                $this->collectRates();
            }

            // Make collect rates when user click "Get shipping methods and rates" in order creating
            // $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
            // $this->getQuote()->getShippingAddress()->collectShippingRates();

            $this->getQuote()->save();
        }

        return $this;
    }

    public function initFromOrderItem(Mage_Sales_Model_Order_Item $orderItem, $qty = null) {
        if (!$orderItem->getId()) {
            return $this;
        }

        $product = Mage::getModel('catalog/product')
                       ->setStoreId($this->getSession()->getStoreId())
                       ->load($orderItem->getProductId());

        if ($product->getId()) {
            $product->setSkipCheckRequiredOption(true);
            $buyRequest = $orderItem->getBuyRequest();
            if (is_numeric($qty)) {
                $buyRequest->setQty($qty);
            }
            $item = $this->getQuote()->addProduct($product, $buyRequest);
            if (is_string($item)) {
                return $item;
            }

            if ($additionalOptions = $orderItem->getProductOptionByCode('additional_options')) {
                $item->addOption(
                    new Varien_Object(
                        array(
                            'product' => $item->getProduct(),
                            'code'    => 'additional_options',
                            'value'   => serialize($additionalOptions)
                        )
                    ));
            }

            Mage::dispatchEvent(
                'sales_convert_order_item_to_quote_item',
                array(
                    'order_item' => $orderItem,
                    'quote_item' => $item
                ));

            return $item;
        }

        return $this;
    }
}
