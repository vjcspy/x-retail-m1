<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/7/16
 * Time: 3:06 PM
 */
class SM_XRetail_Model_Sales_Custom_Observer extends Varien_Object {

    protected $_arrayOptions = array();
    protected $_currentOption = 0;


    public function catalogProductLoadAfter(Varien_Event_Observer $observer) {
        //SM_XRetail_Helper_CustomSales_Data
        if (!SM_XRetail_Helper_CustomSales_Data::getFlagCollect())
            return $this;
        $action = Mage::app()->getFrontController()->getAction();
        if (true ||
            in_array(
                $action->getFullActionName(),
                array('adminhtml_xpos_index', 'adminhtml_xpos_loadBlock', 'adminhtml_xpos_complete1', 'adminhtml_xpos_completeoffline'))
        ) {

            $product   = $observer->getProduct();
            $productId = $product->getId();

            if ($productId != Mage::helper('xretail/customSales_data')->getCustomSalesId())
                return $this;
            $items = $action->getRequest()->getParam('items');
            if (!is_array($items)) {
                return $this;
            }
            foreach ($items as $id => $dataOption) {
                if (strpos($dataOption['product_id'], Mage::helper('xretail/customSales_data')->getCustomSalesId()) !== FALSE) {
                    $this->_arrayOptions[] = array(
                        'Name' => $dataOption['name'],
                        //                        'custom_sales_price' => $dataOption['price'],
                    );
                }
            }
            $options = $this->_arrayOptions[SM_XRetail_Helper_CustomSales_Data::$COUNT_CURRENT_CUSTOM_SALES];

            // add to the additional options array
            $additionalOptions = array();
            if ($additionalOption = $product->getCustomOption('additional_options')) {
                $additionalOptions = (array)unserialize($additionalOption->getValue());
            }
            foreach ($options as $key => $value) {
                $additionalOptions[] = array(
                    'label' => $key,
                    'value' => $value,
                );
            }

            $observer->getProduct()
                     ->addCustomOption('additional_options', serialize($additionalOptions));

            SM_XRetail_Helper_CustomSales_Data::setFlagCollect(false);
            SM_XRetail_Helper_CustomSales_Data::$COUNT_CURRENT_CUSTOM_SALES++;

            return $this;
        }

        return $this;
    }

    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer) {
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $orderItem                     = $observer->getOrderItem();
            $options                       = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
    }
}
