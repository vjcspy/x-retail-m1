<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/10/2016
 * Time: 11:02
 */
class SM_XRetail_Model_Sales_Quote_Address_Total_Collector extends Mage_Sales_Model_Quote_Address_Total_Collector {

    private $_foamy = [];

    protected function foamy($configArray) {
        if (count($configArray) == 0)
            return;

        reset($configArray);
        $config = current($configArray);

        foreach ($configArray as $configItem) {
            if ($this->_compareTotals($configItem, $config) == -1)
                $config = $configItem;
        }
        array_push($this->_foamy, $config);

        $this->foamy($this->removeFoam($configArray, $config));

        return $this->_foamy;
    }

    private function removeFoam($configArray, $config) {
        return $configArray = array_filter(
            $configArray,
            function ($v, $k) use ($config) {
                return $v['_code'] != $config['_code'];
            },
            ARRAY_FILTER_USE_BOTH);
    }

    protected function _getSortedCollectorCodes() {
        if (Mage::app()->useCache('config')) {
            $cachedData = Mage::app()->loadCache($this->_collectorsCacheKey);
            if ($cachedData) {
                return unserialize($cachedData);
            }
        }
        $configArray = $this->_modelsConfig;
        // invoke simple sorting if the first element contains the "sort_order" key
        reset($configArray);
        $element = current($configArray);
        if (isset($element['sort_order'])) {
            uasort($configArray, [$this, '_compareSortOrder']);
        }
        else {
            foreach ($configArray as $code => $data) {
                foreach ($data['before'] as $beforeCode) {
                    if (!isset($configArray[$beforeCode])) {
                        continue;
                    }
                    $configArray[$code]['before']      = array_unique(
                        array_merge(
                            $configArray[$code]['before'],
                            $configArray[$beforeCode]['before']
                        ));
                    $configArray[$beforeCode]['after'] = array_merge(
                        $configArray[$beforeCode]['after'],
                        [$code],
                        $data['after']
                    );
                    $configArray[$beforeCode]['after'] = array_unique($configArray[$beforeCode]['after']);
                }
                foreach ($data['after'] as $afterCode) {
                    if (!isset($configArray[$afterCode])) {
                        continue;
                    }
                    $configArray[$code]['after']       = array_unique(
                        array_merge(
                            $configArray[$code]['after'],
                            $configArray[$afterCode]['after']
                        ));
                    $configArray[$afterCode]['before'] = array_merge(
                        $configArray[$afterCode]['before'],
                        [$code],
                        $data['before']
                    );
                    $configArray[$afterCode]['before'] = array_unique($configArray[$afterCode]['before']);
                }
            }
            $foamy       = $this->foamy($configArray);
            $keys        = array_map(
                function ($a) {
                    return $a['_code'];
                },
                $foamy);
            $configArray = array_combine($keys, $foamy);
        }
        $sortedCollectors = array_keys($configArray);
        if (Mage::app()->useCache('config')) {
            Mage::app()->saveCache(
                serialize($sortedCollectors),
                $this->_collectorsCacheKey,
                [
                    Mage_Core_Model_Config::CACHE_TAG
                ]
            );
        }

        return $sortedCollectors;
    }
}