<?php

/**
 * Created by khoild@smartosc.com/mr.vjcspy@gmail.com
 * User: vjcspy
 * Date: 26/04/2016
 * Time: 17:49
 */
class SM_XRetail_Model_Resource_Category_Indexer_Product extends Mage_Catalog_Model_Resource_Category_Indexer_Product {

    private $storesInfo;

    public function getStoreInfo() {
        if (is_null($this->storesInfo)) {
            $adapter = $this->_getReadAdapter();
            $select  = $adapter->select()
                               ->from(array('s' => $this->getTable('catalog/category_product_index')), array('s.category_id'))
                               ->columns("GROUP_CONCAT(DISTINCT s.store_id SEPARATOR ',') AS store_ids")
                               ->group('s.category_id');
            // die($select->__toString());
            $this->storesInfo = $adapter->fetchAll($select);
        }

        return $this->storesInfo;
    }
}