<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 01/04/2016
 * Time: 12:16
 */
class SM_XRetail_Model_NestedModel_Data {

    /**
     * @var array
     */
    private $_dataNested = array();

    /**
     * @var int for index left/right
     */
    private $_n = 1;

    /**
     * @var \Mage_Catalog_Model_Category
     */
    private $_catalogCategoryModel;
    /**
     * @var Mage_Catalog_Model_Resource_Category_Collection
     */
    private $_collection;
    /**
     * @var array data all category magento
     */
    private $dataCategory;

    /**
     * @var
     */
    private $datacategoryGrouped;

    /**
     * @var array Index category and store id
     */
    private $_categoryIndex;
    /**
     * @var \Mage_Catalog_Model_Category_Indexer_Product
     */
    private $_catalogCategoryProductIndexModel;

    /**
     * SM_XRetail_Model_NestedModel_Data constructor.
     */
    public function __construct() {

        $this->_catalogCategoryModel             = Mage::getModel('catalog/category');
        $this->_collection                       = $this->_catalogCategoryModel->getCollection();
        $this->_catalogCategoryProductIndexModel = Mage::getModel('xretail/resource_category_indexer_product');
    }

    /**
     * @return array
     */
    protected function getDataCategory() {
        if (is_null($this->dataCategory)) {
            $this->dataCategory = array();
            $this->_collection->addAttributeToSelect("*");
            foreach ($this->_collection as $item) {
                $this->dataCategory[] = Mage::getModel('catalog/category')->setStoreId(0)->load($item->getId());
            }
        }

        return $this->dataCategory;
    }

    protected function getCategoryEntityWithStoreData(Mage_Catalog_Model_Category $category) {
        $data              = $category->getData();
        $data['store_id']  = $this->getStoreIdByCategoryId($category['entity_id']);
        $data['is_active'] = $category->getIsActive();

        return $data;
    }

    private $dataCateStoreId = array();

    private function getStoreIdByCategoryId($categoryId) {
        if (!isset($this->dataCateStoreId[$categoryId])) {
            $categoryIndex = $this->getDataCategoryIndex();
            foreach ($categoryIndex as $cata) {
                if ($cata['category_id'] == $categoryId)
                    return $this->dataCateStoreId[$categoryId] = explode(",", $cata['store_ids']);
            }

            return $this->dataCateStoreId[$categoryId] = array();
        }
        else return $this->dataCateStoreId[$categoryId];
    }

    private function getDataCategoryIndex() {
        if (is_null($this->_categoryIndex)) {
            $this->_categoryIndex = $this->_catalogCategoryProductIndexModel->getStoreInfo();
        }

        return $this->_categoryIndex;
    }

    /**
     * @return array
     */
    protected function getGroupedCategory() {

        if (is_null($this->datacategoryGrouped)) {
            $this->datacategoryGrouped = array();
            $catas                     = $this->getDataCategory();
            foreach ($catas as $cata) {
                if (!isset($this->datacategoryGrouped[$cata->getData('level')]))
                    $this->datacategoryGrouped[$cata->getData('level')] = array();
                $this->datacategoryGrouped[$cata->getData('level')][] = $cata;
            }
        }

        return $this->datacategoryGrouped;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function convertData() {

        $this->_dataNested = array();
        $isNextLevel       = false;

        $dataCategoryGrouped = $this->getGroupedCategory();
        foreach ($dataCategoryGrouped as $group => $cata) {
            if ($group == 0)
                $this->_dataNested[] = array(
                    'nested_data' => array(
                        'id'          => $cata[0]->getData('entity_id'),
                        'level_depth' => $group,
                        'nleft'       => $this->_n,
                        'nright'      => $cata[0]->getData('children_count') * 2
                    ),
                    'data'        => $this->getCategoryEntityWithStoreData($cata[0])
                );
            else {
                usort(
                    $cata,
                    function ($a, $b) {

                        if ($a['position'] == $b['position'])
                            return 0;

                        return $a['position'] < $b['position'] ? -1 : 1;
                    });
                foreach ($cata as $item) {
                    if ($isNextLevel) {
                        $this->_dataNested[] = array(
                            'nested_data' => array(
                                'id'          => $item->getData('entity_id'),
                                'level_depth' => $group,
                                'nleft'       => $this->getParentLeft($item) + 1,
                                'nright'      => $this->getParentLeft($item) + 1 + $item->getData('children_count') * 2 + 1
                            ),
                            'data'        => $this->getCategoryEntityWithStoreData($item));
                        $isNextLevel         = !$isNextLevel;
                    }
                    else {
                        $end = end($this->_dataNested);

                        $this->_dataNested[] = array(
                            'nested_data' => array(
                                'id'          => $item->getData('entity_id'),
                                'level_depth' => $group,
                                'nleft'       => $end['nested_data']['nleft'] + 1,
                                'nright'      => $end['nested_data']['nleft'] + 1 + $item->getData('children_count') * 2 + 1
                            ),
                            'data'        => $this->getCategoryEntityWithStoreData($item));
                    }
                }
            }
            $isNextLevel = !$isNextLevel;
        }

        return $this->_dataNested;
    }

    /**
     * @param $cata
     *
     * @return mixed
     * @throws \Exception
     */
    private function getParentLeft($cata) {

        foreach ($this->_dataNested as $item) {
            if ($item['nested_data']['id'] == $cata->getData('parent_id'))
                return $item['nested_data']['nleft'];
        }
        throw new Exception("Can't find parent");
    }

}