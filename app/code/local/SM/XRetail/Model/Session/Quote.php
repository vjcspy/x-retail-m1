<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/29/16
 * Time: 3:59 PM
 */
class SM_XRetail_Model_Session_Quote extends Mage_Adminhtml_Model_Session_Quote {
    /**
     * @var int
     */
    private $_customerId;

    /**
     * @return int
     */
    public function getCustomerId() {
        return $this->_customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId($customerId) {
        $this->_customerId = $customerId;
    }

    /**
     * @return $this
     */
    public function reloadQuote() {
        $this->_quote = null;
        return $this;
    }


}
