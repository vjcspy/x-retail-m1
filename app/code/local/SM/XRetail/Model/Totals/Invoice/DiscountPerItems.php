<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 5:19 PM
 */
class SM_XRetail_Model_Totals_Invoice_DiscountPerItems extends Mage_Sales_Model_Order_Invoice_Total_Abstract {
    public function collect(Mage_Sales_Model_Order_Invoice $invoice) {
        $order = $invoice->getOrder();
        $invoiceCollection = $order->getInvoiceCollection();

        if ($order->getRetailDiscountPerItemsDiscount() < 0.0001) {
            return;
        }

        $totalDiscountAmount = 0;
        $baseTotalDiscountAmount = 0;
        $totalDiscountInvoiced = 0;
        $baseTotalDiscountInvoiced = 0;

        foreach ($invoiceCollection as $previusInvoice) {
            if ($previusInvoice->getRetailDiscountPerItemsDiscount()) {
                $totalDiscountInvoiced += $previusInvoice->getRetailDiscountPerItemsDiscount();
                $baseTotalDiscountInvoiced += $previusInvoice->getRetailDiscountPerItemsBaseDiscount();
            }
        }
        if ($this->isLast($invoice)) {
            $totalDiscountAmount = $order->getRetailDiscountPerItemsDiscount() - $totalDiscountInvoiced;
            $baseTotalDiscountAmount = $order->getRetailDiscountPerItemsBaseDiscount() - $baseTotalDiscountInvoiced;
        }
        else {
            /** @var $item Mage_Sales_Model_Order_Invoice_Item */
            foreach ($invoice->getAllItems() as $item) {
                $orderItem = $item->getOrderItem();
                if ($orderItem->isDummy()) {
                    continue;
                }
                $orderItemDiscount = (float)$orderItem->getRetailDiscountPerItemsDiscount();
                $baseOrderItemDiscount = (float)$orderItem->setRetailDiscountPerItemsBaseDiscount();
                $orderItemQty = $orderItem->getQtyOrdered();
                if ($orderItemDiscount && $orderItemQty) {
                    $totalDiscountAmount += $invoice->roundPrice($orderItemDiscount / $orderItemQty * $item->getQty(), 'regular', true);
                    $baseTotalDiscountAmount += $invoice->roundPrice($baseOrderItemDiscount / $orderItemQty * $item->getQty(), 'base', true);
                }
            }
        }

        $invoice->setRetailDiscountPerItemsDiscount($totalDiscountAmount);
        $invoice->setRetailDiscountPerItemsBaseDiscount($baseTotalDiscountAmount);

        $invoice->setGrandTotal($invoice->getGrandTotal() - $totalDiscountAmount);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $baseTotalDiscountAmount);
        return $this;
    }

    public function isLast($invoice) {
        foreach ($invoice->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if ($orderItem->isDummy()) {
                continue;
            }
            if (!$item->isLast()) {
                return false;
            }
        }
        return true;
    }
}