<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 5:48 PM
 */
class SM_XRetail_Model_Totals_Creditmemo_DiscountPerItems extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract {
    /**
     * Collect total when create Creditmemo
     *
     * @param \Mage_Sales_Model_Order_Creditmemo $creditmemo
     *
     * @return $this|\Mage_Sales_Model_Order_Creditmemo_Total_Abstract
     */
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        $creditmemo->setRetailDiscountPerItemsDiscount(0);
        $creditmemo->setRetailDiscountPerItemsBaseDiscount(0);

        $order = $creditmemo->getOrder();

        if ($order->getRetailDiscountPerItemsDiscount() < 0.0001) {
            return $this;
        }

        $totalDiscountAmount = 0;
        $baseTotalDiscountAmount = 0;
        $baseTotalDiscountRefunded = 0;
        $totalDiscountRefunded = 0;
        foreach ($order->getCreditmemosCollection() as $existedCreditmemo) {
            if ($existedCreditmemo->getRetailDiscountPerItemsDiscount()) {
                $totalDiscountRefunded += $existedCreditmemo->getRetailDiscountPerItemsDiscount();
                $baseTotalDiscountRefunded += $existedCreditmemo->getRetailDiscountPerItemsBaseDiscount();
            }
        }


        if ($this->isLast($creditmemo)) {
            $baseTotalDiscountAmount = $order->getRetailDiscountPerItemsBaseDiscount() - $baseTotalDiscountRefunded;
            $totalDiscountAmount = $order->getRetailDiscountPerItemsDiscount() - $totalDiscountRefunded;
        }
        else {
            /** @var $item Mage_Sales_Model_Order_Invoice_Item */
            foreach ($creditmemo->getAllItems() as $item) {
                $orderItem = $item->getOrderItem();
                if ($orderItem->isDummy()) {
                    continue;
                }
                $orderItemDiscount = (float)$orderItem->getRetailDiscountPerItemsDiscount() * $orderItem->getQtyInvoiced() / $orderItem->getQtyOrdered();
                $baseOrderItemDiscount = (float)$orderItem->getRetailDiscountPerItemsBaseDiscount() * $orderItem->getQtyInvoiced() / $orderItem->getQtyOrdered();

                $orderItemQty = $orderItem->getQtyInvoiced();

                if ($orderItemDiscount && $orderItemQty) {
                    $totalDiscountAmount += $creditmemo->roundPrice($orderItemDiscount / $orderItemQty * $item->getQty(), 'regular', true);
                    $baseTotalDiscountAmount += $creditmemo->roundPrice($baseOrderItemDiscount / $orderItemQty * $item->getQty(), 'base', true);
                }
            }
        }

        $creditmemo->setRetailDiscountPerItemsDiscount($totalDiscountAmount);
        $creditmemo->setRetailDiscountPerItemsBaseDiscount($baseTotalDiscountAmount);

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() - $totalDiscountAmount);// + $totalHiddenTax);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() - $baseTotalDiscountAmount);// + $baseTotalHiddenTax);
        return $this;
    }

    /**
     * check credit memo is last or not
     *
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     *
     * @return boolean
     */
    public function isLast($creditmemo) {
        foreach ($creditmemo->getAllItems() as $item) {
            if (!$item->isLast()) {
                return false;
            }
        }
        return true;
    }
}
