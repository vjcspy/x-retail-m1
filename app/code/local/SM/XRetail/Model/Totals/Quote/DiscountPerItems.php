<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 11:08 AM
 */
class SM_XRetail_Model_Totals_Quote_DiscountPerItems extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    const HIDDEN_TAX = 'hidden_tax';

    /**
     * @var \SM_XRetail_Helper_DataConfig
     */
    protected $dataConfig;

    /**
     * @var \SM_XRetail_Helper_DiscountPerItems_Data
     */
    private $_discountPerItemsHelper;

    /**
     * @var \SM_XRetail_Helper_DiscountPerItems_Calculate_Data
     */
    private $_discountPerItemsCalculate;

    /**
     * @var array
     */
    private $_parenItemsPrice = [];
    /**
     * @var \Mage_Core_Helper_Abstract|\Mage_Tax_Helper_Data
     */
    private $taxHelper;
    /**
     * @var \Mage_Core_Model_Abstract|\Mage_Tax_Model_Calculation
     */
    private $taxCalculator;
    /**
     * @var \Mage_Core_Model_Abstract|\Mage_Tax_Model_Config
     */
    private $taxConfig;
    /**
     * @var Mage_Core_Model_Store
     */
    private $_store;
    /**
     * @var
     */
    private $_roundingDeltas;
    /**
     * @var \Mage_Core_Helper_Abstract|\SM_XRetail_Helper_Data
     */
    private $retailHelper;

    /**
     * SM_XRetail_Model_Totals_Quote_DiscountPerItems constructor.
     */
    public function __construct() {
        $this->setCode('retail_discount_per_items');
        $this->_discountPerItemsHelper    = Mage::helper('xretail/discountPerItems_data');
        $this->_discountPerItemsCalculate = Mage::helper('xretail/discountPerItems_calculate_data');
        $this->dataConfig                 = Mage::helper('xretail/dataConfig');
        $this->taxHelper                  = Mage::helper('tax');
        $this->taxCalculator              = Mage::getSingleton('tax/calculation');
        $this->taxConfig                  = Mage::getSingleton('tax/config');
        $this->retailHelper               = Mage::helper('xretail');
    }

    /**
     * @param \Mage_Sales_Model_Quote_Address $address
     *
     * @return $this
     */
    public function collect(Mage_Sales_Model_Quote_Address $address) {
        parent::collect($address);
        $quote                 = $address->getQuote();
        $applyTaxAfterDiscount = (bool)Mage::getStoreConfig(
            Mage_Tax_Model_Config::CONFIG_XML_PATH_APPLY_AFTER_DISCOUNT,
            $quote->getStoreId()
        );
        if (!$applyTaxAfterDiscount) {
            return $this;
        }
        if ($quote->isVirtual() && $address->getAddressType() == 'shipping') {
            return $this;
        }
        if (!$quote->isVirtual() && $address->getAddressType() == 'billing') {
            return $this;
        }
        if (!$this->_discountPerItemsHelper->isUseDiscount()) {
            return $this;
        }

        $this->_store = $address->getQuote()->getStore();
        $customer     = $address->getQuote()->getCustomer();

        if ($customer) {
            $this->taxCalculator->setCustomer($customer);
        }
        $request = $this->taxCalculator->getRateRequest(
            $address,
            $address->getQuote()->getBillingAddress(),
            $address->getQuote()->getCustomerTaxClassId(),
            $this->_store
        );


        $totalBaseDiscount = $this->_prepareDiscountForTaxAmount($address, $request);
        if ($totalBaseDiscount == 0)
            return $this;

        if ($totalBaseDiscount) {
            $discount = Mage::app()->getStore($quote->getStoreId())->convertPrice($totalBaseDiscount);

            $this->_addAmount(-$discount);
            $this->_addBaseAmount(-$totalBaseDiscount);

            $address->setRetailDiscountPerItemsBaseDiscount($address->getRetailDiscountPerItemsBaseDiscount() + $totalBaseDiscount);
            $address->setRetailDiscountPerItemsDiscount($address->getRetailDiscountPerItemsDiscount() + $discount);

            $quote->setRetailDiscountPerItemsBaseDiscount($address->getRetailDiscountPerItemsBaseDiscount());
            $quote->setRetailDiscountPerItemsDiscount($address->getRetailDiscountPerItemsDiscount());
        }

        return $this;
    }

    /**
     * add spending points row into quote total
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return SM_XRetail_Model_Totals_Quote_DiscountPerItems
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        $quote                 = $address->getQuote();
        $applyTaxAfterDiscount = (bool)Mage::getStoreConfig(
            Mage_Tax_Model_Config::CONFIG_XML_PATH_APPLY_AFTER_DISCOUNT,
            $quote->getStoreId()
        );
        if (!$applyTaxAfterDiscount) {
            return $this;
        }
        if ($amount = $address->getRetailDiscountPerItemsDiscount()) {
            $address->addTotal(
                [
                    'code'  => $this->getCode(),
                    'title' => 'Totals discount per items',
                    'value' => -$amount,
                ]
            );
        }

        return $this;
    }

    /**
     * Prepare Discount Amount used for Tax
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return int|mixed
     */
    public function _prepareDiscountForTaxAmount(Mage_Sales_Model_Quote_Address $address, $request) {
        $baseTotalDiscount = 0;
        $items             = $address->getAllItems();
        if (!count($items))
            return 0;

        if ($this->dataConfig->calculateDiscountByProportion())
            $this->calParentItemsPrice($items);

        foreach ($items as $item) {
            /** @var $item Mage_Sales_Model_Quote_Item */
            if ($item->getParentItemId())
                continue;

            $request->setProductClassId($item->getProduct()->getTaxClassId());
            $rate = $this->taxCalculator->getRate($request);

            $inclTax = $item->getIsPriceInclTax();

            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                $baseItemsDiscount = $item->getQty() * $this->_discountPerItemsHelper->getItemBaseDiscount($item);
                foreach ($item->getChildren() as $child) {
                    $baseItemPrice = $item->getQty() * ($child->getQty() * $this->_discountPerItemsCalculate->_getItemBasePrice($child)) -
                                     $child->getBaseDiscountAmount();


                    // Tính toàn bộ discount trên item này
                    if (!$this->dataConfig->calculateDiscountByProportion())
                        $itemBaseDiscount = min($baseItemPrice, $item->getQty() * $baseItemsDiscount);
                    else
                        $itemBaseDiscount = min(
                            $baseItemPrice,
                            $this->_deltaRound(
                                $baseItemsDiscount * $baseItemPrice / $this->_parenItemsPrice[$item->getId()],
                                $item->getId()
                            )
                        );

                    if ($baseItemPrice <= 0.001 || $itemBaseDiscount <= 0.001)
                        continue;

                    // Nếu là discount theo thứ tự thì trừ lượng discount lần này đi.
                    if (!$this->dataConfig->calculateDiscountByProportion())
                        $baseItemsDiscount -= $itemBaseDiscount;

                    $itemDiscount = $this->convertPrice($itemBaseDiscount);

                    $child->setRetailDiscountPerItemsBaseDiscount($child->getRetailDiscountPerItemsBaseDiscount() + $itemBaseDiscount)
                          ->setRetailDiscountPerItemsDiscount($child->getRetailDiscountPerItemsDiscount() + $itemDiscount);

                    switch ($this->taxConfig->getAlgorithm($this->_store)) {
                        case Mage_Tax_Model_Calculation::CALC_UNIT_BASE:
                            $baseTaxableAmount = $child->getBaseTaxableAmount();
                            $taxableAmount     = $child->getTaxableAmount();
                            $child->setBaseTaxableAmount(max(0, $baseTaxableAmount - $this->round($itemBaseDiscount / $child->getQty())));
                            $child->setTaxableAmount(max(0, $taxableAmount - $this->round($itemDiscount / $child->getQty())));
                            break;
                        case Mage_Tax_Model_Calculation::CALC_ROW_BASE:
                        case Mage_Tax_Model_Calculation::CALC_TOTAL_BASE:
                            $baseTaxableAmount = $child->getBaseTaxableAmount();
                            $taxableAmount     = $child->getTaxableAmount();
                            $child->setBaseTaxableAmount(max(0, $baseTaxableAmount - $itemBaseDiscount));
                            $child->setTaxableAmount(max(0, $taxableAmount - $itemDiscount));
                            break;
                    }

                    /*
                     * Trong trường hợp catalog_tax = incl tax thì giá discount bị trừ vào grand total là discount đã bao gồm thuế. Như cần phải tính lại
                     * giá thực tế trừ vào grand total. -> Lam o model: Mage_Tax_Model_Sales_Total_Quote_Tax
                     */

                    if ($inclTax) {
                        $hiddenTax = $this->retailHelper->registry(self::HIDDEN_TAX);
                        if (is_null($hiddenTax))
                            $hiddenTax = [];

                        $isUnitBase                     = $this->taxConfig->getAlgorithm($this->_store)
                                                          == Mage_Tax_Model_Calculation::CALC_UNIT_BASE;
                        $qty                            = $isUnitBase ? $child->getQty() : 1;
                        $baseTaxAmountOfDiscountPerItem = $this->taxCalculator->calcTaxAmount(
                            $itemBaseDiscount / $qty,
                            $rate,
                            $inclTax,
                            true);
                        $hiddenTax[]                    = [
                            'rate_key'   => $child->getId(),
                            'qty'        => $qty,
                            'item'       => $child,
                            'value'      => $this->convertPrice($baseTaxAmountOfDiscountPerItem),
                            'base_value' => $baseTaxAmountOfDiscountPerItem,
                            'incl_tax'   => $inclTax,
                        ];
                        $this->retailHelper->unregister(self::HIDDEN_TAX);
                        $this->retailHelper->register(self::HIDDEN_TAX, $hiddenTax);
                    }

                    /*
                     * IMPORTANCE
                     * Vì yêu cầu là tính discount/rule/promotin của magento sau discount per item nên sẽ sửa lại giá tính discount của promotion.
                     * Set lại giá tính discount
                     */
                    $promotionPriceCalDiscount = $this->_discountPerItemsCalculate->_getItemBasePrice($child) -
                                                 $this->round($itemBaseDiscount / $child->getQty());
                    if ($inclTax == $this->taxConfig->discountTax($this->_store)) {
                        $child->setDiscountCalculationPrice($this->convertPrice($promotionPriceCalDiscount));
                        $child->setBaseDiscountCalculationPrice($promotionPriceCalDiscount);
                    }
                    elseif ($inclTax) {
                        $baseTaxAmountOfDiscountPerItem = $this->taxCalculator->calcTaxAmount(
                            $itemBaseDiscount,
                            $rate,
                            $inclTax,
                            false);
                        $discountPerItemExclTax         = $this->round(($itemBaseDiscount - $baseTaxAmountOfDiscountPerItem) / $child->getQty());
                        $promotionPriceCalDiscount      = $this->_discountPerItemsCalculate->_getItemBasePrice($child) -
                                                          $discountPerItemExclTax;
                        $item->setDiscountCalculationPrice($this->convertPrice($promotionPriceCalDiscount));
                        $item->setBaseDiscountCalculationPrice($promotionPriceCalDiscount);
                    }
                    elseif ($this->taxConfig->discountTax($this->_store)) {
                        $baseTaxAmountOfDiscountPerItem = $this->taxCalculator->calcTaxAmount(
                            $itemBaseDiscount,
                            $rate,
                            $inclTax,
                            false);
                        $discountPerItemInclTax         = $this->round(($itemBaseDiscount + $baseTaxAmountOfDiscountPerItem) / $child->getQty());
                        $promotionPriceCalDiscount      = $this->_discountPerItemsCalculate->_getItemBasePrice($child) - $discountPerItemInclTax;
                        $item->setDiscountCalculationPrice($this->convertPrice($promotionPriceCalDiscount));
                        $item->setBaseDiscountCalculationPrice($promotionPriceCalDiscount);
                    }


                    $baseTotalDiscount += $itemBaseDiscount;
                }
            }
            elseif ($product = $item->getProduct()) {
                $baseItemPrice    = $item->getQty() * $this->_discountPerItemsCalculate->_getItemBasePrice($item) - $item->getBaseDiscountAmount();
                $itemBaseDiscount = min($item->getQty() * $this->_discountPerItemsHelper->getItemBaseDiscount($product), $baseItemPrice);

                if ($baseItemPrice <= 0.001 || $itemBaseDiscount <= 0.001)
                    continue;

                $itemDiscount = $this->convertPrice($itemBaseDiscount);
                $item->setRetailDiscountPerItemsBaseDiscount($item->getRetailDiscountPerItemsBaseDiscount() + $itemBaseDiscount)
                     ->setRetailDiscountPerItemsDiscount($item->getRetailDiscountPerItemsDiscount() + $itemDiscount);

                switch ($this->taxConfig->getAlgorithm($this->_store)) {
                    case Mage_Tax_Model_Calculation::CALC_UNIT_BASE:
                        $baseTaxableAmount = $item->getBaseTaxableAmount();
                        $taxableAmount     = $item->getTaxableAmount();
                        $item->setBaseTaxableAmount(max(0, $baseTaxableAmount - $this->round($itemBaseDiscount / $item->getQty())));
                        $item->setTaxableAmount(max(0, $taxableAmount - $this->round($itemDiscount / $item->getQty())));
                        break;
                    case Mage_Tax_Model_Calculation::CALC_ROW_BASE:
                    case Mage_Tax_Model_Calculation::CALC_TOTAL_BASE:
                        $baseTaxableAmount = $item->getBaseTaxableAmount();
                        $taxableAmount     = $item->getTaxableAmount();
                        $item->setBaseTaxableAmount(max(0, $baseTaxableAmount - $itemBaseDiscount));
                        $item->setTaxableAmount(max(0, $taxableAmount - $itemDiscount));
                        break;
                }
                /*
                 * Trong trường hợp catalog_tax = incl tax thì giá discount bị trừ vào grand total là discount đã bao gồm thuế. Như cần phải tính lại
                 * giá thực tế trừ vào grand total. -> Lam o model: Mage_Tax_Model_Sales_Total_Quote_Tax
                 */

                if ($inclTax) {
                    $hiddenTax = $this->retailHelper->registry(self::HIDDEN_TAX);
                    if (is_null($hiddenTax))
                        $hiddenTax = [];

                    $isUnitBase                     = $this->taxConfig->getAlgorithm($this->_store)
                                                      == Mage_Tax_Model_Calculation::CALC_UNIT_BASE;
                    $qty                            = $isUnitBase ? $item->getQty() : 1;
                    $baseTaxAmountOfDiscountPerItem = $this->taxCalculator->calcTaxAmount(
                        $itemBaseDiscount / $qty,
                        $rate,
                        $inclTax,
                        true);

                    $hiddenTax[] = [
                        'rate_key'   => $item->getId(),
                        'qty'        => $qty,
                        'item'       => $item,
                        'value'      => $this->convertPrice($baseTaxAmountOfDiscountPerItem),
                        'base_value' => $baseTaxAmountOfDiscountPerItem,
                        'incl_tax'   => $inclTax,
                    ];
                    $this->retailHelper->unregister(self::HIDDEN_TAX);
                    $this->retailHelper->register(self::HIDDEN_TAX, $hiddenTax);
                }
                /*
                 * IMPORTANCE
                 * Vì yêu cầu là tính discount/rule/promotin của magento sau discount per item nên sẽ sửa lại giá tính discount của promotion.
                 * Set lại giá tính discount
                 * */
                $promotionPriceCalDiscount = $this->_discountPerItemsCalculate->_getItemBasePrice($item) -
                                             $this->round($itemBaseDiscount / $item->getQty());
                if ($inclTax == $this->taxConfig->discountTax($this->_store)) {
                    $item->setDiscountCalculationPrice($this->convertPrice($promotionPriceCalDiscount));
                    $item->setBaseDiscountCalculationPrice($promotionPriceCalDiscount);
                }
                elseif ($inclTax) {
                    $baseTaxAmountOfDiscountPerItem = $this->taxCalculator->calcTaxAmount(
                        $itemBaseDiscount,
                        $rate,
                        $inclTax,
                        false);
                    $discountPerItemExclTax         = $this->round(($itemBaseDiscount - $baseTaxAmountOfDiscountPerItem) / $item->getQty());
                    $promotionPriceCalDiscount      = $this->_discountPerItemsCalculate->_getItemBasePrice($item) -
                                                      $discountPerItemExclTax;
                    $item->setDiscountCalculationPrice($this->convertPrice($promotionPriceCalDiscount));
                    $item->setBaseDiscountCalculationPrice($promotionPriceCalDiscount);
                }
                elseif ($this->taxConfig->discountTax($this->_store)) {
                    $baseTaxAmountOfDiscountPerItem = $this->taxCalculator->calcTaxAmount(
                        $itemBaseDiscount,
                        $rate,
                        $inclTax,
                        false);
                    $discountPerItemInclTax         = $this->round(($itemBaseDiscount + $baseTaxAmountOfDiscountPerItem) / $item->getQty());
                    $promotionPriceCalDiscount      = $this->_discountPerItemsCalculate->_getItemBasePrice($item) - $discountPerItemInclTax;
                    $item->setDiscountCalculationPrice($this->convertPrice($promotionPriceCalDiscount));
                    $item->setBaseDiscountCalculationPrice($promotionPriceCalDiscount);
                }


                $baseTotalDiscount += $itemBaseDiscount;
            }
        }

        return $baseTotalDiscount;
    }

    /**
     * @param $price
     * @param $rate
     *
     * @return float
     */
    public function calTax($price, $rate) {
        return $this->round(Mage::getSingleton('tax/calculation')->calcTaxAmount($price, $rate, true, false));
    }

    /**
     * Tính tổng giá children của một parent product
     *
     * @param $items
     */
    protected function calParentItemsPrice($items) {
        foreach ($items as $item) {
            /** @var $item Mage_Sales_Model_Quote_Item */
            if ($item->getParentItemId())
                continue;

            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                $this->_parenItemsPrice[$item->getId()] = 0;
                foreach ($item->getChildren() as $child) {
                    $this->_parenItemsPrice[$item->getId()] += $item->getQty() *
                                                               ($child->getQty() * $this->_discountPerItemsCalculate->_getItemBasePrice($child)) -
                                                               $child->getBaseDiscountAmount();
                }
            }
        }
    }

    /**
     * Round amount
     *
     * @param   float $price
     *
     * @return  float
     */
    public function round($price) {
        return Mage::app()->getStore()->roundPrice($price);
    }

    /**
     * Round price based on previous rounding operation delta
     *
     * @param        $price
     * @param        $parentId
     * @param string $type
     *
     * @return float
     */
    protected function _deltaRound($price, $parentId, $type = 'regular') {
        if ($price) {
            $rate = (string)$parentId;
            // initialize the delta to a small number to avoid non-deterministic behavior with rounding of 0.5
            $delta = isset($this->_roundingDeltas[$type][$rate]) ? $this->_roundingDeltas[$type][$rate] : 0.000001;
            $price += $delta;
            $this->_roundingDeltas[$type][$rate] = $price - $this->round($price);
            $price                               = $this->round($price);
        }

        return $price;
    }

    /**
     * @param      $price
     * @param null $storeId
     *
     * @return float
     * @throws \Exception
     */
    protected function convertPrice($price, $storeId = null) {
        if (is_null($storeId))
            if (is_null($this->_store))
                throw new Exception(__("Can't detect store"));
            else
                $storeId = $this->_store->getId();

        return Mage::app()->getStore($storeId)->convertPrice($price);
    }
}