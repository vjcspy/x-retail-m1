<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 22/06/2016
 * Time: 10:55
 */
require_once 'Mage/Adminhtml/controllers/Tax/ClassController.php';


class SM_XRetail_Adminhtml_Tax_ClassController extends Mage_Adminhtml_Tax_ClassController {

    /**
     * @var SM_XRetail_Model_ResourceModel_RealTimeManagement_Trigger
     */
    protected $realtimeHelper;

    public function saveAction() {
        $this->getRealTimeHelper()->triggerApp(null, array('realtime_type' => 'tax_class'));

        return parent::saveAction(); // TODO: Change the autogenerated stub
    }


    protected function getRealTimeHelper() {
        if (is_null($this->realtimeHelper)) {
            $this->realtimeHelper = Mage::getModel('xretail/resourceModel_realTimeManagement_trigger');
        }

        return $this->realtimeHelper;
    }
}