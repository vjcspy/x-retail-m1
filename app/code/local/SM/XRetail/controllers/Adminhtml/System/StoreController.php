<?php
/**
 * Created by khoild@smartosc.com/mr.vjcspy@gmail.com
 * User: vjcspy
 * Date: 09/05/2016
 * Time: 10:32
 */
require_once 'Mage/Adminhtml/controllers/System/StoreController.php';


class SM_XRetail_Adminhtml_System_StoreController extends Mage_Adminhtml_System_StoreController {


    private $realtimeHelper;

    public function __construct(
        \Zend_Controller_Request_Abstract $request,
        \Zend_Controller_Response_Abstract $response,
        array $invokeArgs
    ) {
        $this->realtimeHelper = Mage::getModel('xretail/resourceModel_realTimeManagement_trigger');
        parent::__construct($request, $response, $invokeArgs);
    }

    public function saveAction() {
        $this->realtimeHelper->triggerApp(null, array('realtime_type' => 'store'));

        return parent::saveAction();
    }
}