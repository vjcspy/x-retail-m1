<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 06/04/2016
 * Time: 12:05
 */
require_once(Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'CustomerController.php');


class SM_XRetail_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController {

    /**
     * @var SM_XRetail_Model_ResourceModel_RealTimeManagement
     */
    private $realTimeManagement;
    static $isSaveNewCustomer = false;

    public function saveAction() {
        $data = $this->getRequest()->getPost();
        if (isset($data['customer_id'])) {
            $cId = $data['customer_id'];
            $this->realTimeManagement()->needReloadRealTime($cId, SM_XRetail_Model_ResourceModel_RealTimeManagement::TYPE_CUSTOMER_CHANGE);
        }
        else
            SM_XRetail_Adminhtml_CustomerController::$isSaveNewCustomer = true;

        return parent::saveAction();
    }

    public function deleteAction() {
        parent::deleteAction(); // TODO: Change the autogenerated stub
        $customer = Mage::registry('current_customer');
        if ($id = $customer->getId()) {
            $this->realTimeManagement()->storeRemoveObject($id, SM_XRetail_Model_ResourceModel_RealTimeManagement::TYPE_CUSTOMER_DELETE);
        }
    }

    /**
     * @return \SM_XRetail_Model_ResourceModel_RealTimeManagement
     */
    protected function realTimeManagement() {
        if (is_null($this->realTimeManagement))
            $this->realTimeManagement = Mage::getModel('xretail/resourceModel_realTimeManagement');

        return $this->realTimeManagement;
    }
}