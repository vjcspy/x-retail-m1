<?php
/**
 * Created by khoild@smartosc.com/mr.vjcspy@gmail.com
 * User: vjcspy
 * Date: 09/05/2016
 * Time: 11:32
 */
require_once 'Mage/Adminhtml/controllers/Catalog/CategoryController.php';


class SM_XRetail_Adminhtml_Catalog_CategoryController extends Mage_Adminhtml_Catalog_CategoryController {

    private $realtimeHelper;

    public function __construct(
        \Zend_Controller_Request_Abstract $request,
        \Zend_Controller_Response_Abstract $response,
        array $invokeArgs
    ) {
        $this->realtimeHelper = Mage::getModel('xretail/resourceModel_realTimeManagement_trigger');
        parent::__construct($request, $response, $invokeArgs);
    }

    public function saveAction() {
        $this->realtimeHelper->triggerApp(null, array('realtime_type' => 'category'));

        return parent::saveAction();
    }
}