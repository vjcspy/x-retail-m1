<?php
/**
 * Created by IntelliJ IDEA.
 * User: tungdt2
 * Date: 06/04/2016
 * Time: 12:05
 */
if(Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()){

    require_once(Mage::getModuleDir('controllers', 'Magestore_Inventorywarehouse') . DS . 'Adminhtml/Inw/RequeststockController.php');

    class SM_XRetail_Adminhtml_Inw_RequeststockController extends Magestore_Inventorywarehouse_Adminhtml_Inw_RequeststockController {

        public function saveAction()
        {
            parent::saveAction();

            if (Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()) {
                $data = $this->getRequest()->getPost();
                $requeststockProducts = array();
                parse_str(urldecode($data['requeststock_products']), $requeststockProducts);

                $warehouseIds = array($data['warehouse_source'], $data['warehouse_target']);
                $warehouseProduct = Mage::helper('xretail/integrate')->getStockWarehouseProducts($requeststockProducts, $warehouseIds);
                Mage::getModel('xretail/resourceModel_realTimeManagement')->needReloadRealTime($warehouseProduct, SM_XRetail_Model_ResourceModel_RealTimeManagement::TYPE_WAREHOUSE_PRODUCT_CHANGE);

            }
        }
    }
}
