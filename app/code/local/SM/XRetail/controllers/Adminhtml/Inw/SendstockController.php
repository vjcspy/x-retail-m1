<?php
/**
 * Created by IntelliJ IDEA.
 * User: tungdt2
 * Date: 06/04/2016
 * Time: 12:05
 */
if (Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()) {

    require_once(Mage::getModuleDir('controllers', 'Magestore_Inventorywarehouse') . DS . 'Adminhtml/Inw/SendstockController.php');

    class SM_XRetail_Adminhtml_Inw_SendstockController extends Magestore_Inventorywarehouse_Adminhtml_Inw_SendstockController
    {

        public function saveAction()
        {
            parent::saveAction();

            if (Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()) {
                $sendstockProducts = array();
                $data = $this->getRequest()->getPost();
                parse_str(urldecode($data['sendstock_products']), $sendstockProducts);

                $warehouseIds = array($data['warehouse_source'], $data['warehouse_target']);
                $warehouseProduct = Mage::helper('xretail/integrate')->getStockWarehouseProducts($sendstockProducts, $warehouseIds);
                Mage::getModel('xretail/resourceModel_realTimeManagement')->needReloadRealTime($warehouseProduct, SM_XRetail_Model_ResourceModel_RealTimeManagement::TYPE_WAREHOUSE_PRODUCT_CHANGE);
            }
        }
    }
}
