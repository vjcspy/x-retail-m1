<?php
/**
 * Created by IntelliJ IDEA.
 * User: tungdt2
 * Date: 06/04/2016
 * Time: 12:05
 */
if(Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()){

    require_once(Mage::getModuleDir('controllers', 'Magestore_Inventoryplus') . DS . 'Adminhtml/Inp/AdjuststockController.php');

    class SM_XRetail_Adminhtml_Inp_AdjuststockController extends Magestore_Inventoryplus_Adminhtml_Inp_AdjuststockController {

        public function saveAction()
        {
           $response = parent::saveAction();

            if($this->getRequest()->getParam('confirm') && Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()){
                $data = $this->getRequest()->getPost();
                parse_str(urldecode($this->getRequest()->getPost('adjuststock_products')), $stocks);

                /** if $stocks item do not need to import by step
                 * trigger real time data to client
                 */
                foreach ($response->getResponse()->getHeaders() as $header){
                    if($header['name'] == 'Location'){
                        if (strpos($header['value'], 'resum') === false) {
                            $warehouseProducts = Mage::helper('xretail/integrate')->getStockWarehouseProducts($stocks, array($data['warehouse_id']));
                            Mage::getModel('xretail/resourceModel_realTimeManagement')->needReloadRealTime($warehouseProducts, SM_XRetail_Model_ResourceModel_RealTimeManagement::TYPE_WAREHOUSE_PRODUCT_CHANGE);
                        }
                    }
                }

            }
        }

        public function doProcessAction() {

            parent::doProcessAction();

            /** if save stock item completed
             * implement pull all data by warehouse process
             */

            $type = $this->getDataType();

            if (strcmp($type, 'Finished') == 0) {
                $model = Mage::getModel('inventoryplus/adjuststock')->load($this->getRequest()->getParam('id'));

                Mage::getModel('xretail/resourceModel_realTimeManagement_trigger')
                    ->triggerApp(null, array('warehouse_product' => 'warehouse_id' . $model->getWarehouseId()));
            }

        }
    }
}
