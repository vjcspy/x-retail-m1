<?php
/**
 * Created by IntelliJ IDEA.
 * User: tungdt2
 * Date: 06/04/2016
 * Time: 12:05
 */
if(Mage::helper('xretail/integrate')->isMultiWarehouseAvailable()){

    require_once(Mage::getModuleDir('controllers', 'Magestore_Inventoryplus') . DS . 'Adminhtml/Inp/WarehouseController.php');

    class SM_XRetail_Adminhtml_Inp_WarehouseController extends Magestore_Inventoryplus_Adminhtml_Inp_WarehouseController {

        public function saveAction()
        {
            parent::saveAction();
            if(Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()){
                Mage::getModel('xretail/resourceModel_realTimeManagement_trigger')
                    ->triggerApp(null, array('realtime_type' => 'warehouse'));
            }
        }

        public function deleteAction()
        {
            parent::deleteAction();
            if(Mage::helper('xretail/integrate')->isIntegrateWithMultiWarehouse()){
                Mage::getModel('xretail/resourceModel_realTimeManagement_trigger')
                    ->triggerApp(null, array('realtime_type' => 'warehouse'));
            }
        }
    }
}
