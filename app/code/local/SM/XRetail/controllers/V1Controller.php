<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/22/16
 * Time: 3:05 PM
 */

require_once(BP .
    DS .
    'app' .
    DS .
    'code' .
    DS .
    'local' .
    DS .
    'SM' .
    DS .
    'XRetail' .
    DS .
    'controllers' .
    DS .
    'Contract' .
    DS .
    'ApiAbstract.php');

/**
 * Class SM_XRetail_V1Controller
 */
class SM_XRetail_V1Controller extends SM_XRetail_Contract_ApiAbstract {

    private $_statusCode;

    /**
     * @return mixed
     */
    public function getStatusCode() {
        return $this->_statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode) {
        $this->_statusCode = $statusCode;
    }

    /**
     * @var \SM_XRetail_Model_Authentication
     */
    private $authentication;

    public function __construct(
        \Zend_Controller_Request_Abstract $request,
        \Zend_Controller_Response_Abstract $response,
        array $invokeArgs
    ) {
        $this->authentication = Mage::getModel('xretail/authentication');
        parent::__construct($request, $response, $invokeArgs);
    }

    /**
     * Rest API
     *
     * @return Zend_Controller_Response_Abstract
     */
    public function XretailAction() {

        try {
            // authenticate
            $this->authentication->authenticate($this);

            // communicate with api before
            $this->dispatchEvent('rest_api_before', array('apiController' => $this));
            // call service
            $this->setOutput(
                call_user_func_array(
                    array($this->getService(), $this->getFunction()),
                    $this->getRequest()->getParams()));
            // communicate with api after
            $this->dispatchEvent('rest_api_after', array('apiController' => $this));

            // output data
            return $this->jsonOutput();

        } catch (Exception $e) {
            return $this->outputError($e->getMessage(), $this->getStatusCode());
        }
    }

    public function authenticateAction() {
        try {
            //check license key
            $this->setOutput($this->authentication->getBlackHole($this));

            return $this->jsonOutput();
        } catch (Exception $e) {
            return $this->outputError($e->getMessage(), $this->getStatusCode());
        }
    }

    public function testAction() {
        $importDir = Mage::getBaseDir('media') . DS . 'upload-incoming/';
        $fileName  = 'keshco.jpg';
        $filePath  = $importDir . $fileName;
        $product   = Mage::getModel('catalog/product')->load(938);
        if (file_exists($filePath)) {
            $product->addImageToMediaGallery($filePath, array('image', 'small_image', 'thumbnail'), true, false);
            $product->save();
        }
        else {
            echo   " not done";
        }
    }
}
