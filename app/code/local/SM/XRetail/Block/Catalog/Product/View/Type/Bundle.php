<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 31/03/2016
 * Time: 09:50
 */
class SM_XRetail_Block_Catalog_Product_View_Type_Bundle extends Mage_Bundle_Block_Catalog_Product_View_Type_Bundle {
    /**
     * @return $this
     */
    public function resetOptions() {
        $this->_options = null;
        return $this;
    }
}