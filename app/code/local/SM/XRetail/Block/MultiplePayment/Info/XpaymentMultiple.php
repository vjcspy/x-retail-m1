<?php

class SM_XRetail_Block_MultiplePayment_Info_XpaymentMultiple extends Mage_Payment_Block_Info {
    protected $_yaSuo;

    protected function _construct() {
        parent::_construct();
        $this->setTemplate('xretail/info/xpaymentMultiple.phtml');
    }

    public function getConfigDataPaymentMethod($code, $field) {
        $path = 'payment/' . $code . '/' . $field;

        return Mage::getStoreConfig($path);
    }

    public function getYaSuo() {
        if (is_null($this->_yaSuo))
            $this->_yaSuo = $this->_convertAdditionalData();
        return $this->_yaSuo;
    }

    protected function _convertAdditionalData() {
        return unserialize($this->getInfo()->getAdditionalData());
    }

}
