<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 02/06/2016
 * Time: 18:06
 */
class SM_XRetail_Block_Adminhtml_Sales_Order_Totals extends Mage_Adminhtml_Block_Sales_Order_Totals {

    /**
     * Neu Thay doi due thi hieu la: Chi refund lai so tien bang chinh so tien khach hang da tra va giu lai phan adjust_refund amount
     * Con neu due khong doi thi hieu la so tien tra lai cho khach hang bang = refund adjust + refund amount
     */
    const ADJUST_DUE = false;
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals() {
        if ($this->getSource()->getData('retail_refund_adjust') > 0 && self::ADJUST_DUE) {
            parent::_initTotals();
            $this->_totals['paid']     = new Varien_Object(
                array(
                    'code'       => 'paid',
                    'strong'     => true,
                    'value'      => $this->getSource()->getTotalPaid(),
                    'base_value' => $this->getSource()->getBaseTotalPaid(),
                    'label'      => $this->helper('sales')->__('Total Paid'),
                    'area'       => 'footer'
                ));
            $this->_totals['refunded'] = new Varien_Object(
                array(
                    'code'       => 'refunded',
                    'strong'     => true,
                    'value'      => $this->getSource()->getTotalRefunded(),
                    'base_value' => $this->getSource()->getBaseTotalRefunded(),
                    'label'      => $this->helper('sales')->__('Total Refunded'),
                    'area'       => 'footer'
                ));

            $this->_totals['due'] = new Varien_Object(
                array(
                    'code'       => 'due',
                    'strong'     => true,
                    'value'      => $this->getSource()->getGrandTotal() -
                        $this->getSource()->getTotalPaid() -
                        $this->getSource()->getData('retail_refund_adjust'),
                    'base_value' => $this->getSource()->getBaseGrandTotal() -
                        $this->getSource()->getBaseTotalPaid() -
                        $this->getSource()->getData('base_retail_refund_adjust'),
                    'label'      => $this->helper('sales')->__('Total Due'),
                    'area'       => 'footer'
                ));

            return $this;
        }
        else {
            return parent::_initTotals();
        }


    }
}