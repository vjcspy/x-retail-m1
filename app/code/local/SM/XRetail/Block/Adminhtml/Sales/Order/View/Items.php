<?php

/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 3/21/16
 * Time: 5:00 PM
 */
class SM_XRetail_Block_Adminhtml_Sales_Order_View_Items extends Mage_Adminhtml_Block_Sales_Order_View_Items {
    private $_itemsCollection = array();

    /**
     * Retrieve order items collection
     *
     * @return unknown
     */
    public function getItemsCollection() {
        /* @var $item Mage_Sales_Model_Order_Item */
        $itemsCollection = $this->getOrder()->getItemsCollection();
        foreach ($itemsCollection as $item) {
            $item->setDiscountAmount($item->getDiscountAmount() + $item->getRetailDiscountPerItemsDiscount());
            $this->_itemsCollection[] = $item;
        }
        return $this->_itemsCollection;
    }
}