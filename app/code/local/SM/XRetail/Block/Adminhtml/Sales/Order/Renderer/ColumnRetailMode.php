<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 25/05/2016
 * Time: 10:52
 */
class SM_XRetail_Block_Adminhtml_Sales_Order_Renderer_ColumnRetailMode extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $value = $row->getData('retail_order_create_mode');
        if ($value == SM_XRetail_Model_ResourceModel_OrderManagement::ONLINE_MODE)
            return '<span style="color:seagreen;">' . 'X-Retail' . '</br>' . '&nbsp&nbsp&nbspONLINE' . '</span>';
        elseif ($value == SM_XRetail_Model_ResourceModel_OrderManagement::OFFLINE_MODE)
            return '<span style="color:red;">' . 'X-Retail' . '</br>' . '&nbsp&nbsp&nbspOFFLINE' . '</span>';
        else
            return '';
    }
}