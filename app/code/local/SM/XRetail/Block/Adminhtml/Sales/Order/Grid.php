<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 25/05/2016
 * Time: 10:13
 */
class SM_XRetail_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid {

    protected function _prepareCollection() {
        /** @var $collection Mage_Sales_Model_Resource_Order_Grid_Collection */
        $collection = Mage::getResourceModel($this->_getCollectionClass());

        $collection->getSelect()->joinLeft(
            array('order_flat' => 'sales_flat_order'),
            "main_table.entity_id=order_flat.entity_id",
            array('retail_order_create_mode' => 'order_flat.retail_order_create_mode'));
        // die($collection->getSelect()->__toString());
        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn(
            'real_order_id',
            array(
                'header'       => Mage::helper('sales')->__('Order #'),
                'width'        => '80px',
                'type'         => 'text',
                'index'        => 'increment_id',
                'filter_index' => 'main_table.increment_id'
            ));

        $this->addColumn(
            'retail_order_create_mode',
            array(
                'header'   => Mage::helper('sales')->__('Retail Mode'),
                'width'    => '80px',
                'type'     => 'options',
                'index'    => 'order_flat.retail_order_create_mode',
                'renderer' => 'SM_XRetail_Block_Adminhtml_Sales_Order_Renderer_ColumnRetailMode',
                'options'  => array(
                    SM_XRetail_Model_ResourceModel_OrderManagement::ONLINE_MODE  => 'Online',
                    SM_XRetail_Model_ResourceModel_OrderManagement::OFFLINE_MODE => 'Offline'
                ),
            ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn(
                'store_id',
                array(
                    'header'          => Mage::helper('sales')->__('Purchased From (Store)'),
                    'index'           => 'store_id',
                    'type'            => 'store',
                    'store_view'      => true,
                    'display_deleted' => true,
                    'filter_index' => 'main_table.store_id'
                ));
        }

        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('sales')->__('Purchased On'),
                'index'  => 'created_at',
                'type'   => 'datetime',
                'width'  => '100px',
                'filter_index' => 'main_table.created_at'
            ));

        $this->addColumn(
            'billing_name',
            array(
                'header' => Mage::helper('sales')->__('Bill to Name'),
                'index'  => 'billing_name',
            ));

        $this->addColumn(
            'shipping_name',
            array(
                'header' => Mage::helper('sales')->__('Ship to Name'),
                'index'  => 'shipping_name',
            ));

        $this->addColumn(
            'base_grand_total',
            array(
                'header'   => Mage::helper('sales')->__('G.T. (Base)'),
                'index'    => 'base_grand_total',
                'type'     => 'currency',
                'currency' => 'base_currency_code',
                'filter_index' => 'main_table.base_grand_total'
            ));

        $this->addColumn(
            'grand_total',
            array(
                'header'   => Mage::helper('sales')->__('G.T. (Purchased)'),
                'index'    => 'grand_total',
                'type'     => 'currency',
                'currency' => 'order_currency_code',
                'filter_index' => 'main_table.grand_total'
            ));

        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('sales')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'width'   => '70px',
                'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
                'filter_index' => 'main_table.status'
            ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn(
                'action',
                array(
                    'header'    => Mage::helper('sales')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'    => 'getId',
                    'actions'   => array(
                        array(
                            'caption'     => Mage::helper('sales')->__('View'),
                            'url'         => array('base' => '*/sales_order/view'),
                            'field'       => 'order_id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
                ));
        }
        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }
}