<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 02/06/2016
 * Time: 16:07
 */
class SM_XRetail_Block_Adminhtml_Totals_Order_AdjustRefund extends Mage_Adminhtml_Block_Sales_Order_Totals_Item {

    public function initTotals() {
        $totalsBlock = $this->getParentBlock();
        $order       = $totalsBlock->getOrder();

        if ($order->getData(SM_XRetail_Model_ResourceModel_XRetailOrderRefund::RETAIL_REFUND_ADJUST_CODE) >= 0.0001) {
            $totalsBlock->addTotal(
                new Varien_Object(
                    array(
                        'code'   => 'refund_adjust',
                        'label'  => $this->__('Refund Partital Adjust'),
                        'value'  => -$order->getData(SM_XRetail_Model_ResourceModel_XRetailOrderRefund::RETAIL_REFUND_ADJUST_CODE),
                        'strong' => true
                    )
                ),
                'paid'
            );
        }
    }
}