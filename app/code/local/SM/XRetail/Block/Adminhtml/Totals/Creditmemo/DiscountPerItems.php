<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 6:01 PM
 */
class SM_XRetail_Block_Adminhtml_Totals_Creditmemo_DiscountPerItems extends Mage_Adminhtml_Block_Sales_Order_Totals_Item {
    public function initTotals() {
        $totalsBlock = $this->getParentBlock();
        $creditmemo = $totalsBlock->getCreditmemo();
        if ($creditmemo->getRetailDiscountPerItemsDiscount() >= 0.0001) {
            $totalsBlock->addTotal(
                new Varien_Object(
                    array(
                        'code'  => 'rewardpoints',
                        'label' => $this->__('Totals X-Retail Discount Per Items'),
                        'value' => -$creditmemo->getRetailDiscountPerItemsDiscount(),
                    )
                ), 'subtotal'
            );
        }
    }
}
