<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 5:42 PM
 */
class SM_XRetail_Block_Adminhtml_Totals_Invoice_DiscountPerItems extends Mage_Adminhtml_Block_Sales_Order_Totals_Item {
    /**
     * add points value into invoice total
     *
     */
    public function initTotals() {
        $totalsBlock = $this->getParentBlock();
        $invoice = $totalsBlock->getInvoice();
        if ($invoice->getRetailDiscountPerItemsDiscount() >= 0.0001) {
            $totalsBlock->addTotal(
                new Varien_Object(
                    array(
                        'code'  => 'rewardpoints',
                        'label' => $this->__('Totals X-Retail Discount Per Items'),
                        'value' => -$invoice->getRetailDiscountPerItemsDiscount(),
                    )
                ), 'subtotal'
            );
        }
    }
}