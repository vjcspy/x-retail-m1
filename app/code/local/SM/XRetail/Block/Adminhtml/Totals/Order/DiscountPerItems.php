<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 4:49 PM
 */
class SM_XRetail_Block_Adminhtml_Totals_Order_DiscountPerItems extends Mage_Adminhtml_Block_Sales_Order_Totals_Item {
    public function initTotals() {
        $totalsBlock = $this->getParentBlock();
        $order = $totalsBlock->getOrder();

        if ($order->getRetailDiscountPerItemsDiscount() >= 0.0001) {
            $totalsBlock->addTotal(
                new Varien_Object(
                    array(
                        'code'  => 'rewardpoints',
                        'label' => $this->__('Totals X-Retail Discount Per Items'),
                        'value' => -$order->getRetailDiscountPerItemsDiscount(),
                    )
                ), 'subtotal'
            );
        }
    }
}
