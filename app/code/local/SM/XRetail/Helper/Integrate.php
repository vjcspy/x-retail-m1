<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 22/07/2016
 * Time: 10:05
 */
class SM_XRetail_Helper_Integrate extends Mage_Core_Helper_Abstract{

    protected $_isMultiWarehouseEnable;

    protected $_isIntegrateMultiWarehouse;

    public function __construct(){
        $this->_isMultiWarehouseEnable =
            (Mage::helper('core')->isModuleEnabled('Magestore_Inventorywarehouse')
            && Mage::helper('core')->isModuleEnabled('Magestore_Inventoryplus'))
            && Mage::helper('core')->isModuleOutputEnabled('Magestore_Inventoryplus')
            && Mage::helper('core')->isModuleOutputEnabled('Magestore_Inventorywarehouse');
        $this->_isIntegrateMultiWarehouse = !!Mage::getModel('xretail/api_configuration')->getConfig('xretail/config/integration.magestore_warehouse')->getValue();
    }

    public function isIntegrateWithMultiWarehouse(){
        return $this->_isIntegrateMultiWarehouse;
    }

    public function isMultiWarehouseAvailable(){
        return $this->_isMultiWarehouseEnable;
    }

    public function getStockWarehouseProducts($productIds, $warehouseIds)
    {

        $warehouseProduct = array();
        foreach ($productIds as $pId => $enCoded) {
            $stockProduct = $this->getWarehouseProduct($pId, $warehouseIds);
            foreach ($stockProduct as $warehouseProductId => $stock){
                $warehouseProduct[$warehouseProductId] = $stock;
            }
        }
        return $warehouseProduct;
    }

    public function getWarehouseProduct($id, $warehouseIds)
    {
        $productStocks = array();
        $prefix = Mage::getConfig()->getTablePrefix();

        $warehouseProducts = Mage::getModel('inventoryplus/warehouse_product')->getCollection();

        $warehouseProducts->getSelect()->join(
            array('stock' => $prefix . 'cataloginventory_stock_item'),
            'stock.product_id = main_table.product_id',
            array(
                'product_id',
                'is_in_stock',
                'backorders',
                'min_qty',
                'stock_id',
                'is_qty_decimal',
                'min_sale_qty',
                'max_sale_qty',
                'low_stock_date',
                'manage_stock'
            )
        )->where('`main_table`.`product_id` = '.$id);

        if(!empty($warehouseIds))
            $warehouseProducts->addFieldToFilter('warehouse_id', $warehouseIds);

        $warehouseProducts->addFieldToSelect(array('qty' => 'available_qty'));
        $warehouseProducts->addFieldToSelect(array('id' => 'warehouse_product_id'));
        $warehouseProducts->addFieldToSelect('warehouse_id');

        foreach($warehouseProducts->getData() as $productStock){
            $productStocks[$productStock['id']] = $productStock;
        }

        return $productStocks;
    }
}