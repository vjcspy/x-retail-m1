<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/29/16
 * Time: 10:58 AM
 */
class SM_XRetail_Helper_Customer extends Mage_Core_Helper_Abstract {
    public function getStreet($street) {
        $value = '';
        foreach ($street as $k => $v) {
            if ($k != 0 && $v != '') {
                $value .= ', ' . $v;
            } else {
                $value .= $v;
            }
        }

        return $value;
    }
}
