<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/17/16
 * Time: 2:57 PM
 */


/**
 * Class CacheManagement
 *
 * @package SM\XRetail\Helper\Realtime
 */
class SM_XRetail_Helper_Realtime_CacheManagement extends Mage_Core_Helper_Abstract {

    /**
     * KEY MANAGER
     */
    const DATA_CACHE_MANAGER_KEY = 'data_cache_manager_key';

    /**
     * @var Zend_Cache_Core
     */
    protected $_mageCache;

    /**
     * @var false|mixed
     */
    protected $_dataCacheManager;

    /**
     * @var SM_XRetail_Helper_Realtime_Data_Products
     */
    protected $_cacheProducts;
    /**
     * @var SM_XRetail_Helper_Realtime_Data_Options
     */
    protected $_cacheOptions;
    /**
     * @var SM_XRetail_Helper_Realtime_Data_StockItems
     */
    protected $_cacheStockItems;
    /**
     * @var \SM_XRetail_Model_Api_Configuration
     */
    protected $_apiConfig;

    /**
     * SM_XRetail_Helper_Realtime_CacheManagement constructor.
     */
    public function __construct() {
        $this->_mageCache       = Mage::app()->getCache();
        $this->_cacheOptions    = Mage::helper('xretail/realtime_data_options');
        $this->_cacheProducts   = Mage::helper('xretail/realtime_data_products');
        $this->_cacheStockItems = Mage::helper('xretail/realtime_data_stockItems');
        $this->_apiConfig       = Mage::getModel('xretail/api_configuration');

        $this->initDataCacheManager();
    }

    private function initDataCacheManager() {
        $this->_dataCacheManager = ($data = $this->_mageCache->load(self::DATA_CACHE_MANAGER_KEY)) != null ? json_decode(
            $data,
            true) : array();
    }

    /**
     * Get key for cache
     *
     * @param array $data
     *
     * @return string
     */
    protected function getCacheKey($data) {
        $serializeData = array();
        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $serializeData[$key] = $value->getId();
            }
            else {
                $serializeData[$key] = $value;
            }
        }

        return md5(serialize($serializeData));
    }


    /*----------------------TODO: communicate with products----------------------*/
    /**
     * @param        $product
     * @param string $fieldId
     *
     *
     *
     * @param int    $storeId
     *
     * @return $this
     * @throws \Exception
     */
    public function addProduct($product, $fieldId = 'id', $storeId = 0) {
        $this->_cacheProducts->getCacheByStore($storeId)->addItem($product, $fieldId);

        return $this;
    }

    /**
     * TODO: update những product change vào cache, nếu sử dụng cron thì chuyển sang chạy asynchronous task thì sẽ cải thiện performance
     *
     * @param array  $products
     * @param string $filedId
     * @param int    $storeId
     */
    public function updateProducts(array $products, $filedId = 'id', $storeId = 0) {
        // FIXME: if use queue, performance will increase!
        $this->_cacheProducts->getCacheByStore($storeId)->updateItems($products, $filedId);
    }

    /**
     * @param     $page
     * @param     $pageSize
     *
     *
     * @param int $storeId
     *
     * @return mixed
     */
    public function getProducts($page, $pageSize, $storeId = 0) {
        return $this->_cacheProducts->getCacheByStore($storeId)->getItems($page, $pageSize);
    }
    /*----------------------end: communicate with product----------------------*/

    /*----------------------TODO: communicate with stock_items----------------------*/
    /**
     * @param $item
     *
     * @return Mage_CatalogInventory_Model_Stock_Item
     */
    public function getStockItem($item) {
        return $this->_cacheStockItems->getStockItem($item);
    }

    /**
     * @param $itemId
     *
     * @return bool
     */
    public function loadStockItemFromXCache($itemId) {
        return $this->_cacheStockItems->getItemsById($itemId);
    }
    /*----------------------end: communicate with stock_items----------------------*/

    /*----------------------TODO: communicate with options----------------------*/
    /**
     * @param $item
     *
     * @return array|mixed|null
     */
    public function getXOptions($item) {
        $xOptions = array();
        if (!($item instanceof Mage_Catalog_Model_Product))
            $item = $this->_cacheOptions->getProductByProductId($item->getId());

        Mage::helper('catalog/product')->setSkipSaleableCheck(true);
        if ($item->getTypeId() == 'configurable')
            $xOptions = array_merge($xOptions, $this->_cacheOptions->getOptionsConfigurableByProduct($item));
        if ($item->getTypeId() == 'bundle')
            $xOptions = array_merge($xOptions, $this->_cacheOptions->getOptionsBundleProduct($item));
        if ($item->getTypeId() == 'grouped')
            $xOptions = array_merge($xOptions, $this->_cacheOptions->getAssociatedProducts($item));

        $typeGetCustomOptions = $this->_apiConfig->getStoreConfig('xretail/config/typeGetCustomOptions');
        if ($item->getOptions() && in_array($item->getTypeId(), preg_split('/,+/', $typeGetCustomOptions)))
            $xOptions = array_merge($xOptions, $this->_cacheOptions->getCustomOptionsSimpleProduct($item));

        return $xOptions;
    }
    /*----------------------end: communicate with options----------------------*/

    /**
     *Clean all cache service
     */
    public function cleanCache() {
        $this->_cacheProducts->cleanCache();
        $this->_cacheOptions->cleanCache();
        $this->_cacheStockItems->cleanCache();
    }

    /**
     * Check If continuous process loading
     *
     * @param        $page
     * @param        $pageSize
     *
     * @param        $totalCount
     * @param string $prefix
     *
     * @return bool
     */
    public function isContinuousLoading($page, $pageSize, $totalCount, $prefix = 'product_data') {
        if (!isset($this->_dataCacheManager[$prefix]) && $page != 1)
            return false;
        elseif ($page == 1 && !isset($this->_dataCacheManager[$prefix]))
            return true;
        elseif (isset($this->_dataCacheManager[$prefix]) && $this->_dataCacheManager[$prefix]['totalCount'] != $totalCount) {
            $this->cleanCache();

            return false;
        }
        else
            return isset($this->_dataCacheManager[$prefix]) &&
            $page == ($this->_dataCacheManager[$prefix]['page'] + 1) &&
            $pageSize == $this->_dataCacheManager[$prefix]['pageSize'] ? true : false;
    }

    /**
     * Kiểm tra xem đã load full data chưa
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function isLoadedFullData($prefix = 'product_data') {
        return isset($this->_dataCacheManager[$prefix]['loadFull']) && $this->_dataCacheManager[$prefix]['loadFull'] == true ? true : false;
    }

    /**
     * @param        $currentPage
     * @param        $totalCount
     * @param bool   $isLastPage
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function updateIndexLoading($currentPage, $totalCount, $isLastPage = false, $prefix = 'product_data') {
        $this->_dataCacheManager[$prefix] = array(
            'page'       => $currentPage,
            'pageSize'   => SM_XRetail_Helper_Realtime_Data::PAGE_SIZE_LOAD_PRODUCT,
            'loadFull'   => $isLastPage,
            'totalCount' => $totalCount
        );

        return $this->_mageCache->save(
            json_encode($this->_dataCacheManager),
            self::DATA_CACHE_MANAGER_KEY,
            array('xCache_RealTime'),
            SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract::XCACHE_LIFE_TIME);
    }

    /**
     * @param string $prefix
     *
     * @return null
     */
    public function getTotalCount($prefix = 'product_data') {
        return isset($this->_dataCacheManager[$prefix]) ? $this->_dataCacheManager[$prefix]['totalCount'] : null;
    }
}
