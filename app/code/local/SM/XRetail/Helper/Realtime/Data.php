<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/17/16
 * Time: 2:48 PM
 */
class SM_XRetail_Helper_Realtime_Data extends Mage_Core_Helper_Abstract {

    const PAGE_SIZE_LOAD_PRODUCT = 100;

    public function register($key, $value, $graceful = false) {
        Mage::register($key, $value, $graceful);
    }

    public function unregister($key) {
        Mage::unregister($key);
    }

    public function registry($key) {
        return Mage::registry($key);
    }
}
