<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/17/16
 * Time: 2:48 PM
 */
class SM_XRetail_Helper_Realtime_Data_Options extends SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract {

    /**
     * @var string
     */
    protected $_prefix_cache_key = 'xOptions_xCache';

    /**
     * @var Mage_Adminhtml_Block_Catalog_Product_Composite_Fieldset_Configurable
     */
    protected $_configurableBlock;
    /**
     * @var SM_XRetail_Block_Catalog_Product_View_Type_Bundle
     */
    protected $_bundleBlock;
    /**
     * @var Mage_Adminhtml_Block_Catalog_Product_Composite_Fieldset_Grouped
     */
    protected $_groupBlock;

    /**
     * Options constructor.
     */
    public function __construct() {
        $this->_configurableBlock = Mage::getBlockSingleton('catalog/product_view_Type_configurable');
        $this->_groupBlock        = Mage::getBlockSingleton('adminhtml/catalog_product_composite_fieldset_grouped');
        $this->_bundleBlock       = Mage::getBlockSingleton('xretail/catalog_product_view_type_bundle');
        parent::__construct();
    }

    /**
     * @param $productId
     *
     * @return mixed
     */
    public function getProductByProductId($productId) {
        return (Mage::getModel('catalog/product')
                    ->load($productId));
    }

    /**
     * cache runtime options
     *
     * @var
     */
    protected $_dataOptions;

    /**
     * @param $product
     *
     * @return mixed
     */
    public function getOptionsConfigurableByProduct(Mage_Catalog_Model_Product $product) {
        $key = $this->getCacheKey(array('configOptions', $product->getId()));
        if (!isset($this->_dataOptions[$key])) {
            $this->_configurableBlock->unsetData('product');
            $this->_configurableBlock->unsetData('allow_products');
            $this->resetProductInBlock($product);
            $this->_dataOptions[$key] = json_decode(
                $this->_configurableBlock->getJsonConfig(),
                true);
        }

        return $this->_dataOptions[$key];
    }

    /**
     * @param $product
     *
     * @return mixed
     */
    public function getOptionsBundleProduct(Mage_Catalog_Model_Product $product) {
        $key = $this->getCacheKey(array('bundleOptions', $product->getId()));
        if (!isset($this->_dataOptions[$key])) {
            $this->_bundleBlock->unsetData('product');
            $this->resetProductInBlock($product);
            $outputOptions = array();
            $options       = (Mage::helper('core')
                                  ->decorateArray($this->_bundleBlock->resetOptions()->getOptions()));
            foreach ($options
                     as
                     $option) {
                foreach ($option->getSelections() as $selection) {
                    if (isset($outputOptions[$option->getTitle()])) {
                        $outputOptions[$option->getTitle()]['section'][] = array('product_id' => $selection->getData());
                    }
                    else {
                        $outputOptions[$option->getTitle()] = array(
                            'data'    => $option->getData(),
                            'section' => array(
                                array(
                                    'product_id' => $selection->getData()
                                )
                            )
                        );
                    }
                }
            }
            $this->_dataOptions[$key] = ($outputOptions);
        }

        return $this->_dataOptions[$key];
    }


    /**
     * @param $product
     *
     * @return mixed
     */
    public function getAssociatedProducts(Mage_Catalog_Model_Product $product) {
        $key = $this->getCacheKey(array('groupOptions', $product->getId()));
        if (!isset($this->_dataOptions[$key])) {
            $outputOptions = array();
            $this->_groupBlock->unsetData('product');
            $this->resetProductInBlock($product);
            $this->_groupBlock->setPreconfiguredValue();
            $_associatedProducts    = $this->_groupBlock->getAssociatedProducts();
            $_hasAssociatedProducts = count($_associatedProducts) > 0;
            if ($_hasAssociatedProducts) {
                foreach ($_associatedProducts as $_item) {
                    $outputOptions[] = $_item->getData();
                }
            }
            $this->_dataOptions[$key] = ($outputOptions);
        }

        return $this->_dataOptions[$key];
    }

    public function getCustomOptionsSimpleProduct(Mage_Catalog_Model_Product $product) {
        $options = array();
        if ($product->getOptions()) {
            foreach ($product->getOptions()
                     as
                     $value) {
                $custom_option      = $value->getData();
                $values             = $value->getValues();
                $custom_option_data = array();
                foreach ($values
                         as
                         $values) {
                    $custom_option_data[] = $values->getData();

                }
                $custom_option['data'] = $custom_option_data;
                $options[]             = $custom_option;
            }
        }

        return $options;
    }

    private function resetProductInBlock($product) {
        $this->_coreRegistry->unregister('current_product');
        $this->_coreRegistry->unregister('product');
        $this->_coreRegistry->register('current_product', $product);
        $this->_coreRegistry->register('product', $product);

        return $this;
    }
}
