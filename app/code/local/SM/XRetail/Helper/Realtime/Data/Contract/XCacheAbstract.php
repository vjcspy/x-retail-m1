<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/17/16
 * Time: 3:16 PM
 */


/**
 * Class XCacheAbstract
 * @package SM\XRetail\Helper\Realtime\Data\Contract
 */
abstract class SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract extends Mage_Core_Helper_Abstract {

    /**
     * @var SM_XRetail_Helper_Realtime_Data
     */
    protected $_coreRegistry;

    /**
     * DEFAULT PAGE SIZE
     */
    const PAGE_SIZE = 100;

    /**
     * DEFAULT LIFE TIME X-CACHE
     */
    const XCACHE_LIFE_TIME = 9999999;

    /**
     * Mỗi một cache instance sẽ override lại prefix
     * @var string
     */
    protected $_prefix_cache_key = 'prefix_cache';

    /**
     * @var string
     */
    protected $_cacheTag = 'xCache_RealTime';

    /**
     * data chứa id của item trong từng page cache
     * @var array
     */
    protected $_pageItemIds;

    /**
     * data all cache: bao gồm các page cache data
     * @var array
     */
    protected $_xCacheData;

    /**
     * @var Zend_Cache_Core
     */
    protected $_mageCache;

    /**
     * XCacheAbstract constructor.
     */
    public function __construct() {
        $this->_mageCache = Mage::app()->getCache();
        $this->_coreRegistry = Mage::helper('xretail/realtime_data');
    }

    /**
     * Load cache data
     * @param null $page
     * @param null $pageSize
     */
    protected function loadXCache($page = null, $pageSize = null) {
        $this->getXCachePageItemIds();
        $this->getXCacheData($page, $pageSize);
    }

    /**
     * load data cache page item ids. Đây là index của item trong cache
     * @return array|string
     */
    protected function getXCachePageItemIds() {
        if (is_null($this->_pageItemIds)) {
            $temp = $this->_mageCache->load($this->getCacheKey(array($this->_prefix_cache_key, 'page_item_ids')));
            $this->_pageItemIds = !$temp ? array(1 => array()) : json_decode($temp, true);
        }
        return $this->_pageItemIds;
    }

    /**
     * load data của per page cache
     * @param null $page
     * @param null $pageSize
     * @return array
     */
    protected function getXCacheData($page = null, $pageSize = null) {
        if (is_null($pageSize) && is_null($page))
            foreach ($this->_pageItemIds as $page => $ids) {
                $dataCache = $this->_mageCache->load($this->getCacheKey(array($this->_prefix_cache_key, 'x_cache_data', $page)));
                if (!isset($this->_xCacheData[$page]))
                    if (!!$dataCache)
                        $this->_xCacheData[$page] = json_decode($dataCache, true);
                    else
                        $this->_xCacheData[$page] = array();
            }
        else {
            $pageNeedLoad = $this->getPageNeedLoad($page, $pageSize);
            $data = array();
            foreach ($pageNeedLoad as $pageLoad) {
                if (!isset($this->_xCacheData[$pageLoad])) {
                    $dataCache = $this->_mageCache->load($this->getCacheKey(array($this->_prefix_cache_key, 'x_cache_data', $pageLoad)));
                    if (!!$dataCache)
                        $data[] = $this->_xCacheData[$pageLoad] = json_decode($dataCache, true);
                    else
                        $this->_xCacheData[$pageLoad] = array();
                }
            }
            return $data;
        }
        return $this->_xCacheData;
    }

    /**
     * Get real page need load because pageSize in cache is difference with current pageSize
     * @param $page
     * @param $pageSize
     * @return array
     * @throws \Exception
     */
    private function getPageNeedLoad($page, $pageSize) {
        if (is_null($pageSize) || is_null($page))
            throw new \Exception('page & page size must not null');
        $startItem = ($page - 1) * $pageSize + 1;
        $endItem = $startItem + $pageSize - 1;
        $pageStart = $this->getPageOfItemByPosition($startItem);
        $pageEnd = $this->getPageOfItemByPosition($endItem);
        $pageNeedLoad = array();
        for ($i = $pageStart; $i <= $pageEnd; $i++) {
            $pageNeedLoad[] = $i;
        }
        return $pageNeedLoad;
    }

    /**
     * @param $position
     * @return float
     */
    private function getPageOfItemByPosition($position) {
        return (int)(($position - 1) / self::PAGE_SIZE) + 1;
    }

    /**
     * Get key for cache
     *
     * @param array $data
     * @return string
     */
    protected function getCacheKey($data) {
        $serializeData = array();
        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $serializeData[$key] = $value->getId();
            } else {
                $serializeData[$key] = $value;
            }
        }

        return md5(serialize($serializeData));
    }

    /**
     * retrieve numbers of cache items
     * @return string
     */
    protected function getItemsCount() {
        return $this->_mageCache->load($this->getCacheKey(array($this->_prefix_cache_key, 'items_count')));
    }

    /**
     * set numbers of cache items
     * @param $count
     * @return bool
     */
    protected function setItemsCount($count) {
        return $this->_mageCache->save($count, $this->getCacheKey(array($this->_prefix_cache_key, 'items_count')), array($this->_cacheTag), self::XCACHE_LIFE_TIME);
    }

    /**
     * Add Items to cache
     * @param $item
     * @param string $fieldId
     * @return mixed
     * @throws Exception
     */
    public function addItem($item, $fieldId = 'id') {
        if (is_array($item) && (!isset($item['id']) && !isset($item['entity_id']))) {
            $originLastPage = $lastPage = $this->getLastPageCache();
            foreach ($item as $itemData) {
                /* Nếu data save là [] rồi thì thôi, còn nếu là dạng API Data thì sẽ được convert về dạng []*/
                if (is_object($itemData))
                    if (!($itemData instanceof SM_XRetail_Model_Api_Data_Contract_ApiDataAbstract))
                        throw new Exception('Not Support Object');
                    else
                        $itemData = $itemData->getOutput();

                if ($originLastPage != $lastPage) {
                    //new page dataXCache. It must save to cache
                    $this->saveDataToXCache($this->_xCacheData[$lastPage - 1], $lastPage - 1);

                    // why i need reloadXCache?
//                    $this->loadXCache($lastPage, self::PAGE_SIZE);

                    // save current data to local variable
                    $position = count($this->_xCacheData[$lastPage]);
                    $this->_xCacheData[$lastPage][$position] = $itemData;
                } else {
                    // why i need reloadXCache? Because must load current page data to add new item
                    $this->loadXCache($lastPage, self::PAGE_SIZE);

                    $position = count($this->_xCacheData[$lastPage]);
                    $this->_xCacheData[$lastPage][$position] = $itemData;
                }
                $this->addItemToXCachePageItemsIds($itemData, $fieldId);
                $lastPage = $this->getLastPageCache();
            }
            if ($originLastPage != $lastPage)
                $this->saveDataToXCache($this->_xCacheData[$lastPage - 1], $lastPage - 1);
            else
                $this->saveDataToXCache($this->_xCacheData[$lastPage], $lastPage);
        } else {
            $lastPage = $this->getLastPageCache();
            $this->loadXCache($lastPage, self::PAGE_SIZE);
            $position = count($this->_xCacheData[$lastPage]);
            $this->_xCacheData[$lastPage][$position] = $item;
            $this->addItemToXCachePageItemsIds($item, $fieldId);
            $this->saveDataToXCache($this->_xCacheData[$lastPage], $lastPage);
        }
        $this->saveXCachePageItemIds();
        return $lastPage;
    }

    /**
     * Update new item to cache
     * @param $items
     * @param string $fieldId
     * @return $this
     * @throws Exception
     */
    public function updateItems($items, $fieldId = 'id') {
        $pageReCache = array();
        foreach ($items as $item) {
            $page = $this->getPageOfItemById($item[$fieldId]);
            if ($page === false) {
                $this->addItem(array($item));
                continue;
            }
            $this->loadXCache($page, self::PAGE_SIZE);
            foreach ($this->_xCacheData[$page] as $position => $cacheItem) {
                if ($cacheItem[$fieldId] == $item[$fieldId]) {
                    $this->_xCacheData[$page][$position] = $item;
                    $pageReCache[$page] = $page;
                }
            }
        }
        if (!empty($pageReCache)) {
            foreach ($pageReCache as $pa) {
                $this->saveDataToXCache($this->_xCacheData[$pa], $pa);
            }
        }
        return $this;
    }

    /**
     * Save data x cache page item ids vào cache
     * @return bool
     */
    private function saveXCachePageItemIds() {
        return $this->_mageCache->save(json_encode($this->_pageItemIds), $this->getCacheKey(array($this->_prefix_cache_key, 'page_item_ids')), array($this->_cacheTag), self::XCACHE_LIFE_TIME);
    }

    /**
     * Khi add 1 item vào x cache thì cũng phải update index tức là page items ids
     * @param $item
     * @param $fieldId
     */
    private function addItemToXCachePageItemsIds($item, $fieldId = 'id') {
        $pageItemIds = $this->getXCachePageItemIds();
        end($pageItemIds);
        $keyEnd = key($pageItemIds);
        if (count($this->_pageItemIds[$keyEnd]) >= self::PAGE_SIZE)
            $keyEnd++;
        $this->_pageItemIds[$keyEnd][] = $item['id'];
    }

    /**
     * Retrieve last page Cache
     * @return mixed
     */
    private function getLastPageCache() {
        $pageItemIds = $this->getXCachePageItemIds();
        end($pageItemIds);
        $keyEnd = key($pageItemIds);
        if (count($this->_pageItemIds[$keyEnd]) < self::PAGE_SIZE)
            return $keyEnd == 0 ? 1 : $keyEnd;
        else
            return $keyEnd + 1;
    }

    /**
     * Cái này để lưu kết quả khi mà bên ngoài gọi vào hàm getItems với tham số là page, pageSize
     * @var
     */
    protected $_itemsData;

    /**
     * @param $page
     * @param $pageSize
     * @param bool $foreMode
     * @return mixed
     */
    public function getItems($page, $pageSize, $foreMode = false) {
        /** @var $page #A#M#CMage_Core_Helper_Abstract.getItems.0|? */
        /** @var TYPE_NAME $page */
        $key = $this->getCacheKey(array($page, $pageSize));
        if (!isset($this->_itemsData[$key]) || $foreMode === true) {
            $dataItems = array();
            $this->loadXCache($page, $pageSize);

            /*Start Position and end position*/
            $startItem = ($page - 1) * $pageSize;
            $endItem = $startItem + $pageSize - 1;

            foreach ($this->_xCacheData as $page => $items) {
                foreach ($items as $position => $pageItem) {
                    $p = ($page - 1) * self::PAGE_SIZE + $position;
                    if ($startItem <= $p && $p <= $endItem)
                        $dataItems[] = $pageItem;
                }
            }
            $this->_itemsData[$key] = $dataItems;
        }
        return $this->_itemsData[$key];
    }

    /**
     * @param $data
     * @param $page
     * @return bool
     */
    protected function saveDataToXCache($data, $page) {
        if (is_array($data))
            $data = json_encode($data);
        return $this->_mageCache->save($data, $this->getCacheKey(array($this->_prefix_cache_key, 'x_cache_data', $page)), array($this->_cacheTag), self::XCACHE_LIFE_TIME);
    }

    /**
     * @var
     */
    protected $_itemData;

    /**
     * Retrieve $item by id in cache
     * @param $id
     * @param string $fieldId
     * @return bool
     */
    public function getItemsById($id, $fieldId = 'id') {
        if (!isset($this->_itemData[$id])) {
            $page = $this->getPageOfItemById($id);
            if ($page === false)
                return $this->_itemData[$id] = false;
            $this->loadXCache($page, self::PAGE_SIZE);
            foreach ($this->_xCacheData[$page] as $position => $item) {
                if ($item[$fieldId] == $id)
                    return $this->_itemData[$id] = $item;
            }
            return $this->_itemData[$id] = false;
        }
        return $this->_itemData;
    }

    /**
     * Retrieve page of item by id which saved in cache
     * @param $id
     * @return bool|int|string
     */
    private function getPageOfItemById($id) {
        $this->getXCachePageItemIds();
        foreach ($this->_pageItemIds as $pageItemId => $data) {
            if (in_array($id, $data))
                return $pageItemId;
        }
        return false;
    }

    /**
     * Clean All Cache
     * @return bool
     */
    public function cleanCache() {
        return $this->_mageCache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array($this->_cacheTag));
    }

}
