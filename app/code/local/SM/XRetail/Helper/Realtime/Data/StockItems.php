<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/17/16
 * Time: 2:49 PM
 */

/**
 * Class StockItems
 *
 * @package SM\XRetail\Helper\Realtime\Data
 */
class SM_XRetail_Helper_Realtime_Data_StockItems extends SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract {
    protected $_prefix_cache_key = 'xStockItems_xCache';
    /**
     * @var Mage_CatalogInventory_Model_Resource_Stock_Item
     */
    protected $_stockItemsModel;

    static $is_loading = false;

    public function __construct() {
        $this->_stockItemsModel = Mage::getResourceModel('cataloginventory/stock_item');
        parent::__construct();
    }

    public function getStockItem($item) {
        if (!!($itemData = $item->getData()) && isset($itemData['stock_item']) && $itemData['stock_item'] instanceof Mage_CatalogInventory_Model_Stock_Item)
            return $itemData['stock_item'];
        $inventoryModel = Mage::getModel('cataloginventory/stock_item');
        $this->_stockItemsModel->loadByProductId($inventoryModel, $item->getId());
        return $inventoryModel;
    }
}
