<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/17/16
 * Time: 2:48 PM
 */
class SM_XRetail_Helper_Realtime_Data_Products extends SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract {

    /**
     * @var string
     */
    protected $_prefix_cache_key = 'xProducts_xCache';

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function getCacheByStore($storeId = 0) {
        $this->_prefix_cache_key = 'xProducts_xCache' . $storeId;

        return $this;
    }
}
