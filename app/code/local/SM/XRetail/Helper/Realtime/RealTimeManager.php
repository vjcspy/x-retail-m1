<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 3/11/16
 * Time: 9:49 AM
 */
class SM_XRetail_Helper_Realtime_RealTimeManager extends SM_XRetail_Helper_Realtime_Data_Contract_XCacheAbstract {

    /**
     * @var string
     */
    protected $_prefix_cache_key = 'xRetail_RealTime';
    /**
     * key for cache product must update
     */
    const PRODUCT_NEED_UPDATE = 'PRODUCT_NEED_UPDATE';
    /**
     * key for cache REAL_TIME_KEY
     */
    const XPOS_NEW_KEY = 'XPOS_NEW_KEY';

    /**
     * @var Mage_Catalog_Model_Product
     */
    protected $_productModel;

    /**
     * @var array
     */
    protected $_dataReloadedProduct;

    /**
     * @var \SM_XRetail_Helper_Realtime_CacheManagement
     */
    protected $_xCache;

    /**
     * @var \SM_XRetail_Model_Api_Configuration
     */
    protected $_apiConfig;
    /**
     * @var SM_XRetail_Helper_Data
     */
    private $_helper;

    /**
     * SM_XPos_Helper_RealTimeProduct constructor.
     */
    public function __construct() {

        $this->_productModel = Mage::getModel('catalog/product');
        $this->_xCache       = Mage::helper('xretail/realtime_cacheManagement');
        $this->_apiConfig    = Mage::getModel('xretail/api_configuration');
        $this->_helper       = Mage::helper('xretail');
        parent::__construct();
    }

    /**
     * @param $productId
     *
     * @return $this
     */
    public function proNeedReload($productId) {

        if (is_array($productId))
            foreach ($productId as $item) {
                $this->setProductForUseReload($item);
            }
        else
            $this->setProductForUseReload($productId);
        $this->resetNewKey();

        return $this->triggerAnotherApp();
    }

    /**
     * Trigger X-Retail client
     */
    public function triggerAnotherApp($url = null, $data = array()) {
        if (is_null($url))
            $url = $this->_apiConfig->getStoreConfig('xretail/config/triggerurl');
        if (is_null($url))
            throw new Exception("Please config realtime url");
        else {
            $url = "http://mage1.dev/index.php/rest/v1/test";

            return $this->sendPostViaSocket($url, $data);
        }
    }

    /**
     * @param $url
     * @param $params
     *
     * @return string
     */
    private function sendRequest($url, $params) {

        $opts = array(
            'http' =>
                array(
                    'method'  => 'GET',
                    'header'  => "Content-Type: text/xml\r\n",
                    // "Authorization: Basic " . base64_encode("$https_user:$https_password") . "\r\n",
                    'content' => $params,
                    'timeout' => 30
                )
        );

        $context = stream_context_create($opts);

        return (file_get_contents($url, false, $context, -1, 40000));
    }

    /**
     * @param      $url
     * @param null $param
     *
     * @return bool
     */
    private function sendGetViaSocket($url, $param = null) {

        $parts  = parse_url($url);
        $packet = "GET " . $parts['path'] . " HTTP/1.0\r\n";
        $packet .= "Host: {$parts['host'] }\r\n";
        $packet .= "Connection: close\r\n\r\n";


        try {
            $sock = fsockopen(
                $parts['host'],
                isset($parts['port']) ? $parts['port'] : 80,
                $errno,
                $errstr,
                30);

            // FIXME:FUCKING MAGENTO ERROR ?????????????????????????
            // fwrite($sock, $packet);
            // fclose($sock);

            socket_close($sock);

            return true;
        } catch (Exception $e) {
            $this->_helper->addLog("Can't update real time product. Because: " . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $url
     * @param $params
     *
     * @return bool
     */
    private function sendPostViaSocket($url, $params) {
        $post_params = array();
        foreach ($params as $key => &$val) {
            if (is_array($val)) $val = implode(',', $val);
            $post_params[] = $key . '=' . urlencode($val);
        }
        $post_string = implode('&', $post_params);

        $parts = parse_url($url);
        $fp    = fsockopen(
            $parts['host'],
            isset($parts['port']) ? $parts['port'] : 80,
            $errno,
            $errstr,
            30);

        $out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $out .= "Host: " . $parts['host'] . "\r\n";
        $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out .= "Content-Length: " . strlen($post_string) . "\r\n";
        $out .= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out .= $post_string;

        fwrite($fp, $out);

        // socket_close($fp);

        return true;
    }

    /**
     * @return array|bool
     */
    public function reloadProduct() {

        if (is_null($this->_dataReloadedProduct)) {
            $this->_dataReloadedProduct = array();
            $products                   = $this->getProductForUseReload();

            if (empty($products))
                die('FALSE????');
            // else
            //     var_dump($products);die();

            /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
            $collection = $this->_productModel->getCollection();

            $collection->addFieldToFilter('entity_id', array('in' => $products));

            foreach ($collection as $item) {
                //if ($item->getTypeId() == 'simple')
                    $item = Mage::getModel('catalog/product')->load($item->getId());

                $xProduct = new SM_XRetail_Model_Api_Data_XProduct($item->getData());

                // get options
                $xProduct->setData('x_options', $this->_xCache->getXOptions($item));

                // get stock_items
                $xProduct->setData('stock_items', $this->_xCache->getStockItem($item)->getData());

                $this->_dataReloadedProduct[] = $xProduct->getOutput();

            }

            $this->_xCache->updateProducts($this->_dataReloadedProduct);

            $this->updateListProductFinished($products);
        }

        return $this->_dataReloadedProduct;
    }

    /**
     * @return $this
     */
    public function resetRealTimeUpdate() {

        $this->resetNewKey();
        $this->resetProductUseReload();

        return $this;
    }

    /**
     * @param $productId
     *
     * @return $this
     * @internal param $storeId
     */
    private function setProductForUseReload($productId) {

        $d = $this->getProductForUseReload();
        if ($d == false)
            $d = array();
        $d[$productId] = $productId;

        $this->setProductUserReload($d);

        return $this;
    }


    /**
     * @param $dataProductFinished
     *
     * @return bool
     */
    protected function updateListProductFinished($dataProductFinished) {

        $productForUseReload = $this->getProductForUseReload();
        if (is_array($dataProductFinished)) {
            foreach ($dataProductFinished as $productId) {
                unset($productForUseReload[$productId]);
            }

            return $this->setProductUserReload($productForUseReload);
        }
        else
            return false;

    }

    /**
     * @return mixed
     */
    private function getProductForUseReload() {

        $productOldNeedLoad = $this->_mageCache->load($this->getCacheKey(array(self::PRODUCT_NEED_UPDATE)));

        return $productOldNeedLoad != false ? json_decode($productOldNeedLoad, true) : array();
    }

    /**
     * @param $dataProduct
     *
     * @return bool
     */
    private function setProductUserReload($dataProduct) {

        if (is_array($dataProduct))
            $dataProduct = json_encode($dataProduct);

        return $this->_mageCache->save(
            $dataProduct,
            $this->getCacheKey(array(self::PRODUCT_NEED_UPDATE)),
            array($this->_cacheTag),
            self::XCACHE_LIFE_TIME);
    }

    /**
     * @return bool
     */
    private function resetProductUseReload() {

        return $this->_mageCache->remove(self::PRODUCT_NEED_UPDATE);
    }

    /**
     * @return bool
     */
    private function resetNewKey() {

        return $this->_mageCache->save(
            md5(microtime()),
            $this->getCacheKey(array($this->_prefix_cache_key, 'x_cache_data', self::XPOS_NEW_KEY)),
            array($this->_cacheTag),
            self::XCACHE_LIFE_TIME);
    }

    /**
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession() {

        return Mage::getSingleton('adminhtml/session');
    }
}
