<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 02/06/2016
 * Time: 17:12
 */
class SM_XRetail_Helper_SqlDirect extends Mage_Core_Helper_Abstract {

    public function fetchQuery($query) {
        $resource = Mage::getSingleton('core/resource');

        /**
         * Retrieve the write connection
         */
        $writeConnection = $resource->getConnection('core_write');

        // /**
        //  * Retrieve our table name
        //  */
        // $table = $resource->getTableName($table);

        /**
         * Execute the query
         */
        $writeConnection->query($query);
    }
}