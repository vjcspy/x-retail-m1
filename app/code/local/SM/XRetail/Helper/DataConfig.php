<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 10:38 AM
 */
class SM_XRetail_Helper_DataConfig {

    public function getApiGetCustomAttributes() {
        return false;
    }

    public function getOrderCreateAllowEvent() {
        return false;
    }

    /**
     * Cách tính discount per item.
     * True: Tính discount ammount theo tỷ trọng
     * False: Trừ theo thứ tự
     *
     * @return bool
     */
    public function calculateDiscountByProportion() {
        return true;
    }
}
