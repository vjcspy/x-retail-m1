<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 10:44 AM
 */
class SM_XRetail_Helper_DiscountPerItems_Data extends Mage_Core_Helper_Abstract {

    /**
     * key discount_per_items
     */
    const DISCOUNT_PER_ITEMS = 'discount_per_items';

    /**
     * key use_discount_per_items
     */
    const USE_DISCOUNT_PER_ITEMS = 'use_discount_per_items';

    /**
     * @var array
     */
    private $_orderItemsData = array();
    /**
     * @var bool
     */
    private $_isUseDiscountPerItems;

    /**
     * @var \SM_XRetail_Helper_Data
     */
    protected $_xRetailHelperData;

    /**
     * @var \SM_XRetail_Helper_CustomSales_Data
     */
    protected $_customeSalesHelper;

    /**
     * SM_XRetail_Helper_DiscountPerItems_Data constructor.
     */
    public function __construct() {

        $this->_xRetailHelperData = Mage::helper('xretail');
        $this->_customeSalesHelper = Mage::helper('xretail/customSales_data');
    }

    /**
     * @param $orderItemsData
     */
    public function processData($orderItemsData) {

        $this->setOrderItemsData($orderItemsData);
        $this->isUseDiscount();
    }

    /**
     * @return bool
     */
    public function isUseDiscount() {

        if (count($this->getOrderItemsData()) == 0)
            return false;
        if (is_null($this->_isUseDiscountPerItems)) {
            $this->_isUseDiscountPerItems = false;
            foreach ($this->getOrderItemsData() as $item) {
                if (isset($item[self::DISCOUNT_PER_ITEMS]) && floatval($item[self::DISCOUNT_PER_ITEMS]) > 0) {
                    Mage::register(self::USE_DISCOUNT_PER_ITEMS, true);
                    return $this->_isUseDiscountPerItems = true;
                }
            }
            Mage::register(self::USE_DISCOUNT_PER_ITEMS, $this->_isUseDiscountPerItems);
        }
        return $this->_isUseDiscountPerItems;
    }

    /**
     * @param $product
     *
     * @return int
     */
    public function getItemBaseDiscount($product) {

        try {
            if ($product instanceof Mage_Catalog_Model_Product) {
                if ($product->getTypeId() == 'configurable') {
                    $productId = $product->getEntityId();
                    $customOptions = $product->getCustomOptions();
                    /* @var $info Mage_Sales_Model_Quote_Item_Option */
                    $info = $customOptions['info_buyRequest'];
                    $vInfo = unserialize($info->getData('value'));
                    foreach ($this->getOrderItemsData() as $item) {
                        if ($item['product_id'] == $productId) {
                            if (isset($item['super_attribute']) && isset($vInfo['super_attribute']))
                                if (count(array_diff($vInfo['super_attribute'], $item['super_attribute'])) > 0)
                                    continue;
                                else
                                    return isset($item[self::DISCOUNT_PER_ITEMS]) ? $item[self::DISCOUNT_PER_ITEMS] : 0;
                        }
                    }
                }
                elseif ($product->getTypeId() == 'simple' && $product->getEntityId() != $this->_customeSalesHelper->getCustomSalesId()) {
                    $productId = $product->getEntityId();

                    // Check with item Split
                    $customOptions = $product->getCustomOption('additional_options');
                    if (!!$customOptions) {
                        $value = unserialize($customOptions->getValue());
                        if (isset($value['split_product']))
                            $splitValue = $value['split_product'];
                    }

                    foreach ($this->getOrderItemsData() as $item) {
                        if ($productId == $item['product_id'] && (!isset($splitValue) || $splitValue == $item['split_product']))
                            return isset($item[self::DISCOUNT_PER_ITEMS]) ? $item[self::DISCOUNT_PER_ITEMS] : 0;
                    }
                }
            }
            elseif ($product instanceof Mage_Sales_Model_Quote_Item) {
                if ($product->getProductType() == 'bundle') {
                    foreach ($this->getOrderItemsData() as $item) {
                        if ($product->getProductId() == $item['product_id'])
                            return isset($item[self::DISCOUNT_PER_ITEMS]) ? $item[self::DISCOUNT_PER_ITEMS] : 0;
                    }
                }
            }
        } catch (\Exception $e) {
            $this->_xRetailHelperData->addLog("Can't get info_buyRequest product with error: " . $e->getMessage());
            return 0;
        }
        return 0;
    }

    /**
     * @return array
     */
    public function getOrderItemsData() {

        return $this->_orderItemsData;
    }

    /**
     * @param array $orderItemsData
     */
    public function setOrderItemsData($orderItemsData) {

        $this->_orderItemsData = $orderItemsData;
    }
}