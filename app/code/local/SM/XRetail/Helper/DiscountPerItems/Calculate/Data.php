<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com
 * Date: 3/21/16
 * Time: 11:25 AM
 */
class SM_XRetail_Helper_DiscountPerItems_Calculate_Data extends Mage_Core_Helper_Abstract {

    private $_quoteBaseTotal;

    public function __construct() { }

    public function getQuoteBaseTotal(Mage_Sales_Model_Quote $quote, Mage_Sales_Model_Quote_Address $address = null) {
        if (is_null($this->_quoteBaseTotal)) {
            if (is_null($address)) {
                if ($quote->isVirtual()) {
                    $address = $quote->getBillingAddress();
                }
                else {
                    $address = $quote->getShippingAddress();
                }
            }
            $baseTotal = 0;
            foreach ($address->getAllItems() as $item) {
                if ($item->getParentItemId())
                    continue;
                if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                    foreach ($item->getChildren() as $child) {
                        $baseTotal += $item->getQty() * ($child->getQty() * $this->_getItemBasePrice($child)) - $child->getBaseDiscountAmount();
                    }
                }
                elseif ($item->getProduct()) {
                    $baseTotal += $item->getQty() * $this->_getItemBasePrice($item)
                                  - $item->getBaseDiscountAmount()
                                  - $item->getMagestoreBaseDiscount();
                }
            }
            if (Mage::getStoreConfig(self::XML_PATH_SPEND_FOR_SHIPPING, $quote->getStoreId())) {
                $shippingAmount = $address->getShippingAmountForDiscount();
                if ($shippingAmount !== null) {
                    $baseShippingAmount = $address->getBaseShippingAmountForDiscount();
                }
                else {
                    $baseShippingAmount = $address->getBaseShippingAmount();
                }
                $baseTotal += $baseShippingAmount - $address->getBaseShippingDiscountAmount() - $address->getMagestoreBaseDiscountForShipping();
            }
            $this->_quoteBaseTotal = $baseTotal;
        }

        return $this->_quoteBaseTotal;
    }

    /**
     * @param $item
     *
     * @return mixed
     */
    public function _getItemBasePrice($item) {
        $price = $item->getBaseDiscountCalculationPrice();

        return ($price !== null) ? $item->getBaseDiscountCalculationPrice() : $item->getBaseCalculationPrice();
    }
}