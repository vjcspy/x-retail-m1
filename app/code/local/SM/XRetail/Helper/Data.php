<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/17/16
 * Time: 2:47 PM
 */
class SM_XRetail_Helper_Data extends Mage_Core_Helper_Abstract {

    const TYPE_OUTPUT = 'underscore';

    /**
     * @return bool
     */
    public function getSupportCustomOptionsSimpleProduct() {
        return false;
    }

    /**
     * @param $array
     *
     * @return mixed
     */
    public function formatDataOutput($array) {
        if (self::TYPE_OUTPUT == 'underscore')
            return $this->fixArrayKey($array);
        else
            return $array;
    }

    /**
     * @param $arr
     *
     * @return array
     */
    protected function fixArrayKey($arr) {
        $php53 = $this;
        $arr   = array_combine(
            array_map(
                function ($str) use ($php53) {
                    return $php53->_underscore($str);
                },
                array_keys($arr)),
            array_values($arr));
        foreach ($arr as $key => $val) {
            if (is_array($val))
                $this->fixArrayKey($arr[$key]);
        }

        return $arr;
    }

    /**
     * Converts field names for setters and geters
     *
     * $this->setMyField($value) === $this->setData('my_field', $value)
     * Uses cache to eliminate unneccessary preg_replace
     *
     * @param string $name
     *
     * @return string
     */
    public function _underscore($name) {
        return strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $name));
    }

    public function addLog($mess, $level = null, $file = null) {
        if (is_null($file))
            $file = 'xRetail_api.log';

        return Mage::log($mess, $level, $file, true);
    }

    public function arraySumIdenticalKeys() {
        $arrays      = func_get_args();
        $arrayReduce = array_reduce(
            $arrays,
            function ($keys, $arr) {
                $a = $keys + $arr;

                return $a;
            },
            []);
        $keys        = array_keys($arrayReduce);
        $sums        = [];

        foreach ($keys as $key) {
            $sums[$key] = array_reduce($arrays, function ($sum, $arr) use ($key) { return $sum + @$arr[$key]; });
        }

        return $sums;
    }

    public function register($key, $value, $graceful = false) {
        Mage::register($key, $value, $graceful);
    }

    public function unregister($key) {
        Mage::unregister($key);
    }

    public function registry($key) {
        return Mage::registry($key);
    }
}
