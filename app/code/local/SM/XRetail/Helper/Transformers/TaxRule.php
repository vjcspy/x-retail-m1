<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 19/07/2016
 * Time: 15:32
 */
class SM_XRetail_Helper_Transformers_TaxRule extends Mage_Core_Helper_Abstract{

    public function transformPullData(&$taxRules)
    {
        foreach ($taxRules as &$item) {
            $item['id']= $item['tax_calculation_rule_id'];
            $item['name'] = $item['code'];
            $taxClass = array();
            foreach($item['tax_calculations'] as $calculation){
                $taxClass['customer'][] = intval($calculation['customer_tax_class_id']);
                $taxClass['product'][] = intval($calculation['product_tax_class_id']);
                $taxClass['rates'][] = intval($calculation['tax_calculation_rate_id']);
            }
            $item['customer_tax_class'] = array_values(array_unique($taxClass['customer']));
            $item['product_tax_class'] = array_values(array_unique($taxClass['product']));
            $item['rates'] = array_values(array_unique($taxClass['rates']));
            unset($item['tax_calculations']);
        }
        return $taxRules;
    }
}