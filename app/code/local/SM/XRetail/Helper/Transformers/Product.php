<?php

/**
 * Created by PhpStorm.
 * User: tung
 * Date: 19/07/2016
 * Time: 13:52
 */
class SM_XRetail_Helper_Transformers_Product extends Mage_Core_Helper_Abstract
{
    protected $_isMultiWarehouseEnable;

    public function __construct(){
        $this->_isMultiWarehouseEnable = Mage::helper('xretail/integrate')->isMultiWarehouseAvailable();
    }

    public function transformPullData(&$products)
    {
        foreach ($products as &$productData) {
            if (!$productData['websites']) {
                $productData['websites'] = array();
            }

            $productData['image'] = $productData['origin_image'];

            if(!$this->_isMultiWarehouseEnable){
                $productData['stock_item'] = $productData['stock_items'];
            }

            foreach ($productData['category_id'] as $category) {
                $productData['categories'][] = intval($category);
            }

            $productData['children'] = array();

            $configurableProduct = array(
                'items' => array(),
                'attribute' => array()
            );

            /*process configurable product with child items*/
            if ($productData['type_id'] == 'configurable' && $productData['x_options']) {
                $productData['configurable_base_price'] = $productData['x_options']['basePrice'];

                foreach ($productData['x_options']['attributes'] as $attribute) {
                    $attribute['attribute_id'] = $attribute['id'];
                    foreach ($attribute['options'] as &$section) {
                        $section['id'] = intval($section['id']);
                        foreach ($section['products'] as $productId) {
                            if (empty($configurableProduct['items'][$productId])) {
                                $configurableProduct['items'][$productId] = array();
                            }

                            $configurableProduct['items'][$productId][$attribute['id']] = intval($section['id']);
                        }
                    }

                    $configurableProduct['attribute'][] = $attribute;
                }

                foreach ($configurableProduct['items'] as $productId => $options) {
                    $productData['configurable_products'][] = array(
                        'parent_id' => intval($productData['id']),
                        'product_id' => intval($productId),
                        'options' => $options,
                    );

                    $productData['children'][] = $productId;
                }

                /*todo connector need to trigger event create new simple product if customer create child product in parent page*/
                /*todo in case of realtime use, need to check child simple product is exist in x-retail or not*/
                /*todo the creation of new child product is higher priority to make sure the creation of relations process is correct */
                $productData['configurable_attributes'] = $configurableProduct['attribute'];
                unset($productData['x_options']);
            }

            /*process bundle product with child items*/
            if ($productData['type_id'] == 'bundle') {
                foreach ($productData['x_options'] as $option) {
                    //separate custom , bundle option
                    if (empty($option['section'])) {
                        return;
                    }
                    foreach ($option['section'] as &$section) {
                        //todo : field selection_can_change_qty return '0'/ '1' is string type
                        $section['product_id']['default_qty'] = $section['product_id']['selection_qty'];
                        $section['product_id']['selection_can_change_qty'] = (bool)($section['product_id']['selection_can_change_qty']);
                        $section['product_id']['is_in_stock'] = (bool)($section['product_id']['is_in_stock']);
                        $section['product_id']['is_default'] = (bool)($section['product_id']['is_default']);
                        $productId = $section['product_id']['product_id'];
                        $section = $section['product_id'];
                        unset($section['product_id']);
                        $section['product_id'] = $productId;
                        $productData['children'][] = $productId;
                    }

                    $productData['bundle_items'][] = array(
                        'id' => $option['data']['option_id'],
                        'title' => $option['data']['title'],
                        'type' => $option['data']['type'],
                        'is_required' => (boolean)$option['data']['required'],
                        'position' => $option['data']['position'],
                        'bundle_selections' => $option['section'],
                    );
                }
                unset($productData['x_options']);
            }

            /*process group product*/
            $productGroup = array('items' => array());
            if ($productData['type_id'] == 'grouped') {
                foreach ($productData['x_options'] as $option) {
                    $productGroup['items'][$option['entity_id']] = array(
                        'default_qty' => $option['qty'],
                        'product_id' => $option['entity_id'],
                        'position' => $option['position']
                    );
                    $productData['children'][] = $option['entity_id'];
                }
                $productData['group_items'] = $productGroup['items'];
                unset($productData['x_options']);
            }
            unset($productData['category_id']);
            unset($productData['stock_items']);
        }
        return $products;
    }
}
