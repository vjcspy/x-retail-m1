<?php

/**
 * Created by PhpStorm.
 * User: tung
 * Date: 18/07/2016
 * Time: 15:47
 */
class SM_XRetail_Helper_Transformers_Exchange extends Mage_Core_Helper_Abstract
{

    public function transformPostData($refundData)
    {
        $payload = array();

        $payload['creditmemo'] = array(
            'items' => $refundData['items'],
            'order_id' => $refundData['connector_order_id'],
            'do_offline' => '1',
            'comment_text' => '',
            'shipping_amount' => '0',
            'adjustment_positive' => 0,
            'adjustment_negative' => 0,
        );

        $payload['creditmemo']['warehouse-select'] = $refundData['warehouse-select'];
        $payload['creditmemo']['select-warehouse-supplier'] = $refundData['select-warehouse-supplier'];

        if ($refundData['refund_adjustment'] > 0) {
            $payload['creditmemo']['adjustment_negative'] = $refundData['refund_adjustment'];
        }

        if ($refundData['refund_adjustment'] < 0) {
            $payload['creditmemo']['adjustment_positive'] = -$refundData['refund_adjustment'];
        }

        //where exchange
        //total paid for new order = total paid - adjustment
//        $items = array();
//        $adjustment = abs($orderRefund['refund_adjustment']);
//        if(count($orderRefund['exchanges'])) {
//            foreach ($orderRefund['payments'] as $payment) {
//                $paymentAmount = $payment['amount'];
//                if ($payment['amount'] > $adjustment && $adjustment > 0) {
//                    $paymentAmount -= $adjustment;
//                    $adjustment = 0;
//                }
//
//                if ($payment['amount'] < $adjustment && $adjustment > 0) {
//                    $paymentAmount = 0;
//                    $adjustment -= $paymentAmount;
//                }
//                $payments[$payment['payment_method']['code']] = $paymentAmount;
//            }
//
//            foreach ($orderRefund['order']['items'] as $item) {
//                //if item is partial refunded and isn't refunded then add item to
//                if ($item['quantity_refunded'] < $item['quantity'] || !$item['connector_item_id']) {
//                    $newItem = array(
//                        'origin_id' => $item['id'],
//                        'qty' => $item['quantity'] - $item['quantity_refunded'],
//                        'custom_price' => $item['custom_price'],
//                        'use_discount' => "1",
//                        'discount_per_items' => ($item['item_discount_amount'] + $item['whole_order_discount_amount']) / $item['quantity'],
//                        'product_id' => $item['product_id'],
//                    );
//                    if ($item['type'] == 'bundle') {
//                        $newItem['bundle_option'] = array();
//                        $newItem['bundle_option_qty'] = array();
//                        foreach ($item['bundle_items'] as $childProduct) {
//                            $newItem['bundle_option'][$childProduct['bundle_option_id']] = $childProduct['selection_id'];
//                            $newItem['bundle_option_qty'][$childProduct['bundle_option_id']] = $childProduct['quantity'];
//                            unset($newItem['bundle_items']);
//                            //todo: magento does not support custom price for bundle
//                            unset($newItem['custom_price']);
//                        }
//                    }
//                    if (!empty($item['custom_options'])) {
//                        foreach ($item['custom_options'] as $id => $option) {
//                            $newItem['options'][$id] = $option['raw_value'];
//                        }
//                        unset($newItem['custom_options']);
//                    }
//                    if (!$item['product_id']) {
//                        $newItem['product_id'] = 'custom_sale';
//                    }
//                    $items[] = $newItem;
//                }
//            }
//        }
//        $payload['items'] = $items;
//        $order = $orderRefund['order'];
//        $payload['customer_id'] = $order['customer_id'];
//        $payload['store_id'] = $order['store_id'];
//        $payload['order'] = array(
//            'billing_address' => $order['billing_address'],
//            'shipping_address' => $order['shipping_address'],
//            'payment_method' => 'xretail_multiple_payment',
//            'payment_data' => $payments,
//            'shipping_method' => 'xretail_shipping_xretail_dummy_1_code'
//        );
//        $payload['order']['shipping_address']['firstname'] = $order['customer_first_name'];
//        $payload['order']['shipping_address']['lastname'] = $order['customer_last_name'];
//        $payload['order']['billing_address']['firstname'] = $order['customer_first_name'];
//        $payload['order']['billing_address']['lastname'] = $order['customer_last_name'];
//
//        $payload['order']['shipping_address']['street'] = array(0 => $payload['order']['billing_address']['street']);
//        $payload['order']['billing_address']['street'] = array(0 => $payload['order']['billing_address']['street']);
//
//        $payload['order']['account'] = array(
//            'group_id' => $order['customer_group_id'],
//            'email' => $order['customer_email']
//        );
        return $payload;
    }
}