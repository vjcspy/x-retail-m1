<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 18/07/2016
 * Time: 17:59
 */

class SM_XRetail_Helper_Transformers_Customer extends Mage_Core_Helper_Abstract {

    public function transformPostData($customer)
    {
        $data = array(
            'customer_id' => $customer['id'],
            'account' => array(
                'website_id'       => $customer['website_id'],
                'group_id'         => $customer['group_id'],
                'firstname'        => $customer['first_name'],
                'lastname'         => $customer['last_name'],
                'email'            => $customer['email'],
                'default_billing'  => $customer['default_billing'],
                'default_shipping'  => $customer['default_shipping']
            ),
            'address' => array()
        );

        foreach ($customer['addresses'] as $index => $address) {
            $addressData = array(
                'id' => $address['id'],
                'entity_id' => $address['id'],
                'firstname'        => $customer['first_name'],
                'lastname'         => $customer['last_name'],
                'street'     =>
                    array(
                        0 => $address['street'],
                        1 => '',
                    ),
                'city'       => $address['city'],
                'country_id' => $address['country_id'],
                'region_id'  => $address['region_id'],
                'postcode'   => $address['postcode'],
                'telephone'  => $address['telephone'],
            );

            // todo: this is temporary solution while we do not support custom region
            if (empty($addressData['region_id'])) {
                $addressData['region'] = '';
            }

            $idx = $address['id'];
            if (! $address['id']) {
                $idx = '_item' . $index;
            }

            if ($index == $customer['default_billing_index']) {
                $data['account']['default_billing'] = $idx;
            }

            if ($index ==  $customer['default_shipping_index']) {
                $data['account']['default_shipping'] = $idx;
            }

            $data['address'][$idx] = $addressData;
        }

        return $data;
    }

    public function transformPullData(&$customers)
    {
        foreach ($customers as &$customer) {
            $customer['addresses'] = $customer['address'];
            unset($customer['address']);
            if ($customer['addresses']) {
                foreach ($customer['addresses'] as &$address) {
                    $address['id'] = $address['entity_id'];
                    $address['postcode'] = (int)$address['postcode'];
                }
            } else {
                $customer['addresses'] = array();
            }
        }
        return $customers;
    }
}
