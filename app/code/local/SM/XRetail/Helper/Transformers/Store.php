<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 19/07/2016
 * Time: 15:57
 */

class SM_XRetail_Helper_Transformers_Store extends Mage_Core_Helper_Abstract{

    public function transformPullData(&$stores)
    {
        foreach ($stores as &$item) {
            $item['id'] = $item['website_id'];

            if (! empty($item['groups'])) {
                foreach ($item['groups'] as &$group) {
                    $group['id'] = $group['group_id'];
                    if (! empty($group['stores'])) {
                        foreach ($group['stores'] as &$store) {
                            $store['id'] = $store['store_id'];
                        }
                    }
                }
            }
        }
        return $stores;
    }
}