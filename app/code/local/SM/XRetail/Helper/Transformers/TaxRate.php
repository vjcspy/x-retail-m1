<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 19/07/2016
 * Time: 15:37
 */

class SM_XRetail_Helper_Transformers_TaxRate extends Mage_Core_Helper_Abstract{

    public function transformPullData(&$taxRates)
    {
        foreach ($taxRates as &$item) {
            $item['id'] = $item['tax_calculation_rate_id'];
        }
        return $taxRates;
    }
}