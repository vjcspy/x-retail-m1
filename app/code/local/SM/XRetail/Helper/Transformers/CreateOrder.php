<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 18/07/2016
 * Time: 15:47
 */
class SM_XRetail_Helper_Transformers_CreateOrder extends Mage_Core_Helper_Abstract {

    public function transformPostData($orderData){
        $orderData['order'] = array();
        $orderData['billing_address']['firstname'] = $orderData['customer_first_name'];
        $orderData['billing_address']['lastname'] =$orderData['customer_last_name'];
        $orderData['shipping_address']['firstname'] = $orderData['customer_first_name'];
        $orderData['shipping_address']['lastname'] = $orderData['customer_last_name'];
        $orderData['order']['billing_address'] = $orderData['billing_address'];
        $orderData['order']['shipping_address'] = $orderData['shipping_address'];
        $orderData['order']['account']['group_id'] = $orderData['customer_group_id'];
        if(!empty($orderData['coupon_code'])) {
            $orderData['order']['coupon']['code'] = $orderData['coupon_code'];
        }

        foreach ($orderData['payments'] as $payment) {
                $orderData['order']['payment_data'][$payment['payment_method_code']] = $payment['amount'];
                $orderData['order']['payment_method'] = $payment['payment_method_code'];
        }

        foreach ($orderData['items'] as $key => &$item) {
            $item["qty"] = $item["quantity"];
            $item['discount_per_items'] = $item['item_discount_amount'];
            unset($item['quantity']);
            if(!$item['product_id']){
                $item['product_id'] = 'custom_sale';
            }else{
                if($item['type'] == 'configurable'){
                    $item['product_id'] = $item['parent_product_id'];
                    $superAttributes = array();
                    foreach($item['super_attributes'] as $attributeId => $attribute){
                        $superAttributes[$attributeId] = $attribute['option_id'];
                    }
                    unset($item['super_attributes']);
                    $item['super_attribute'] = $superAttributes;
                }
                if($item['type'] == 'simple' && !empty($item['custom_options'])){
                    foreach ($item['custom_options'] as $id => $option) {
                        $item['options'][$id] = $option['raw_value'];
                    }
                    unset($item['custom_options']);
                }
            }
        }

        if($orderData['has_shipment']){
            $orderData['order']['shipping_method'] = $orderData['shipping_address']['code'];
            $orderData['order']['shipping_amount'] = $orderData['shipping_amount'];
        }

        // order should always use multiple payment, so it can be exchanged later
        $orderData['order']['payment_method'] = 'xretail_multiple_payment';

        unset($orderData['billing_address']);
        unset($orderData['shipping_address']);
        unset($orderData['customer']);
        unset($orderData['payments']);
        return $orderData;
    }
}