<?php
/**
 * Created by PhpStorm.
 * User: tung
 * Date: 19/07/2016
 * Time: 10:45
 */
class SM_XRetail_Helper_Transformers_Update extends Mage_Core_Helper_Abstract{

    protected $transformerMap = array(
        'customer' => 'Customer',
        'product' => 'Product'
    );

    /*convert data for realtime*/
    public function transformPullData($data)
    {
        foreach($data as $modelName => &$changes){

            if(is_null($this->transformerMap[$modelName])){
                return $data;
            }

            $helper = Mage::helper('xretail/transformers_'.$this->transformerMap[$modelName]);
            foreach($changes as $action => &$items){
                switch ($action) {
                    case 'new':
                        $helper->transformPullData($items);
                        break;
                    case 'change':
                        $helper->transformPullData($items);
                        break;
                    case 'remove':
                        break;
                }
            }
        }
        return $data;
    }
}