<?php

/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/26/16
 * Time: 10:21 AM
 */
class SM_XRetail_Helper_Product extends Mage_Core_Helper_Abstract {

    protected $_helperData;

    public function getAllCustomAttributes() {
        $key = 'custom_attributes';
        if (!isset($this->_helperData[$key])) {
            $result = array();
            $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                ->addVisibleFilter();
            if ($attributes != null && $attributes->count() > 0)
                foreach ($attributes as $attribute) {
                    $result[] = array('value' => $attribute->getAttributeCode(), 'key' => $attribute->getFrontendLabel());
                }
            $this->_helperData[$key] = $result;
        }
        return $this->_helperData[$key];
    }
}
