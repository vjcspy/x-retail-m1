<?php
/**
 * User: vjcspy
 * Date: 3/21/16
 * Time: 10:19 AM
 */

/** @var $installer SM_XRetail_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
    $this->getTable('sales/order'),
    'retail_order_create_mode',
    'varchar(50) NOT NULL default ""');

$installer->install();

$installer->endSetup();