<?php
/**
 * User: vjcspy
 * Date: 3/21/16
 * Time: 10:19 AM
 */

/** @var $installer SM_XRetail_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('sales/order'), 'warehouse_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'length' => 12,
    'nullable' => true,
    'default' => 0,
    'comment' => 'Warehouse id for order'
));

$installer->install();

$installer->endSetup();