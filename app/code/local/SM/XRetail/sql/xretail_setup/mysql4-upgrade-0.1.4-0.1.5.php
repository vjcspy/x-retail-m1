<?php
/**
 * User: vjcspy
 * Date: 3/21/16
 * Time: 10:19 AM
 */

/** @var $installer SM_XRetail_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
          ->addColumn(
              $this->getTable('sales/order'),
              'retail_refund_adjust',
              'decimal(12,4) NOT NULL default 0'
          );
$installer->getConnection()
          ->addColumn(
              $this->getTable('sales/order'),
              'base_retail_refund_adjust',
              'decimal(12,4) NOT NULL default 0'
          );

$installer->install();

$installer->endSetup();