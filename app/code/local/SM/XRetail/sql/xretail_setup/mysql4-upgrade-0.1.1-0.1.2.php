<?php
/**
 * User: vjcspy
 * Date: 3/21/16
 * Time: 10:19 AM
 */

/** @var $installer SM_XRetail_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

// Fixed shipping. IF remove it, will have multiple shipping
Mage::getModel('xretail/shipping_shippmentManager')->update();

$installer->getConnection()->addColumn(
    $this->getTable('sales/invoice'),
    'retail_discount_per_items_base_discount',
    'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'retail_discount_per_items_discount', 'decimal(12,4) NOT NULL default 0');

$installer->install();

$installer->endSetup();